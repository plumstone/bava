package de.bredex.phil.bava;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import de.bredex.phil.bava.components.AlertDialog;
import de.bredex.phil.bava.icons.Icons;
import de.bredex.phil.bava.utils.BavaUncaughtExceptionHandler;
import de.bredex.phil.bava.utils.StageTitleBuilder;

import com.jthemedetecor.OsThemeDetector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Start Klasse des Projekts Bundesliga Auswetungs- und Verwaltungsanwendung.
 *
 * @author Philemon Hilscher
 */
@Configuration
@EnableScheduling
public class Bava extends Application {

    private static final Logger LOG = LoggerFactory.getLogger(Bava.class);
    private Parameters programArguments;
    private ApplicationContext applicationContext;
    private BavaUncaughtExceptionHandler uncaughtExceptionHandler;

    @Override
    public void init() {
        programArguments = getParameters();
        String[] args = programArguments.getRaw().toArray(new String[0]);
        applicationContext = SpringApplication.run(Main.class, args);
        uncaughtExceptionHandler = applicationContext.getBean(BavaUncaughtExceptionHandler.class);
    }

    @Override
    public void start(Stage primaryStage) {
        System.setProperty("java.net.useSystemProxies", "true");
        Thread.setDefaultUncaughtExceptionHandler(uncaughtExceptionHandler::handleException);
        ApplicationObjects.setApplicationHostServices(getHostServices());
        ApplicationObjects.setPrimaryStage(primaryStage);
        try {
            ParentController parentController = applicationContext.getBean(ParentController.class);
            Parent rootLayout = parentController.getPane();
            var scene = new Scene(rootLayout, Double.MIN_VALUE, Double.MIN_VALUE);
            initTheme(parentController, scene);

            primaryStage.setScene(scene);
            primaryStage.setMaximized(programArguments.getRaw().stream().anyMatch(s -> s.contains("maximized")));
            primaryStage.titleProperty().bind(new StageTitleBuilder().stageTitleProperty());
            primaryStage.getIcons().add(Icons.APPLICATION_ICON);
            primaryStage.show();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            AlertDialog.showExceptionDialog(e);
        } catch (ExceptionInInitializerError e) {
            LOG.error("Exception in Application start method", e);
            stop();
        }
    }

    private void initTheme(ParentController parentController, Scene scene) {
        scene.getStylesheets().add(String.valueOf(getClass().getResource("stylesheets.css")));

        if (OsThemeDetector.isSupported()) {
            final OsThemeDetector detector = OsThemeDetector.getDetector();
            parentController.setTheme(detector.isDark());
            detector.registerListener(isDark -> Platform.runLater(() -> parentController.setTheme(isDark)));
            parentController.setManualThemeSwitchDisabled();
        } else {
            parentController.setTheme(programArguments.getRaw().stream().anyMatch(s -> s.contains("dark")));
        }

        if (programArguments.getRaw().stream().anyMatch(s -> s.contains("vintage"))) {
            scene.getStylesheets().add(String.valueOf(getClass().getResource("vintage.css")));
        }
    }

    @Override
    public void stop() {
        System.exit(SpringApplication.exit(applicationContext));
    }

    /**
     * Runs the application.
     *
     * @param args
     *         the arguments
     */
    public static void runApplication(String... args) {
        launch(args);
    }

}
