package de.bredex.phil.bava.components;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

import de.bredex.phil.bava.entity.Spiel;
import de.bredex.phil.bava.entity.Spieler;
import de.bredex.phil.bava.entity.Spieltag;
import de.bredex.phil.bava.entity.Verein;
import de.bredex.phil.bava.i18n.Messages;

/**
 * Input for an entire Matchday.
 *
 * @author Philemon Hilscher
 */
@SuppressWarnings("java:S110")
public class MatchdayInput extends GridPane {

    private static final int COLUMN_TEAM_LABEL_HOME = 0;
    private static final int COLUMN_GOALS_LABEL_HOME = 1;
    private static final int COLUMN_SEPARATOR = 2;
    private static final int COLUMN_GOALS_LABEL_GUEST = 3;
    private static final int COLUMN_TEAM_LABEL_GUEST = 4;
    private static final String SEPARATOR_STYLECLASS = "separator-label";

    private int matchCount;

    /**
     * Creates a new {@link MatchdayInput}.
     */
    public MatchdayInput() {
        rebuild();
    }

    private void addMatchInput(int matchNumber) {
        add(new TeamLabel(null, Messages.getString("stammdaten.spieltage.heim")), COLUMN_TEAM_LABEL_HOME,
                matchNumber);
        add(new GoalsLabel(null, Messages.getString("stammdaten.spieltage.goals")), COLUMN_GOALS_LABEL_HOME,
                matchNumber);
        var trenner = new Label(":");
        trenner.getStyleClass().addAll(EntityLabel.STYLECLASS, SEPARATOR_STYLECLASS);
        trenner.pseudoClassStateChanged(EntityLabel.PSEUDO_CLASS_FILLED, true);
        add(trenner, COLUMN_SEPARATOR, matchNumber);
        add(new GoalsLabel(null, Messages.getString("stammdaten.spieltage.goals")), COLUMN_GOALS_LABEL_GUEST,
                matchNumber);
        add(new TeamLabel(null, Messages.getString("stammdaten.spieltage.gast")), COLUMN_TEAM_LABEL_GUEST,
                matchNumber);
    }

    /**
     * @return the currently applied matchday to this {@link MatchdayInput}.
     */
    public Spieltag getMatchday() {
        var result = new Spieltag();
        for (var i = 0; i < matchCount; i++) {
            result.addMatch(getMatch(i));
        }
        return result;
    }

    private Spiel getMatch(int matchIndex) {
        var result = new Spiel();
        result.setNummer(matchIndex + 1);

        var teamLabelHeim = (TeamLabel) getChild(COLUMN_TEAM_LABEL_HOME, matchIndex);
        var goalsLabelHeim = (GoalsLabel) getChild(COLUMN_GOALS_LABEL_HOME, matchIndex);
        var homePlayer = new Spieler();
        homePlayer.setVereinId(teamLabelHeim.getEntity().getId());
        homePlayer.setTore(goalsLabelHeim.getEntity());
        result.setHeim(homePlayer);

        var goalsLabelGast = (GoalsLabel) getChild(COLUMN_GOALS_LABEL_GUEST, matchIndex);
        var teamLabelGast = (TeamLabel) getChild(COLUMN_TEAM_LABEL_GUEST, matchIndex);
        var guestPlayer = new Spieler();
        guestPlayer.setVereinId(teamLabelGast.getEntity().getId());
        guestPlayer.setTore(goalsLabelGast.getEntity());
        result.setGast(guestPlayer);


        result.setStarted(Objects.nonNull(goalsLabelHeim.getEntity()) && Objects.nonNull(goalsLabelGast.getEntity()));
        result.setFinished(result.isStarted());
        return result;
    }

    /**
     * Sets a match to a matchNumber.
     *
     * @param matchNumber
     *         the matchNumber
     * @param match
     *         the match
     * @param home
     *         the home team
     * @param guest
     *         the guest team
     */
    public void setMatch(int matchNumber, Spiel match, Verein home, Verein guest) {
        var teamLabelHeim = (TeamLabel) getChild(COLUMN_TEAM_LABEL_HOME, matchNumber);
        teamLabelHeim.setEntity(Objects.isNull(match) ? null : home);

        var goalsLabelHeim = (GoalsLabel) getChild(COLUMN_GOALS_LABEL_HOME, matchNumber);
        goalsLabelHeim.setEntity(Objects.isNull(match) ? null : match.getHeim().getTore());

        var goalsLabelGast = (GoalsLabel) getChild(COLUMN_GOALS_LABEL_GUEST, matchNumber);
        goalsLabelGast.setEntity(Objects.isNull(match) ? null : match.getGast().getTore());

        var teamLabelGast = (TeamLabel) getChild(COLUMN_TEAM_LABEL_GUEST, matchNumber);
        teamLabelGast.setEntity(Objects.isNull(match) ? null : guest);
    }

    private Node getChild(int columnIndex, int rowIndex) {
        for (Node child : getChildren()) {
            if (GridPane.getColumnIndex(child) == columnIndex && GridPane.getRowIndex(child) == rowIndex) {
                return child;
            }
        }
        throw new IndexOutOfBoundsException(String.format("No item at indices %d %d", columnIndex, rowIndex));
    }

    /**
     * @return all team labels which are applied to this {@link MatchdayInput}.
     */
    public Set<TeamLabel> getTeamLabels() {
        Set<TeamLabel> result = new HashSet<>();
        for (var i = 0; i < matchCount; i++) {
            var teamLabelHeim = (TeamLabel) getChild(COLUMN_TEAM_LABEL_HOME, i);
            result.add(teamLabelHeim);
            var teamLabelGast = (TeamLabel) getChild(COLUMN_TEAM_LABEL_GUEST, i);
            result.add(teamLabelGast);
        }
        return result;
    }

    /**
     * Gets the match count.
     *
     * @return the match count
     */
    public int getMatchCount() {
        return matchCount;
    }

    /**
     * Sets the match count.
     *
     * @param matchCount
     *         the match count
     */
    public void setMatchCount(int matchCount) {
        this.matchCount = matchCount;
    }

    /**
     * Resets the {@link MatchdayInput}.
     */
    public void reset() {
        for (Node child : getChildren()) {
            if (child instanceof EntityLabel<?> entityLabel) {
                entityLabel.reset();
            }
        }
    }

    /**
     * Rebuilds the {@link MatchdayInput}.
     */
    public void rebuild() {
        getChildren().clear();

        for (var i = 0; i < matchCount; i++) {
            addMatchInput(i);
        }
    }
}
