package de.bredex.phil.bava.components;

import java.util.Objects;
import javafx.css.PseudoClass;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

/**
 * Abstract class for an Entity displayed on a label.
 *
 * @param <T>
 *         The entity to display
 * @author Philemon Hilscher
 */
@SuppressWarnings("java:S110")
public abstract class EntityLabel<T> extends Label {

    /** Default styleclass for this class. */
    public static final String STYLECLASS = "entity-label";

    /** Pseudo class for the filled state. */
    public static final PseudoClass PSEUDO_CLASS_FILLED = PseudoClass.getPseudoClass("filled");
    protected static final PseudoClass PSEUDO_CLASS_HOVERED = PseudoClass.getPseudoClass("hovered");
    private static final PseudoClass PSEUDO_CLASS_EMPTY = PseudoClass.getPseudoClass("empty");

    private T entity;

    private T tempEntity;

    private final String placeholder;

    private Type type = Type.TARGET;

    /**
     * Creates a new {@link EntityLabel}.
     *
     * @param entity
     *         the entity
     * @param placeholder
     *         the placeholder
     */
    protected EntityLabel(T entity, String placeholder) {
        this.placeholder = placeholder;
        getStyleClass().add(STYLECLASS);
        pseudoClassStateChanged(PSEUDO_CLASS_EMPTY, true);
        setEntity(entity);

        setOnDragEntered(this::onDragEntered);
        setOnDragExited(this::onDragExited);
        setOnDragOver(this::onDragOver);
        setOnDragDropped(this::onDragDropped);
        setOnDragDone(this::onDragDone);
        setOnDragDetected(this::onDragDetected);
    }

    protected abstract void onDragDetected(MouseEvent event);

    protected abstract void onDragExited(DragEvent event);

    protected abstract void onDragEntered(DragEvent event);

    protected abstract void onDragOver(DragEvent event);

    protected abstract void onDragDropped(DragEvent event);

    protected abstract void onDragDone(DragEvent event);

    /**
     * Sets the temporary entity and updates the pseudo classes.
     *
     * @param tempEntity
     *         the tempEntity
     */
    public void setTempEntity(T tempEntity) {
        this.tempEntity = tempEntity;
        updatePseudoClasses(tempEntity, entity, PSEUDO_CLASS_HOVERED, PSEUDO_CLASS_FILLED);
    }

    /**
     * Gets entity.
     *
     * @return the entity
     */
    public T getEntity() {
        return entity;
    }

    /**
     * Sets the entity and updates the pseudo classes.
     *
     * @param entity
     *         the entity
     */
    public void setEntity(T entity) {
        this.entity = entity;
        updatePseudoClasses(entity, tempEntity, PSEUDO_CLASS_FILLED, PSEUDO_CLASS_HOVERED);
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public Type getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param type
     *         the type
     */
    public void setType(Type type) {
        this.type = type;
    }

    private void updatePseudoClasses(T primaryEntity, T secondaryEntity, PseudoClass primaryPseudoClass,
            PseudoClass secondaryPseudoClass) {
        if (Objects.nonNull(primaryEntity)) {
            if (Objects.nonNull(secondaryEntity)) {
                pseudoClassStateChanged(secondaryPseudoClass, false);
            } else {
                pseudoClassStateChanged(PSEUDO_CLASS_EMPTY, false);
            }
            pseudoClassStateChanged(primaryPseudoClass, true);
        } else {
            pseudoClassStateChanged(primaryPseudoClass, false);
            if (Objects.nonNull(secondaryEntity)) {
                pseudoClassStateChanged(secondaryPseudoClass, true);
            } else {
                pseudoClassStateChanged(PSEUDO_CLASS_EMPTY, true);
            }
        }
        updateText();
    }

    private void updateText() {
        if (tempEntity != null) {
            setText(tempEntity.toString());
        } else if (entity != null) {
            setText(entity.toString());
        } else {
            setText(placeholder);
        }
    }

    /**
     * Inits the given dragboard.
     *
     * @param dragboard
     *         the dragboard
     * @param event
     *         the mouse event
     * @param dataFormat
     *         the data format
     */
    protected void initDragboard(Dragboard dragboard, MouseEvent event, DataFormat dataFormat) {
        SnapshotParameters snapshotParameters = new SnapshotParameters();
        snapshotParameters.setFill(Color.TRANSPARENT);
        dragboard.setDragView(this.snapshot(snapshotParameters, null), event.getX(), event.getY());
        var ccontent = new ClipboardContent();
        ccontent.put(dataFormat, getEntity());
        dragboard.setContent(ccontent);
    }

    /**
     * The enum Type.
     */
    enum Type {

        /**
         * Source type.
         */
        SOURCE,
        /**
         * Target type.
         */
        TARGET
    }

    /**
     * Resets the {@link EntityLabel}.
     */
    public void reset() {
        setEntity(null);
        setTempEntity(null);
    }
}
