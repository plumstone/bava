package de.bredex.phil.bava.stammdaten.verein;

import java.util.UUID;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.fxml.FXML;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.util.Callback;

import de.bredex.phil.bava.JFXDialogController;
import de.bredex.phil.bava.entity.Verein;
import de.bredex.phil.bava.i18n.Messages;
import de.bredex.phil.bava.utils.Constants;

/**
 * The type team dialog controller.
 *
 * @author Philemon Hilscher
 */
public class TeamDialogController extends JFXDialogController<Verein, GridPane> {

    @FXML
    private TextField nameInput;
    @FXML
    private TextField townInput;

    /**
     * Instantiates a new team dialog controller.
     */
    public TeamDialogController() {
        super("TeamDialogPane.fxml");
    }

    @FXML
    private void initialize() {
        setTitle(Messages.getString("stammdaten.new"));
        setHeaderText(Messages.getString("stammdaten.teams.new.header"));
        Label plus = new Label("+");
        plus.getStyleClass().add("plus-sign");
        setGraphic(plus);

        BooleanBinding nameInputInvalid = Bindings.createBooleanBinding(
                () -> !nameInput.isDisabled() && !nameInput.getText().matches("^\\w[\\wßÄäÖöÜü .-]*$"),
                nameInput.textProperty());

        validateInput(nameInput, nameInputInvalid);
        prepareButtons(nameInputInvalid);
        setOnShown(event -> Platform.runLater(nameInput::requestFocus));
    }

    @Override
    protected Callback<ButtonType, Verein> resultConverter() {
        return buttonType -> {
            if (buttonType.equals(getSaveButtonType())) {
                Verein team = new Verein();
                team.setId(UUID.randomUUID().getMostSignificantBits());
                team.setName(nameInput.getText().isBlank() ? Constants.EMPTY_STRING : nameInput.getText());
                team.setStadt(townInput.getText().isBlank() ? Constants.EMPTY_STRING : townInput.getText());
                return team;
            }
            return null;

        };
    }
}
