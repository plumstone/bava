package de.bredex.phil.bava.tabelle;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.bredex.phil.bava.entity.Verein;
import de.bredex.phil.bava.mapper.TeamMapper;
import de.bredex.phil.bava.mapper.TeamMapperImpl;
import de.openligadb.api.teams.Team;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests for the {@link VereinWrapper}.
 *
 * @author Philemon Hilscher
 */
class VereinWrapperTest {

    private static final List<Verein> TEAMS = new ArrayList<>();

    @BeforeAll
    static void init() throws URISyntaxException, IOException {
        final URL teams = VereinWrapperTest.class.getResource("teams_19_20.json");
        String json = Files.readString(Path.of(Objects.requireNonNull(teams).toURI()));
        ObjectMapper objectMapper = new ObjectMapper();
        TeamMapper teamMapper = new TeamMapperImpl();

        List<Verein> teamList = teamMapper.mapList(Arrays.asList(objectMapper.readValue(json, Team[].class)));
        TEAMS.addAll(teamList);

        final Verein team = new Verein();
        final Long id = 1L;
        final String name = "Team 1";
        team.setId(id);
        team.setName(name);

        TEAMS.add(team);
    }

    private static Stream<Verein> provideTeams() {
        return TEAMS.stream();
    }

    @ParameterizedTest
    @MethodSource("provideTeams")
    void testWrapping(Verein team) {
        final VereinWrapper vereinWrapper = new VereinWrapper(team);
        assertEquals(vereinWrapper.getId(), team.getId());
        assertEquals(vereinWrapper.getName(), team.getName());
    }

    @Test
    void testTore() {
        VereinWrapper vereinWrapper = new VereinWrapper(new Verein());
        final int auswaertsGegentore = 4;
        final int heimGegentore = 6;
        vereinWrapper.addAuswaertsGegentore(auswaertsGegentore);
        vereinWrapper.addHeimGegentore(heimGegentore);
        assertEquals(auswaertsGegentore + heimGegentore, vereinWrapper.getGesamtGegentore());

        final int auswaertsTore = 2;
        final int heimTore = 7;
        vereinWrapper.addAuswaertsTore(auswaertsTore);
        vereinWrapper.addHeimTore(heimTore);
        assertEquals(auswaertsTore + heimTore, vereinWrapper.getGesamtTore());

        assertEquals(formatGoals(auswaertsTore, auswaertsGegentore), vereinWrapper.getAuswaertsToreFormatted());
        assertEquals(formatGoals(heimTore, heimGegentore), vereinWrapper.getHeimToreFormatted());
        assertEquals(formatGoals(auswaertsTore + heimTore, auswaertsGegentore + heimGegentore),
                vereinWrapper.getGesamtToreFormatted());

        vereinWrapper.reset();

        assertEquals(0, vereinWrapper.getGesamtGegentore());
        assertEquals(0, vereinWrapper.getGesamtTore());
        assertEquals(formatGoals(0, 0), vereinWrapper.getAuswaertsToreFormatted());
        assertEquals(formatGoals(0, 0), vereinWrapper.getHeimToreFormatted());
        assertEquals(formatGoals(0, 0), vereinWrapper.getGesamtToreFormatted());
    }

    @Test
    void testSpiele() {
        VereinWrapper vereinWrapper = new VereinWrapper(new Verein());
        final int auswaertsSiege = 5;
        final int heimSiege = 9;
        vereinWrapper.addAuswaertsSiege(auswaertsSiege);
        vereinWrapper.addHeimSiege(heimSiege);
        assertEquals(auswaertsSiege + heimSiege, vereinWrapper.getGesamtSiege());

        final int auswaertsUnentschieden = 3;
        final int heimUnentschieden = 4;
        vereinWrapper.addAuswaertsUnentschieden(auswaertsUnentschieden);
        vereinWrapper.addHeimUnentschieden(heimUnentschieden);
        assertEquals(auswaertsUnentschieden + heimUnentschieden, vereinWrapper.getGesamtUnentschieden());

        final int auswaertsNiederlagen = 6;
        final int heimNiederlagen = 7;
        vereinWrapper.addAuswaertsNiederlagen(auswaertsNiederlagen);
        vereinWrapper.addHeimNiederlagen(heimNiederlagen);
        assertEquals(auswaertsNiederlagen + heimNiederlagen, vereinWrapper.getGesamtNiederlagen());

        assertEquals(auswaertsSiege + auswaertsUnentschieden + auswaertsNiederlagen,
                vereinWrapper.getAuswaertsSpiele());
        assertEquals(heimSiege + heimUnentschieden + heimNiederlagen, vereinWrapper.getHeimSpiele());
        assertEquals(auswaertsSiege + heimSiege + auswaertsUnentschieden + heimUnentschieden + auswaertsNiederlagen
                + heimNiederlagen, vereinWrapper.getGesamtSpiele());

        vereinWrapper.reset();

        assertEquals(0, vereinWrapper.getGesamtSiege());
        assertEquals(0, vereinWrapper.getGesamtUnentschieden());
        assertEquals(0, vereinWrapper.getGesamtNiederlagen());
        assertEquals(0, vereinWrapper.getAuswaertsSpiele());
        assertEquals(0, vereinWrapper.getHeimSpiele());
        assertEquals(0, vereinWrapper.getGesamtSpiele());
    }

    private String formatGoals(int arg1, int arg2) {
        return String.format("%02d:%02d", arg1, arg2);
    }
}
