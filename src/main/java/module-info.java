/**
 * The module-info for the bava module.
 *
 * @since 1.1
 */
module bava {
    requires com.fasterxml.jackson.annotation;
    requires com.fasterxml.jackson.databind;
    requires com.jthemedetector;
    requires eventbus.java;
    requires java.annotation;
    requires java.compiler;
    requires java.sql;
    requires java.net.http;
    requires java.persistence;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires org.hibernate.orm.core;
    requires org.mapstruct;
    requires org.slf4j;
    requires spring.beans;
    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires spring.core;
    requires spring.context;
    requires spring.data.commons;
    requires spring.data.jpa;
    requires spring.tx;

    exports com.gitlab.api.releases to
            com.fasterxml.jackson.databind;
    exports de.bredex.phil.bava to
            javafx.graphics,
            spring.beans,
            spring.context,
            eventbus.java;
    exports de.bredex.phil.bava.components to
            javafx.fxml;
    exports de.bredex.phil.bava.db to
            spring.beans;
    exports de.bredex.phil.bava.entity to
            spring.beans;
    exports de.bredex.phil.bava.mapper to
            spring.beans;
    exports de.bredex.phil.bava.tabelle to
            eventbus.java,
            spring.beans;
    exports de.bredex.phil.bava.verlauf to
            eventbus.java,
            spring.beans;
    exports de.bredex.phil.bava.spieltag to
            eventbus.java,
            spring.beans;
    exports de.bredex.phil.bava.stammdaten to
            eventbus.java,
            spring.beans;
    exports de.bredex.phil.bava.stammdaten.saison to
            eventbus.java,
            spring.beans;
    exports de.bredex.phil.bava.stammdaten.spieltag to
            eventbus.java,
            spring.beans;
    exports de.bredex.phil.bava.stammdaten.verein to
            eventbus.java,
            spring.beans;
    exports de.bredex.phil.bava.utils to
            eventbus.java,
            spring.beans;
    exports de.bredex.phil.bava.web to
            spring.beans,
            spring.context,
            eventbus.java;
    exports de.openligadb.api.matchdata to
            com.fasterxml.jackson.databind;
    exports de.openligadb.api.teams to
            com.fasterxml.jackson.databind;

    opens db.migration;
    opens de.bredex.phil.bava to
            javafx.fxml,
            spring.core;
    opens de.bredex.phil.bava.entity to
            org.hibernate.orm.core,
            spring.core;
    opens de.bredex.phil.bava.spieltag to
            javafx.base,
            javafx.fxml,
            spring.core;
    opens de.bredex.phil.bava.stammdaten to
            javafx.fxml,
            spring.core;
    opens de.bredex.phil.bava.stammdaten.saison to
            javafx.fxml,
            spring.core;
    opens de.bredex.phil.bava.stammdaten.spieltag to
            javafx.fxml,
            spring.core;
    opens de.bredex.phil.bava.stammdaten.verein to
            javafx.fxml,
            spring.core;
    opens de.bredex.phil.bava.tabelle to
            javafx.fxml;
    opens de.bredex.phil.bava.utils to
            spring.core;
    opens de.bredex.phil.bava.verlauf to
            javafx.fxml;
    opens de.bredex.phil.bava.web to
            spring.core;

}
