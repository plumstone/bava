package de.bredex.phil.bava.repository;

import de.bredex.phil.bava.entity.Verein;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * The team repository is responsible for managing all {@link Verein} objects in the application.
 *
 * @author Philemon Hilscher
 */
@Repository
public interface TeamRepository extends JpaRepository<Verein, Long> {

    /**
     * Returns an {@link Optional}&lt;{@link Verein}&gt; if there exists one with the given name.
     *
     * @param name
     *         the name
     * @return an {@link Optional} which may contain a {@link Verein}
     */
    Optional<Verein> findByName(String name);
}
