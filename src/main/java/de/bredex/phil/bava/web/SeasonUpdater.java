package de.bredex.phil.bava.web;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.http.HttpResponse;
import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;

import de.bredex.phil.bava.BavaEvent;
import de.bredex.phil.bava.TeamSynchronizer;
import de.bredex.phil.bava.entity.Saison;
import de.bredex.phil.bava.entity.Verein;
import de.bredex.phil.bava.mapper.TeamMapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import static de.bredex.phil.bava.utils.EntityVoConverter.convert;
import de.openligadb.api.matchdata.Match;
import de.openligadb.api.teams.Team;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * The {@link SeasonUpdater} repetitively updates the season.
 *
 * @author Philemon Hilscher
 */
@Component
public class SeasonUpdater {

    /** The logger. */
    private static final Logger LOG = LoggerFactory.getLogger(SeasonUpdater.class);

    private final TeamMapper teamMapper;

    private final TeamSynchronizer teamSynchronizer;

    private final WebService service;

    private boolean pause;

    private Saison season;

    private final WebRequest webRequest;

    /**
     * Creates a new {@link SeasonUpdater}.
     *
     * @param teamMapper
     *         the team mapper
     * @param teamSynchronizer
     *         the team synchronizer
     * @param webRequest
     *         the web request component for api calls
     */
    public SeasonUpdater(TeamMapper teamMapper, TeamSynchronizer teamSynchronizer, WebRequest webRequest) {
        this.teamMapper = teamMapper;
        this.teamSynchronizer = teamSynchronizer;
        this.webRequest = webRequest;
        service = new WebService();
        service.setOnSucceeded(value -> {
            EventBus.getDefault().post(new BavaEvent.UpdateStateChanged(Worker.State.SUCCEEDED, null));
            EventBus.getDefault().post(new BavaEvent.SeasonListFetched(service.getValue()));
        });
        service.setOnRunning(
                value -> EventBus.getDefault().post(new BavaEvent.UpdateStateChanged(Worker.State.RUNNING, null)));
        service.setOnFailed(value -> {
            Throwable t = value.getSource().getException();
            EventBus.getDefault().post(new BavaEvent.UpdateStateChanged(Worker.State.FAILED, t));
            if (t instanceof IOException e) {
                LOG.warn(e.getMessage(), e);
            } else {
                LOG.error(t.getMessage(), t);
            }
        });
        EventBus.getDefault().register(this);
    }

    /**
     * Updates the season in a schedule defined by the cron expression.
     */
    @Scheduled(cron = "0/30 * * * * ?")
    public void updateSeasonScheduled() {
        if (!pause && season != null) {
            EventBus.getDefault().post(new BavaEvent.UpdateStateChanged(Worker.State.RUNNING, null));
            if (LOG.isTraceEnabled()) {
                LOG.trace("update the season!");
            }
            try {
                if (teamSynchronizer.getWebTeamList().isEmpty()) {
                    HttpResponse<String> teamResponse =
                            webRequest.getAvailableTeams(season.getLeague(), season.getYear());
                    List<Team> teams = getListFromResponse(teamResponse, Team[].class);
                    teamSynchronizer.setWebTeamList(teams);
                }

                HttpResponse<String> seasonResponse = webRequest.getSpieltage(season.getLeague(), season.getYear());
                List<Verein> teams;
                teams = teamSynchronizer.getSeasonTeams();
                List<Match> matches = getListFromResponse(seasonResponse, Match[].class);
                Saison saison = convert(matches, teams.size());

                saison.setLeague(season.getLeague());
                saison.setYear(season.getYear());
                saison.setTeams(teamSynchronizer.getSeasonTeams(teams));
                EventBus.getDefault().post(new BavaEvent.UpdateStateChanged(Worker.State.SUCCEEDED, null));
                this.season = saison;
                EventBus.getDefault().post(new BavaEvent.SeasonUpdated(saison));
            } catch (IOException e) {
                EventBus.getDefault().post(new BavaEvent.UpdateStateChanged(Worker.State.FAILED, e));
            }
        }
    }

    /**
     * Gets list from response.
     *
     * @param <T>
     *         the type parameter
     * @param teamResponse
     *         the team response
     * @param tClass
     *         the t class
     * @return the list from response
     */
    public <T> List<T> getListFromResponse(HttpResponse<String> teamResponse, Class<T[]> tClass) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return Arrays.asList(objectMapper.readValue(teamResponse.body(), tClass));
        } catch (JsonProcessingException e) {
            LOG.warn("Could not process json.", e);
            return Collections.emptyList();
        }
    }

    /**
     * Updates the season manually. Called by the event bus.
     *
     * @param event
     *         the event
     */
    @Subscribe
    public void updateManually(BavaEvent.UpdateManually event) {
        service.setYear(event.year());
        Platform.runLater(service::restart);
    }

    /**
     * Sets the currently selected season. Called by the event bus.
     *
     * @param event
     *         the event
     */
    @Subscribe
    public void seasonSelected(BavaEvent.SeasonUpdated event) {
        this.season = event.saison();
    }

    /**
     * Pauses the automatic season update. Called by the event bus.
     *
     * @param event
     *         the event
     */
    @Subscribe
    public void pause(BavaEvent.ToggleOnlineOfflineMode event) {
        this.pause = !event.online();
    }

    private class WebService extends Service<List<Saison>> {

        private Year year;

        /**
         * Sets year.
         *
         * @param year
         *         the year
         */
        public void setYear(Year year) {
            this.year = year;
        }

        @Override
        protected Task<List<Saison>> createTask() {
            return new Task<>() {

                @Override
                protected List<Saison> call() throws IOException {
                    LOG.trace("call the task!");
                    List<Saison> result = new ArrayList<>();
                    Saison saison;
                    int league = 0;

                    do {
                        league++;
                        saison = getSaison(league);

                        if (saison != null) {
                            result.add(saison);
                        }
                    } while (saison != null);
                    return result;
                }

                private Saison getSaison(int league) throws IOException {
                    Saison saison = null;
                    HttpResponse<String> teamsResponse = webRequest.getAvailableTeams(league, year);

                    if (teamsResponse.statusCode() == HttpURLConnection.HTTP_OK) {
                        List<Verein> teams = teamMapper.mapList(getListFromResponse(teamsResponse, Team[].class));
                        HttpResponse<String> seasonResponse = webRequest.getSpieltage(league, year);

                        if (seasonResponse.statusCode() == HttpURLConnection.HTTP_OK) {
                            List<Match> matches = SeasonUpdater.this.getListFromResponse(seasonResponse,
                                    Match[].class);
                            saison = convert(matches, teams.size());
                            saison.setYear(year);
                            saison.setLeague(league);
                            saison.setTeams(teamSynchronizer.getSeasonTeams(teams));
                            if (LOG.isDebugEnabled()) {
                                LOG.debug(saison.getName());
                            }

                        }
                    }
                    return saison;
                }
            };
        }
    }

}
