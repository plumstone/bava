package de.bredex.phil.bava;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javafx.application.Platform;

import de.bredex.phil.bava.entity.Saison;
import de.bredex.phil.bava.entity.Verein;
import de.bredex.phil.bava.repository.TeamRepository;
import de.bredex.phil.bava.utils.Comparators;

import de.openligadb.api.teams.Team;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * Synchronizes the online teams with the local teams. This way, the changes that the user made locally will not be lost
 * when fetching the new team list.
 *
 * @author Philemon Hilscher
 */
@Component
public class TeamSynchronizer implements SeasonManager {

    private static final Logger LOG = LoggerFactory.getLogger(TeamSynchronizer.class);

    @Autowired
    private TeamRepository teamRepository;

    private boolean online;

    private Saison season;

    private List<Team> webTeamList;

    /**
     * Instanciates a new team synchronizer.
     */
    public TeamSynchronizer() {
        webTeamList = new ArrayList<>();
        EventBus.getDefault().register(this);
    }

    /**
     * Gets season teams.
     *
     * @return the teams of the current season
     */
    public List<Verein> getSeasonTeams() {
        if (Objects.isNull(season)) {
            return Collections.emptyList();
        }
        return getSeasonTeams(season.getTeams());
    }

    /**
     * Takes a team list and returns a list with all teams that compete in the current season.
     *
     * @param teamList
     *         the team list to filter
     * @return the filtered team list
     */
    public List<Verein> getSeasonTeams(List<Verein> teamList) {
        if (haveTeamsChanged()) {
            List<Verein> result = new ArrayList<>();
            List<Verein> localVereinRepo = teamRepository.findAll();

            if (online) {
                for (Verein webVerein : teamList) {
                    var seasonVerein = localVereinRepo.stream()
                            .filter(verein -> verein.getId().equals(webVerein.getId())).findFirst().orElse(webVerein);
                    result.add(seasonVerein);
                    merge(webVerein);
                }
            } else {
                result = teamList;
            }
            result.sort(Comparators.VEREIN);
            return result;
        } else {
            return teamList;
        }
    }

    private boolean haveTeamsChanged() {
        if (season == null || season.getTeams() == null) {
            return true;
        }
        if (season.getTeams().isEmpty() && !webTeamList.isEmpty()) {
            return true;
        }

        for (Verein team : season.getTeams()) {
            if (webTeamList.stream()
                    .noneMatch(verein -> Objects.equals(Long.valueOf(verein.getTeamId()), team.getId()))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Merges a web team into the current local saved teams.
     *
     * @param verein
     *         the team to merge
     */
    public void merge(Verein verein) {
        var localVerein = teamRepository.findById(verein.getId())
                .orElse(teamRepository.findByName(verein.getName()).orElse(verein));
        var changed = false;

        if (localVerein.getId() == null) {
            localVerein.setId(verein.getId());
            changed = true;
        }
        if (!Objects.equals(localVerein.getId(), verein.getId())) {
            LOG.info("The id of {} has changed from {} to {}", verein, localVerein.getId(), verein.getId());
            // if the id has changed first delete the team with the old id
            teamRepository.deleteById(localVerein.getId());
            localVerein.setId(verein.getId());
            changed = true;
        }
        if (!StringUtils.hasText(localVerein.getName())) {
            localVerein.setName(verein.getName());
            changed = true;
        }
        if (!StringUtils.hasText(localVerein.getStadt())) {
            localVerein.setStadt(verein.getStadt());
            changed = true;
        }
        if (!StringUtils.hasText(localVerein.getIconUrl())) {
            localVerein.setIconUrl(verein.getIconUrl());
            changed = true;
        }

        if (!teamRepository.existsById(localVerein.getId()) || changed) {
            teamRepository.save(localVerein);
        }

    }

    @Subscribe
    @Override
    public void updateView(BavaEvent.SeasonUpdated event) {
        this.season = event.saison();
        if (online && season != null) {
            Platform.runLater(() -> season.setTeams(getSeasonTeams(season.getTeams())));
        }
    }

    /**
     * Toggles the online offline mode.
     *
     * @param event
     *         the toggle event
     */
    @Subscribe
    public void toggleOnlineOffline(BavaEvent.ToggleOnlineOfflineMode event) {
        this.online = event.online();
    }

    /**
     * Gets the web team list.
     *
     * @return the web team list
     */
    public List<Team> getWebTeamList() {
        return webTeamList;
    }

    /**
     * Sets the web team list.
     *
     * @param webTeamList
     *         the web team list
     */
    public void setWebTeamList(List<Team> webTeamList) {
        this.webTeamList = webTeamList;
    }
}
