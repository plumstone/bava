package de.bredex.phil.bava.entity;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Ein Spieler kann an einem Spieltag entweder als Heim oder Gast Verein auftreten.
 *
 * @author Philemon Hilscher
 */
@Entity
public class Spieler {

    @Id
    @GeneratedValue
    private Long id;

    @Basic
    private Long vereinId;

    /** Die Tore des Spielers. */
    @Basic
    private Integer tore;

    /**
     * Gets verein id.
     *
     * @return the verein id
     */
    public Long getVereinId() {
        return vereinId;
    }

    /**
     * Sets verein id.
     *
     * @param vereinId
     *         the verein id
     */
    public void setVereinId(Long vereinId) {
        this.vereinId = vereinId;
    }

    /**
     * Gets tore.
     *
     * @return the tore
     */
    public Integer getTore() {
        return tore;
    }

    /**
     * Sets tore.
     *
     * @param tore
     *         the tore
     */
    public void setTore(Integer tore) {
        this.tore = tore;
    }
}
