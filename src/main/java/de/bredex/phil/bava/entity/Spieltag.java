package de.bredex.phil.bava.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * Die Klasse Spieltag. Sie repräsentiert einen Spieltag in der Saison.
 *
 * @author Philemon Hilscher
 */
@Entity
public class Spieltag {

    @Id
    @GeneratedValue
    private Long id;

    /** Die Nummer des Spieltags. */
    @Basic
    private int nummer;

    /** Die Liste mit allen Spielen an dem Spieltag. */
    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Spiel> spiele = new ArrayList<>();

    /**
     * Holt zu der übergebenen Spielnummer das Spiel und gibt es zurück.
     *
     * @param matchNumber
     *         die Nummer
     * @return das Spiel
     */
    public Spiel getSpiel(int matchNumber) {
        return spiele.stream().filter(spiel -> spiel.getNummer() == matchNumber).findFirst().orElse(null);
    }

    /**
     * Is started boolean.
     *
     * @return true if the match has started
     */
    public boolean isStarted() {
        return spiele.stream().anyMatch(Spiel::isStarted);
    }

    /**
     * Is finished boolean.
     *
     * @return true if the match is finished
     */
    public boolean isFinished() {
        return spiele.stream().allMatch(Spiel::isFinished);
    }

    /**
     * Adds a match to the matchday.
     *
     * @param match
     *         the match
     */
    public void addMatch(Spiel match) {
        spiele.add(match);
    }

    /**
     * Gets nummer.
     *
     * @return the nummer
     */
    public int getNummer() {
        return nummer;
    }

    /**
     * Sets nummer.
     *
     * @param nummer
     *         the nummer
     */
    public void setNummer(int nummer) {
        this.nummer = nummer;
    }

    /**
     * Gets spiele.
     *
     * @return the spiele
     */
    public List<Spiel> getSpiele() {
        return spiele;
    }

    /**
     * Sets spiele.
     *
     * @param spiele
     *         the spiele
     */
    public void setSpiele(List<Spiel> spiele) {
        this.spiele = spiele;
    }
}
