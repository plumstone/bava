package de.bredex.phil.bava;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

import de.bredex.phil.bava.i18n.Messages;
import de.bredex.phil.bava.spieltag.SpieltagController;
import de.bredex.phil.bava.stammdaten.StammdatenController;
import de.bredex.phil.bava.stammdaten.saison.SeasonController;
import de.bredex.phil.bava.tabelle.TabelleController;
import de.bredex.phil.bava.utils.Listeners;
import de.bredex.phil.bava.utils.UpdateChecker;
import de.bredex.phil.bava.verlauf.VerlaufController;
import de.bredex.phil.bava.web.WebRequest;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StopWatch;

/**
 * Der HauptController des Projektes. Steuert die Views in dem Programm.
 *
 * @author Philemon Hilscher
 */
@Controller
public class ParentController extends JFXController<BorderPane> {

    /** The logger. */
    private static final Logger LOG = LoggerFactory.getLogger(ParentController.class);

    /** Der container, in welchem die verschiedenen Views übergeben und dargestellt werden. */
    @FXML
    private StackPane childContainer;

    /** Der view switch button Stammdaten. */
    @FXML
    private Button btnStammdaten;

    /** Der view switch button Spieltag. */
    @FXML
    private Button btnSpieltag;

    /** Der view switch button Tabelle. */
    @FXML
    private Button btnTabelle;

    /** Der view switch button Verlauf. */
    @FXML
    private Button btnVerlauf;

    /** Checkbox for showing/hiding quick results. */
    @FXML
    private CheckBox chboxQuickResults;

    /** Status bar at the bottom to show status information. */
    @FXML
    private HBox statusBar;

    @FXML
    private Pane statusIcon;

    @FXML
    private Label apiUrl;

    @FXML
    private ImageView imgViewSuccess;

    @FXML
    private ImageView imgViewFailure;

    @FXML
    private Hyperlink hyperlinkUpdate;

    @FXML
    private Button btnSwitchTheme;

    @FXML
    private ImageView imgLightTheme;

    @FXML
    private ImageView imgDarkTheme;

    @Autowired
    private StammdatenController controllerStammdaten;

    @Autowired
    private SpieltagController controllerSpieltag;

    @Autowired
    private TabelleController controllerTabelle;

    @Autowired
    private VerlaufController controllerVerlauf;

    private SimpleObjectProperty<Button> activeBtnProperty;

    private SimpleObjectProperty<Worker.State> workerState;

    @Autowired
    private UpdateChecker updateChecker;

    @Autowired
    private WebRequest webRequest;

    /**
     * Creates a new {@link ParentController}.
     */
    public ParentController() {
        super("ParentPane.fxml");
        EventBus.getDefault().register(this);
    }

    @Override
    protected void postConstruct() {
        btnStammdatenClicked();
    }

    @FXML
    private void initialize() {
        activeBtnProperty = new SimpleObjectProperty<>();
        activeBtnProperty.addListener(new Listeners.ViewSwitchButton());

        workerState = new SimpleObjectProperty<>();
        workerState.addListener((observable, oldValue, newValue) -> {
            switch (newValue) {
                case RUNNING -> {
                    statusIcon.getChildren().clear();
                    statusIcon.getChildren().add(new ProgressIndicator());
                }
                case SUCCEEDED -> {
                    statusIcon.getChildren().clear();
                    statusIcon.getChildren().add(imgViewSuccess);
                }
                case FAILED -> {
                    statusIcon.getChildren().clear();
                    statusIcon.getChildren().add(imgViewFailure);
                }
                default -> throw new IllegalArgumentException("Case " + newValue + " not implemented!");
            }
        });

        initControls();
    }

    private void initControls() {
        chboxQuickResults.selectedProperty()
                .addListener((observable, oldValue, newValue) -> chboxQuickResultsClicked(newValue));
    }

    private void chboxQuickResultsClicked(boolean newValue) {
        EventBus.getDefault().post(new BavaEvent.CheckBoxQuickResultChanged(newValue));
    }

    @Override
    protected void refreshView() {
        EventBus.getDefault().post(new BavaEvent.CheckBoxQuickResultChanged(chboxQuickResults.isSelected()));
        EventBus.getDefault().post(new BavaEvent.UpdateManually(SeasonController.getYear()));
        EventBus.getDefault().post(new BavaEvent.ToggleOnlineOfflineMode(true));
        apiUrl.setText(webRequest.getBaseUrl());
    }

    /**
     * Handles the update check result. Shows a download link if a new version is available.
     *
     * @param event
     *         the event
     */
    @Subscribe
    public void updateCheckFinished(BavaEvent.ApplicationUpdateCheckFinished event) {
        if (event.updateAvailable()) {
            Platform.runLater(() -> {
                hyperlinkUpdate.setText(
                        Messages.getString("parent.update-available", updateChecker.getLatestRelease()));
                hyperlinkUpdate.setVisible(true);
            });
        }
    }

    /**
     * Handles the change of the service state. Called by the event bus.
     *
     * @param event
     *         the event
     */
    @Subscribe
    public void serviceStateChanged(BavaEvent.UpdateStateChanged event) {
        Platform.runLater(() -> workerState.set(event.state()));
    }

    @FXML
    private void btnStammdatenClicked() {
        showView(btnStammdaten, controllerStammdaten);
    }

    @FXML
    private void btnSpieltagClicked() {
        showView(btnSpieltag, controllerSpieltag);
    }

    @FXML
    private void btnTabelleClicked() {
        showView(btnTabelle, controllerTabelle);
    }

    @FXML
    private void btnVerlaufClicked() {
        showView(btnVerlauf, controllerVerlauf);
    }

    @FXML
    private void switchTheme() {
        setTheme(getPane().getScene().getStylesheets().contains(String.valueOf(getClass().getResource("light.css"))));
    }

    /**
     * Sets the theme.
     *
     * @param isDark
     *         if the theme should be set to dark
     */
    public void setTheme(boolean isDark) {
        String dark = String.valueOf(getClass().getResource("dark.css"));
        String light = String.valueOf(getClass().getResource("light.css"));

        if (isDark) {
            btnSwitchTheme.setGraphic(imgDarkTheme);
            getPane().getScene().getStylesheets().remove(light);
            getPane().getScene().getStylesheets().add(dark);
        } else {
            btnSwitchTheme.setGraphic(imgLightTheme);
            getPane().getScene().getStylesheets().remove(dark);
            getPane().getScene().getStylesheets().add(light);
        }
    }

    /**
     * Removes the theme switch button visually from the view.
     */
    public void setManualThemeSwitchDisabled() {
        btnSwitchTheme.setVisible(false);
    }

    private void showView(Button button, JFXController<?> controller) {
        if (activeBtnProperty.get() == button) {
            return;
        }
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("display view " + button.getText());
        activeBtnProperty.set(button);
        Timeline timeline = prepareOldPane();

        controller.getPane().translateYProperty().set(childContainer.getHeight());
        childContainer.getChildren().add(controller.getPane());
        stopWatch.stop();
        if (LOG.isTraceEnabled()) {
            LOG.trace(stopWatch.prettyPrint());
        }
        prepareNewPane(controller, timeline);
    }

    private Timeline prepareOldPane() {
        Timeline timeline = new Timeline();
        if (!childContainer.getChildren().isEmpty()) {
            Pane pane = (Pane) childContainer.getChildren().get(childContainer.getChildren().size() - 1);
            if (childContainer.getChildren().size() > 1) {
                childContainer.getChildren().removeIf(paneInView -> paneInView != pane);
            }
            pane.translateXProperty().set(0);
            KeyValue kvOldPane = new KeyValue(pane.translateXProperty(), childContainer.getWidth(),
                    Interpolator.EASE_BOTH);
            final int duration = 500;
            KeyFrame kfOldPane = new KeyFrame(Duration.millis(duration), kvOldPane);
            timeline.getKeyFrames().add(kfOldPane);
            timeline.setOnFinished(event -> {
                if (childContainer.getChildren().get(childContainer.getChildren().size() - 1) != pane) {
                    childContainer.getChildren().remove(pane);
                }
                pane.translateXProperty().set(0);
            });
        }
        return timeline;
    }

    private void prepareNewPane(JFXController<?> controller, Timeline timeline) {
        KeyValue keyValue = new KeyValue(controller.getPane().translateYProperty(), 0, Interpolator.EASE_BOTH);
        final int duration = 500;
        KeyFrame keyFrame = new KeyFrame(Duration.millis(duration), keyValue);
        timeline.getKeyFrames().add(keyFrame);
        timeline.play();
    }

    /**
     * Toggles the showing of the status bar. Called by the event bus.
     *
     * @param event
     *         the event
     */
    @Subscribe
    public void toggleShowStatusBar(BavaEvent.ToggleOnlineOfflineMode event) {
        if (event.online()) {
            showStatusBar();
        } else {
            hideStatusBar();
        }
    }

    private void showStatusBar() {
        Timeline timeline = prepareStatusBar(statusBar.getHeight(), 0);
        timeline.play();
        getPane().setBottom(statusBar);
    }

    private void hideStatusBar() {
        Timeline timeline = prepareStatusBar(0, statusBar.getHeight());
        timeline.play();
        timeline.setOnFinished(event -> getPane().setBottom(null));
    }

    private Timeline prepareStatusBar(double startValue, double endValue) {
        Timeline timeline = new Timeline();
        statusBar.translateYProperty().set(startValue);
        KeyValue keyValue = new KeyValue(statusBar.translateYProperty(), endValue, Interpolator.EASE_BOTH);
        final int duration = 500;
        KeyFrame keyFrame = new KeyFrame(Duration.millis(duration), keyValue);
        timeline.getKeyFrames().add(keyFrame);
        return timeline;
    }

    @FXML
    private void followLink() {
        ApplicationObjects.getApplicationHostServices().showDocument(updateChecker.getDownloadUrl());
    }
}
