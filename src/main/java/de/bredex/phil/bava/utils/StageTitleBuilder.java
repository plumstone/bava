package de.bredex.phil.bava.utils;

import java.util.Objects;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import de.bredex.phil.bava.ApplicationObjects;
import de.bredex.phil.bava.BavaEvent;
import de.bredex.phil.bava.SeasonManager;
import de.bredex.phil.bava.entity.Saison;
import de.bredex.phil.bava.i18n.Messages;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.springframework.util.StringUtils;

/**
 * Builds the stage title.
 *
 * @author Philemon Hilscher
 */
public final class StageTitleBuilder implements SeasonManager {

    private StringProperty stageTitleProperty;

    /**
     * Creates a new {@link StageTitleBuilder}.
     */
    public StageTitleBuilder() {
        EventBus.getDefault().register(this);
    }

    String build(Saison season) {
        final var applicationName = Messages.getString("primarystage.title");

        String versionNumber = ApplicationObjects.getBuildInfo().getProperty("build.version");
        if (versionNumber != null) {
            versionNumber = " v" + versionNumber;
        } else {
            versionNumber = "";
        }

        String seasonName = Constants.EMPTY_STRING;
        if (Objects.nonNull(season) && StringUtils.hasText(season.getName())) {
            seasonName = " - " + season.getName();
        }

        return applicationName + versionNumber + seasonName;
    }

    /**
     * @return the stage title property
     */
    public StringProperty stageTitleProperty() {
        if (Objects.isNull(stageTitleProperty)) {
            stageTitleProperty = new SimpleStringProperty(build(null));
        }
        return stageTitleProperty;
    }

    @Override
    @Subscribe
    public void updateView(BavaEvent.SeasonUpdated event) {
        String newStageTitle = build(event.saison());
        if (Platform.isFxApplicationThread()) {
            stageTitleProperty.set(newStageTitle);
        } else {
            Platform.runLater(() -> stageTitleProperty.set(newStageTitle));
        }
    }
}
