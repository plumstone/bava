package de.bredex.phil.bava.mapper;

import de.bredex.phil.bava.entity.Verein;
import de.openligadb.api.teams.Team;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

/**
 * Mapper between {@link Team} and {@link Verein}.
 *
 * @author Philemon Hilscher
 */
@Mapper(componentModel = "spring")
public interface TeamMapper {

    /**
     * Maps a team value object to a verein entity.
     *
     * @param team
     *         the team value object
     * @return the verein entity
     */
    @Mapping(source = "teamId", target = "id")
    @Mapping(source = "teamName", target = "name")
    @Mapping(source = "shortName", target = "stadt")
    @Mapping(expression = "java(String.valueOf(team.getTeamIconUrl()))", target = "iconUrl")
    Verein map(Team team);

    /**
     * Maps a list of team value objects to a list of verein entities.
     *
     * @param teamList
     *         the list of team value objects
     * @return the list of verein entities
     */
    List<Verein> mapList(List<Team> teamList);
}
