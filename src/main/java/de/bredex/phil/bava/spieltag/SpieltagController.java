package de.bredex.phil.bava.spieltag;

import java.util.Arrays;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;

import de.bredex.phil.bava.BavaEvent;
import de.bredex.phil.bava.JFXController;
import de.bredex.phil.bava.SeasonManager;
import de.bredex.phil.bava.TeamIconCache;
import de.bredex.phil.bava.components.SpieltagSlider;
import de.bredex.phil.bava.entity.Saison;
import de.bredex.phil.bava.entity.Spiel;
import de.bredex.phil.bava.entity.Spieler;
import de.bredex.phil.bava.entity.Spieltag;
import de.bredex.phil.bava.i18n.Messages;
import de.bredex.phil.bava.utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * Die Klasse ControllerSpieltag. Sie ist für die Funktionen in der View Spieltag verantwortlich.
 *
 * @author Philemon Hilscher
 */
@Controller
public class SpieltagController extends JFXController<BorderPane> implements SeasonManager {

    private static final Label NO_SEASON_PLACEHOLDER = new Label(
            Messages.getString("spieltag.tableview.placeholder.no-season"));

    /** Die Tabelle, in der sich die Spiele des aktuel ausgewählten Spieltages befinden. */
    @FXML
    private TableView<SpielWrapper> tableViewSpieltag;

    /** Der SpieltagSlider, um den gewünschten Spieltag auszuwählen. */
    @FXML
    private SpieltagSlider spieltagSlider;

    @Autowired
    private TeamIconCache teamIconCache;

    private MatchTeamCellFactory homeTeamCellFactory;
    private MatchTeamCellFactory guestTeamCellFactory;

    private Saison season;

    /**
     * Der Konstruktor des ControllerSpieltag. Erzeugt eine neue Instanz.
     */
    public SpieltagController() {
        super("SpieltagPane.fxml");
        EventBus.getDefault().register(this);
    }

    @Override
    protected void postConstruct() {
        homeTeamCellFactory.setTeamIconCache(teamIconCache);
        guestTeamCellFactory.setTeamIconCache(teamIconCache);
    }

    @FXML
    private void initialize() {
        initTabelle();
        spieltagSlider.setOnChangedConsumer(this::spieltagSliderChanged);
    }

    /**
     * Initialisiert die TableView, in der die Spiele des aktuell ausgewählen Spieltages angezeigt werden.
     */
    private void initTabelle() {
        final int fixedCellSize = 48;
        final double columnTeamFactor = 0.4;
        final double columnGoalsFactor = 0.1;

        tableViewSpieltag.setFocusTraversable(false);
        tableViewSpieltag.focusModelProperty().set(null);
        tableViewSpieltag.getColumns().clear();
        tableViewSpieltag.setFixedCellSize(fixedCellSize);
        tableViewSpieltag.setPlaceholder(NO_SEASON_PLACEHOLDER);

        TableColumn<SpielWrapper, Spieler> columnHeim = new TableColumn<>(
                Messages.getString("spieltag.columns.hometeam"));
        columnHeim.prefWidthProperty()
                .bind(tableViewSpieltag.widthProperty().multiply(columnTeamFactor * Constants.COLUMN_WIDTH_FACTOR));
        homeTeamCellFactory = new MatchTeamCellFactory();
        columnHeim.setCellFactory(homeTeamCellFactory);
        columnHeim.setCellValueFactory(new PropertyValueFactory<>("heimSpieler"));

        TableColumn<SpielWrapper, String> columnToreHeim = new TableColumn<>(
                Messages.getString("spieltag.columns.tore"));
        columnToreHeim.prefWidthProperty().bind(tableViewSpieltag.widthProperty()
                .multiply(columnGoalsFactor * Constants.COLUMN_WIDTH_FACTOR));
        columnToreHeim.setCellFactory(new MatchGoalsCellFactory());
        columnToreHeim.setCellValueFactory(new PropertyValueFactory<>("heimTore"));

        TableColumn<SpielWrapper, String> columnToreGast = new TableColumn<>(
                Messages.getString("spieltag.columns.tore"));
        columnToreGast.prefWidthProperty().bind(tableViewSpieltag.widthProperty()
                .multiply(columnGoalsFactor * Constants.COLUMN_WIDTH_FACTOR));
        columnToreGast.setCellFactory(new MatchGoalsCellFactory());
        columnToreGast.setCellValueFactory(new PropertyValueFactory<>("gastTore"));

        TableColumn<SpielWrapper, Spieler> columnGast = new TableColumn<>(
                Messages.getString("spieltag.columns.guestteam"));
        columnGast.prefWidthProperty()
                .bind(tableViewSpieltag.widthProperty().multiply(columnTeamFactor * Constants.COLUMN_WIDTH_FACTOR));
        guestTeamCellFactory = new MatchTeamCellFactory();
        columnGast.setCellFactory(guestTeamCellFactory);
        columnGast.setCellValueFactory(new PropertyValueFactory<>("gastSpieler"));

        tableViewSpieltag.getColumns().addAll(Arrays.asList(columnHeim, columnToreHeim, columnToreGast, columnGast));

        tableViewSpieltag.getColumns().forEach(column -> {
            column.setSortable(false);
            column.setResizable(false);
            column.setReorderable(false);
        });

    }

    /**
     * Aufgerufen durch den Wechsel des Spieltages auf dem SpieltagSlider. Leitet die Änderungen in der Tabelle ein.
     *
     * @param value
     *         die Nummer des neu ausgewählten Spieltages (1 - MAX_MATCHDAYS)
     */
    private void spieltagSliderChanged(int value) {
        tableViewSpieltag.getItems().clear();
        if (season != null) {
            Spieltag spieltag = season.getSpieltag(value);
            if (spieltag != null) {
                ObservableList<SpielWrapper> list = tableViewSpieltag.getItems();
                for (Spiel spiel : spieltag.getSpiele()) {
                    list.add(new SpielWrapper(spiel));
                }
                tableViewSpieltag.setItems(list);
            }
        }
        // avoid reusing rows by refreshing the table view
        tableViewSpieltag.refresh();
    }

    @Subscribe
    @Override
    public void updateView(BavaEvent.SeasonUpdated event) {
        this.season = event.saison();
        Label placeholder = NO_SEASON_PLACEHOLDER;
        int matchdayCount = 0;
        if (season != null) {
            homeTeamCellFactory.setCurrentTeams(season.getTeams());
            guestTeamCellFactory.setCurrentTeams(season.getTeams());
            placeholder = new Label(Messages.getString("spieltag.tableview.placeholder.no-matchday"));
            matchdayCount = season.getSpieltage().size();
        }
        tableViewSpieltag.setPlaceholder(placeholder);
        spieltagSlider.setMaximum(matchdayCount);
        spieltagSliderChanged(spieltagSlider.getIntValue());
    }
}
