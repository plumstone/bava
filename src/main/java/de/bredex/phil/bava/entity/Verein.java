package de.bredex.phil.bava.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

/**
 * Die Klasse Verein. Sie repräsentiert einen Verein in der Saison.
 *
 * @author Philemon Hilscher
 */
@Entity
public class Verein implements Serializable {

    /** The id of the club. */
    @Id
    private Long id;

    /** The name of the club. */
    @Column(unique = true, nullable = false)
    private String name;

    /** The name of the city of the club. */
    @Column(nullable = false)
    private String stadt;

    @Basic
    private String iconUrl;

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Verein other) {
            return Objects.equals(id, other.id);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id
     *         the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name
     *         the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets stadt.
     *
     * @return the stadt
     */
    public String getStadt() {
        return stadt;
    }

    /**
     * Sets stadt.
     *
     * @param stadt
     *         the stadt
     */
    public void setStadt(String stadt) {
        this.stadt = stadt;
    }

    /**
     * Gets icon url.
     *
     * @return the icon url
     */
    public String getIconUrl() {
        return iconUrl;
    }

    /**
     * Sets icon url.
     *
     * @param iconUrl
     *         the icon url
     */
    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
}
