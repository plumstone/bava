package de.bredex.phil.bava.utils;

import de.bredex.phil.bava.components.AlertDialog;
import de.bredex.phil.bava.db.DatabaseExceptionHandler;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.SubscriberExceptionEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

/**
 * The default exception handler for this project.
 *
 * @author Philemon Hilscher
 */
@Component
public class BavaUncaughtExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(BavaUncaughtExceptionHandler.class);
    private final DatabaseExceptionHandler databaseExceptionHandler;

    /**
     * Creates a new {@link BavaUncaughtExceptionHandler}.
     *
     * @param databaseExceptionHandler
     *         the database error handler
     */
    public BavaUncaughtExceptionHandler(DatabaseExceptionHandler databaseExceptionHandler) {
        this.databaseExceptionHandler = databaseExceptionHandler;
        EventBus.getDefault().register(this);
    }

    /**
     * Handles the given {@link Throwable}.
     *
     * @param thread
     *         the thead on which the {@link Throwable} occured.
     * @param throwable
     *         the {@link Throwable} which occured.
     */
    public void handleException(Thread thread, Throwable throwable) {
        Throwable integrityViolation = getCause(throwable, DataIntegrityViolationException.class);
        if (integrityViolation != null) {
            databaseExceptionHandler.handle((DataIntegrityViolationException) integrityViolation);
        } else {
            LOG.error("An unexpected error occurred in {}: {}", thread, throwable);
            AlertDialog.showExceptionDialog(throwable);
        }
    }

    /**
     * Handles an exception which occured in a {@link EventBus} thread.
     *
     * @param event
     *         the {@link SubscriberExceptionEvent}
     */
    @Subscribe
    public void handleException(SubscriberExceptionEvent event) {
        handleException(Thread.currentThread(), event.throwable);
    }

    private <T> boolean isCausedBy(Throwable throwable, Class<T> causingClass) {
        return getCause(throwable, causingClass) != null;
    }

    private <T> Throwable getCause(Throwable throwable, Class<T> causingClass) {
        if (causingClass.isInstance(throwable)) {
            return throwable;
        }
        if (throwable.getCause() != null) {
            return getCause(throwable, causingClass);
        }
        return null;
    }
}
