package de.bredex.phil.bava.web;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.text.MessageFormat;
import java.time.Duration;
import java.time.Year;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Handles the api requests to OpenLigaDB.
 *
 * @author Philemon Hilscher
 */
@Component
public class WebRequest {

    private static final Logger LOG = LoggerFactory.getLogger(WebRequest.class);

    private final int timeout;
    private final String baseUrl;

    /**
     * Instantiates a new Web request.
     *
     * @param timeout
     *         the timeout
     * @param baseUrl
     *         the base url
     */
    public WebRequest(@Value("${web-request.timeout}") int timeout, @Value("${openligadb.api.url}") String baseUrl) {
        this.timeout = timeout;
        this.baseUrl = baseUrl;
    }

    /**
     * Sends a request to get all available teams.
     *
     * @param league
     *         the league
     * @param year
     *         the year
     * @return the {@link HttpResponse}
     * @throws IOException
     *         the io exception
     */
    public HttpResponse<String> getAvailableTeams(int league, Year year) throws IOException {
        return getJsonFromRequest(MessageFormat.format("{0}getavailableteams/bl{1}/{2}", baseUrl, league, year));
    }

    /**
     * Sends a request to get all matchdays.
     *
     * @param league
     *         the league
     * @param year
     *         the year
     * @return the {@link HttpResponse}
     * @throws IOException
     *         the io exception
     */
    public HttpResponse<String> getSpieltage(int league, Year year) throws IOException {
        return getJsonFromRequest(MessageFormat.format("{0}getmatchdata/bl{1}/{2}", baseUrl, league, year));
    }

    private HttpResponse<String> getJsonFromRequest(String urlString) throws IOException {
        try {
            HttpClient client = HttpClient.newHttpClient();

            HttpRequest request = HttpRequest.newBuilder(URI.create(urlString))
                    .header("Content-Type", "application/json")
                    .timeout(Duration.ofMillis(timeout))
                    .GET().build();

            return client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOG.trace("Current request on thread {} has been cancelled.", Thread.currentThread());
        }
        return null;
    }

    /**
     * Gets base url.
     *
     * @return the base url
     */
    public String getBaseUrl() {
        return baseUrl;
    }
}
