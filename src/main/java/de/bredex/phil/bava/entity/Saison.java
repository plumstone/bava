package de.bredex.phil.bava.entity;

import java.time.Year;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import de.bredex.phil.bava.db.YearConverter;
import de.bredex.phil.bava.i18n.Messages;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.util.ObjectUtils;

/**
 * The entity {@link Saison}.
 *
 * @author Philemon Hilscher
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"name", "year"}))
public class Saison {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String name;

    @Basic
    @Convert(converter = YearConverter.class)
    private Year year;

    @Transient
    private Integer league;

    /** Die Liste mit allen Spieltagen einer Saison. */
    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Spieltag> spieltage = new ArrayList<>();

    @OneToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Verein> teams = new ArrayList<>();

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name
     *         the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets year.
     *
     * @return the year
     */
    public Year getYear() {
        return year;
    }

    /**
     * Sets year.
     *
     * @param year
     *         the year
     */
    public void setYear(Year year) {
        this.year = year;
    }

    /**
     * Gets league.
     *
     * @return the league
     */
    public Integer getLeague() {
        return league;
    }

    /**
     * Sets league.
     *
     * @param league
     *         the league
     */
    public void setLeague(Integer league) {
        this.league = league;
    }

    /**
     * Gets spieltage.
     *
     * @return the spieltage
     */
    public List<Spieltag> getSpieltage() {
        return spieltage;
    }

    /**
     * Gets teams.
     *
     * @return the teams
     */
    public List<Verein> getTeams() {
        return teams;
    }

    /**
     * Sets teams.
     *
     * @param teams
     *         the teams
     */
    public void setTeams(List<Verein> teams) {
        this.teams = teams;
    }

    /**
     * Gets spieltag.
     *
     * @param nummer
     *         the number of the Spieltag
     * @return den Spieltag oder null, falls er nicht existiert
     */
    public Spieltag getSpieltag(int nummer) {
        for (Spieltag spieltag : spieltage) {
            if (spieltag.getNummer() == nummer) {
                return spieltag;
            }
        }
        return null;
    }

    /**
     * Adds a matchday to this season.
     *
     * @param spieltag
     *         the matchday to add
     */
    public void addSpieltag(Spieltag spieltag) {
        if (Objects.isNull(spieltage)) {
            spieltage = new ArrayList<>();
        }
        removeSpieltag(spieltag.getNummer());
        spieltage.add(spieltag);
        spieltage = spieltage.stream().sorted(
                Comparator.comparingInt(Spieltag::getNummer)).collect(Collectors.toList());
    }

    /**
     * @param nummer
     *         the number of the Spieltag
     */
    private void removeSpieltag(int nummer) {
        for (Spieltag spieltag : spieltage) {
            if (spieltag.getNummer() == nummer) {
                spieltage.remove(spieltag);
                return;
            }
        }
    }

    @Override
    public String toString() {
        return !ObjectUtils.isEmpty(name) ? name : Messages.getString("stammdaten.season.noname");
    }
}
