package de.bredex.phil.bava.utils;

import java.util.List;

/**
 * The type url escaper.
 *
 * @author Philemon Hilscher
 */
public final class UrlEscaper {

    private static final List<StringPair> CHARACTERS = List.of(
            new StringPair("\u00C4", "%C3%84"),
            new StringPair("\u00D6", "%C3%96"),
            new StringPair("\u00DC", "%C3%9C"),
            new StringPair("\u00E4", "%C3%A4"),
            new StringPair("\u00F6", "%C3%B6"),
            new StringPair("\u00FC", "%C3%BC"),
            new StringPair("\u00DF", "%C3%9F"),
            new StringPair("\u0028", "%28"),
            new StringPair("\u0029", "%29"),
            new StringPair("\u2013", "%E2%80%93")
    );

    private UrlEscaper() {
    }

    /**
     * Escapes a string.
     *
     * @param string
     *         the string
     * @return the string
     */
    public static String escape(final String string) {
        String result = string;
        if (result != null) {
            for (StringPair pair : CHARACTERS) {
                result = result.replace(pair.first(), pair.second());
            }
        }
        return result;
    }

    /**
     * Unescapes a string.
     *
     * @param string
     *         the string
     * @return the string
     */
    public static String unescape(String string) {
        String result = string;
        if (result != null) {
            for (StringPair pair : CHARACTERS) {
                result = result.replace(pair.second(), pair.first());
            }
        }
        return result;
    }

    private record StringPair(String first, String second) {
    }
}
