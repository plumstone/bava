package de.bredex.phil.bava.utils;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import de.bredex.phil.bava.entity.Spiel;
import de.bredex.phil.bava.entity.Spieler;
import de.bredex.phil.bava.entity.Spieltag;
import de.bredex.phil.bava.tabelle.VereinWrapper;

/**
 * The Calculator is a class with extracted methods to calculate the data for the table and the course.
 *
 * @author Philemon Hilscher
 */
public class Calculator {

    private final boolean includeQuickResults;

    /**
     * Instantiates a new Calculator.
     *
     * @param includeQuickResults
     *         the include quick results
     */
    public Calculator(boolean includeQuickResults) {
        this.includeQuickResults = includeQuickResults;
    }

    /**
     * Sortiert die Tabellendaten dem übergebenen Comparator nach.
     *
     * @param comparator
     *         the comparator
     * @param tabellendaten
     *         the table data
     */
    public void calculatePositions(Comparator<VereinWrapper> comparator, List<VereinWrapper> tabellendaten) {
        if (!tabellendaten.isEmpty()) {
            tabellendaten.sort(comparator);
            tabellendaten.get(0).setPosition(1);
            for (int i = 1; i < tabellendaten.size(); i++) {
                VereinWrapper vereinWrapperAbove = tabellendaten.get(i - 1);
                VereinWrapper vereinWrapperCurrent = tabellendaten.get(i);
                int compareResult = comparator.compare(vereinWrapperAbove, vereinWrapperCurrent);
                if (compareResult < Comparators.EQUAL) {
                    vereinWrapperCurrent.setPosition(i + 1);
                } else if (compareResult == Comparators.EQUAL) {
                    vereinWrapperCurrent.setPosition(vereinWrapperAbove.getPosition());
                } else {
                    throw new IllegalStateException("Sort function did not work!");
                }
            }
        }
    }

    /**
     * Calculates the results of the given matchday.
     *
     * @param tabellendaten
     *         the table data
     * @param spieltag
     *         the matchday
     */
    public void calculateSpieltagResults(List<VereinWrapper> tabellendaten, Spieltag spieltag) {
        for (Spiel spiel : spieltag.getSpiele()) {
            if (spiel.isFinished() || (spiel.isStarted() && includeQuickResults)) {
                Spieler spielerHeim = spiel.getHeim();
                Spieler spielerGast = spiel.getGast();
                if (spielerHeim.getTore() == null || spielerGast.getTore() == null) {
                    continue;
                }
                int homePlayerGoals = spielerHeim.getTore();
                int guestPlayerGoals = spielerGast.getTore();

                VereinWrapper vereinHeim = tabellendaten.stream()
                        .filter(vereinWrapper -> Objects.equals(vereinWrapper.getId(), spielerHeim.getVereinId()))
                        .findFirst().orElseThrow();
                vereinHeim.addHeimTore(homePlayerGoals);
                vereinHeim.addHeimGegentore(guestPlayerGoals);

                VereinWrapper vereinGast = tabellendaten.stream()
                        .filter(vereinWrapper -> Objects.equals(vereinWrapper.getId(), spielerGast.getVereinId()))
                        .findFirst().orElseThrow();
                vereinGast.addAuswaertsTore(guestPlayerGoals);
                vereinGast.addAuswaertsGegentore(homePlayerGoals);

                if (homePlayerGoals > guestPlayerGoals) {
                    vereinHeim.addHeimSiege(1);
                    vereinGast.addAuswaertsNiederlagen(1);
                } else if (homePlayerGoals < guestPlayerGoals) {
                    vereinHeim.addHeimNiederlagen(1);
                    vereinGast.addAuswaertsSiege(1);
                } else {
                    vereinHeim.addHeimUnentschieden(1);
                    vereinGast.addAuswaertsUnentschieden(1);
                }
            }
        }
    }
}
