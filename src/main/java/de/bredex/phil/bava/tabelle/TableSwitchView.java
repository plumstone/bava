package de.bredex.phil.bava.tabelle;

import de.bredex.phil.bava.utils.Comparators;

import java.util.Comparator;

/**
 * Das Enum TableSwitchView. Es wird benutzt, um in einigen Funktionen zwischen den Fällen Gesamt, Heim oder Auswärts
 * zu unterscheiden.
 *
 * @author Philemon Hilscher
 */
public enum TableSwitchView {

    /** The case GESAMT. */
    GESAMT(Comparators.GESAMT),

    /** The case HEIM. */
    HEIM(Comparators.HEIM),

    /** The case AUSWAERTS. */
    AUSWAERTS(Comparators.AUSWAERTS);

    private final Comparator<VereinWrapper> comparator;

    TableSwitchView(Comparator<VereinWrapper> comparator) {
        this.comparator = comparator;
    }

    /**
     * Gets comparator.
     *
     * @return the comparator
     */
    public Comparator<VereinWrapper> getComparator() {
        return comparator;
    }
}
