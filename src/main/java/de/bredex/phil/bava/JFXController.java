package de.bredex.phil.bava;

import java.io.IOException;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;

import de.bredex.phil.bava.components.AlertDialog;
import de.bredex.phil.bava.i18n.Messages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract controller for all sub controllers used in this application.
 *
 * @param <T>
 *         The root element type of the view conrtolled by this controller
 * @author Philemon Hilscher
 */
public abstract class JFXController<T extends Pane> {

    private static final Logger LOG = LoggerFactory.getLogger(JFXController.class);

    /** The root pane of this view. */
    private T pane;

    /**
     * Creates a new instance of this controller including the loading of the fxml.
     *
     * @param fxmlPath
     *         The path to the fxml file.
     */
    protected JFXController(String fxmlPath) {
        FXMLLoader fxmlloader = new FXMLLoader(getClass().getResource(fxmlPath), Messages.RESOURCE_BUNDLE, null,
                param -> this);
        try {
            pane = fxmlloader.load();
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
            AlertDialog.showExceptionDialog(e);
        }

        if (Objects.nonNull(pane)) {
            // pane.setStyle("-fx-border-color: black"); // UI debugging helper

            pane.sceneProperty().addListener((observable, oldValue, newValue) -> {
                if (Objects.nonNull(newValue)) {
                    refreshView();
                }
            });
        }
    }

    /**
     * Called when the controller has been successfully instanciated by Spring. Can be used for inital loading of data,
     * etc.
     */
    @PostConstruct
    protected void postConstruct() {
    }

    /**
     * Refreshes the UI Elements on the view belonging to this controller.
     */
    protected void refreshView() {
    }

    /**
     * Gets the pane of this controller.
     *
     * @return The root pane of this view.
     */
    public T getPane() {
        return pane;
    }

}
