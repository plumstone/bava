package de.bredex.phil.bava.verlauf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.BorderPane;

import de.bredex.phil.bava.BavaEvent;
import de.bredex.phil.bava.JFXController;
import de.bredex.phil.bava.QuickResultHandler;
import de.bredex.phil.bava.SeasonManager;
import de.bredex.phil.bava.calculator.MatchdayCountCalculator;
import de.bredex.phil.bava.entity.Saison;
import de.bredex.phil.bava.entity.Spieltag;
import de.bredex.phil.bava.entity.Verein;
import de.bredex.phil.bava.tabelle.VereinWrapper;
import de.bredex.phil.bava.utils.Calculator;
import de.bredex.phil.bava.utils.Comparators;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.springframework.stereotype.Controller;

/**
 * Die Klasse ControllerVerlauf. Sie steuert die Funktionen in der View Verlauf.
 *
 * @author Philemon Hilscher
 */
@Controller
public class VerlaufController extends JFXController<BorderPane> implements SeasonManager, QuickResultHandler {

    /** Die ComboBox um einen Verein zu wählen, dessen Verlauf dann auf der LineChart angezeigt wird. */
    @FXML
    private ComboBox<Verein> cbVereinAuswahl;

    /** Die Achse der LineChart für die Spieltage. */
    @FXML
    private CategoryAxis spieltagsAchse;

    /** Die Achse der LineChart für die Positionen. */
    @FXML
    private CategoryAxis positionsAchse;

    /** Die LineChart, in der die Verlaufskurven der Vereine während einer Saison angezeigt werden. */
    @FXML
    private LineChart<String, String> verlaufLineChart;

    /** This Map contains all Series mapped to each team in this season. */
    private Map<Verein, Series<String, String>> lineGraphs;

    /** Caches the last known season to determine whether it has changed. */
    private Saison currentSeason;

    private boolean showQuickResults;

    /**
     * Der Konstruktor des ControllerVerlauf.
     */
    public VerlaufController() {
        super("VerlaufPane.fxml");
        EventBus.getDefault().register(this);
    }

    @FXML
    private void initialize() {
        initComboBox();
        initLineChart();
    }

    private void initComboBox() {
        cbVereinAuswahl.valueProperty()
                .addListener((obs, oldValue, newValue) -> setDataToChart(newValue));
    }

    /**
     * Initializes the {@link LineChart}.
     */
    private void initLineChart() {
        verlaufLineChart.getStyleClass().add("line-chart");
        verlaufLineChart.setAnimated(false); // evtl animation einfügen (von links nach rechts den graphen aufbauen)
    }

    private void updateLineChart() {
        positionsAchse.getCategories().clear();
        spieltagsAchse.getCategories().clear();
        ObservableList<String> positionLabels = FXCollections.observableArrayList();
        ObservableList<String> spieltagLabels = FXCollections.observableArrayList();
        verlaufLineChart.getData().clear();

        int teamCount = currentSeason != null ? currentSeason.getTeams().size() : 0;

        for (int i = teamCount; i > 0; i--) {
            positionLabels.add(String.valueOf(i));
        }
        int matchdayCount = new MatchdayCountCalculator().calculateMatchdayCount(currentSeason);
        for (int i = 1; i <= matchdayCount; i++) {
            spieltagLabels.add(String.valueOf(i));
        }

        positionsAchse.setCategories(positionLabels);
        spieltagsAchse.setCategories(spieltagLabels);
    }

    private void updateComboBox(List<Verein> teamList) {
        Verein verein = cbVereinAuswahl.getValue();
        cbVereinAuswahl.getItems().setAll(teamList);
        cbVereinAuswahl.getItems().sort(Comparators.VEREIN);
        cbVereinAuswahl.setVisibleRowCount(teamList.size());
        if (!teamList.isEmpty()) {
            if (teamList.contains(verein)) {
                cbVereinAuswahl.getSelectionModel().select(verein);
            } else {
                cbVereinAuswahl.getSelectionModel().selectFirst();
            }
        }
    }

    /**
     * Schreibt die Daten des Verlaufs eines Vereins in die LineChart.
     *
     * @param verein
     *         der Verein, dessen Verlauf dargestellt werden soll
     */
    private void setDataToChart(Verein verein) {
        verlaufLineChart.getData().clear();
        if (verein != null) {
            verlaufLineChart.getData().add(lineGraphs.get(verein));
        }
    }

    /**
     * Wandelt die Tabellendaten in eine Liste um, die die Positionen aller Vereine nach jedem Spieltag enthält.
     *
     * @param vereinsListe
     *         the list of teams
     * @param matchdays
     *         the list of matchdays
     */
    private void berechneVerlaufsgraphen(List<Verein> vereinsListe, List<Spieltag> matchdays) {
        List<VereinWrapper> tabellendaten = new ArrayList<>();
        initLineGraphMap(vereinsListe);
        for (Verein verein : lineGraphs.keySet()) {
            tabellendaten.add(new VereinWrapper(verein));
        }

        for (Spieltag spieltag : matchdays) {
            if (!spieltag.getSpiele().isEmpty() && spieltag.isStarted()) {
                Calculator calculator = new Calculator(showQuickResults);
                calculator.calculateSpieltagResults(tabellendaten, spieltag);
                calculator.calculatePositions(Comparators.GESAMT, tabellendaten);

                for (Verein verein : vereinsListe) {
                    VereinWrapper vereinWrapper = tabellendaten.stream().filter(
                                    vWrapper -> vWrapper.getName().equals(verein.getName())).findFirst()
                            .orElseThrow();
                    Data<String, String> data = new Data<>(String.valueOf(spieltag.getNummer()),
                            String.valueOf(vereinWrapper.getPosition()));
                    ObservableList<Data<String, String>> dataList = lineGraphs.get(verein).getData();
                    if (dataList.size() < spieltag.getNummer()) {
                        dataList.add(data);
                    } else {
                        dataList.get(spieltag.getNummer() - 1).setYValue(data.getYValue());
                    }
                }
            }
        }

    }

    private void initLineGraphMap(List<Verein> vereinsListe) {
        lineGraphs = new HashMap<>();
        for (Verein verein : vereinsListe) {
            lineGraphs.put(verein, new Series<>());
        }
    }

    @Override
    @Subscribe
    public void updateView(BavaEvent.SeasonUpdated event) {
        currentSeason = event.saison();
        List<Verein> teams = new ArrayList<>();
        List<Spieltag> matchdays = new ArrayList<>();
        if (currentSeason != null) {
            teams.addAll(currentSeason.getTeams());
            matchdays.addAll(currentSeason.getSpieltage());
        }
        Platform.runLater(() -> {
            updateLineChart();
            initLineGraphMap(teams);
            berechneVerlaufsgraphen(teams, matchdays);
            updateComboBox(teams);
        });
    }

    @Override
    @Subscribe
    public void showQuickResults(BavaEvent.CheckBoxQuickResultChanged event) {
        this.showQuickResults = event.checked();
        if (currentSeason != null) {
            berechneVerlaufsgraphen(currentSeason.getTeams(), currentSeason.getSpieltage());
        }
        setDataToChart(cbVereinAuswahl.getValue());
    }

}
