create table VEREIN
(
    ID       BIGINT       not null primary key,
    ICON_URL VARCHAR(255),
    NAME     VARCHAR(255) not null
        constraint VEREIN_UK unique,
    STADT    VARCHAR(255)
);
