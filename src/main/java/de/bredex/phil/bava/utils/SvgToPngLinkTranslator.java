package de.bredex.phil.bava.utils;

import java.text.MessageFormat;

/**
 * Workaround component because JavaFX does not support SVG image format out of the box.
 *
 * @author Philemon Hilscher
 */
public class SvgToPngLinkTranslator {

    private static final int DEFAULT_SIZE = 512;

    private final int pngImageSize;

    /**
     * Instantiates a new svg to png link translator.
     */
    public SvgToPngLinkTranslator() {
        this(DEFAULT_SIZE);
    }

    /**
     * Instantiates a new svg to png link translator.
     *
     * @param size
     *         the size
     */
    public SvgToPngLinkTranslator(int size) {
        pngImageSize = size;
    }

    /**
     * Translates the given SVG link into a PNG image link from <a href="https://upload.wikimedia.org">
     * https://upload.wikimedia.org</a>.
     *
     * @param toTranslate
     *         the SVG link
     * @return the PNG link
     */
    public String doWork(String toTranslate) {
        if (toTranslate.endsWith(".svg") && toTranslate.startsWith("https://upload.wikimedia.org")) {
            if (toTranslate.contains("/commons")) {
                toTranslate = toTranslate.replace("commons/", "commons/thumb/");
            }
            if (toTranslate.contains("/de")) {
                toTranslate = toTranslate.replace("de/", "de/thumb/");
            }
            if (toTranslate.contains("/en")) {
                toTranslate = toTranslate.replace("en/", "en/thumb/");
            }

            String postfix = toTranslate.replaceAll(toTranslate.replaceAll("[\\w-]*.svg", ""), "");
            return MessageFormat.format("{0}/{1}px-{2}.png", toTranslate, pngImageSize, postfix);
        }
        return toTranslate;
    }
}
