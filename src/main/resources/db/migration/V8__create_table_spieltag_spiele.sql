create table SPIELTAG_SPIELE
(
    SPIELTAG_ID BIGINT not null,
    SPIELE_ID   BIGINT not null
        constraint SPIELTAG_SPIELE_UK unique,
    constraint SPIELTAG_SPIELE_FK1 foreign key (SPIELE_ID) references SPIEL (ID) on delete cascade,
    constraint SPIELTAG_SPIELE_FK2 foreign key (SPIELTAG_ID) references SPIELTAG (ID) on delete cascade
);
