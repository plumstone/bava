package de.bredex.phil.bava.utils;

import de.bredex.phil.bava.entity.Saison;
import de.bredex.phil.bava.entity.Spiel;
import de.bredex.phil.bava.entity.Spieler;
import de.bredex.phil.bava.entity.Spieltag;
import de.openligadb.api.matchdata.Match;
import de.openligadb.api.matchdata.MatchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * The type Entity vo converter.
 *
 * @author Philemon Hilscher
 */
public final class EntityVoConverter {

    private static final Logger LOG = LoggerFactory.getLogger(EntityVoConverter.class);

    /**
     * Convert saison.
     *
     * @param matches
     *         the matches
     * @param teamCount
     *         the team count
     * @return the saison
     */
    public static Saison convert(List<Match> matches, int teamCount) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("Match count: {}", matches.size());
        }
        Saison saison = new Saison();
        List<Spiel> spielList = new ArrayList<>();
        for (Match match : matches) {
            if (!StringUtils.hasText(saison.getName()) && StringUtils.hasText(match.getLeagueName())) {
                saison.setName(match.getLeagueName());
            }
            Spiel spiel = convert(match);
            spiel.setSpieltagnummer(match.getGroup().getGroupOrderID());
            spielList.add(spiel);
        }

        final int matchdayCount = (teamCount - 1) * 2;
        for (int i = 0; i < matchdayCount; i++) {
            saison.addSpieltag(createSpieltag(i, spielList));
        }
        return saison;
    }

    private static Spieltag createSpieltag(int index, List<Spiel> spielList) {
        Spieltag spieltag = new Spieltag();
        spieltag.setNummer(index + 1);
        List<Spiel> spieleSpieltag = spielList.stream().filter(
                spiel -> spiel.getSpieltagnummer() == spieltag.getNummer()).toList();
        int j = 0;
        for (Spiel spiel : spieleSpieltag) {
            spiel.setNummer(++j);
        }
        spieltag.setSpiele(spieleSpieltag);
        if (LOG.isTraceEnabled()) {
            LOG.trace("Spieltag {}: has started {}, is finished {}",
                    spieltag.getNummer(), spieltag.isStarted(), spieltag.isFinished());
        }
        return spieltag;
    }

    private static Spiel convert(Match match) {
        Spiel spiel = new Spiel();
        MatchResult matchResult = getEndMatchResult(match.getMatchResults());

        LocalDateTime matchStart = LocalDateTime.ofInstant(match.getMatchDateTimeUTC().toInstant(),
                ZoneId.systemDefault());
        boolean hasStarted = matchStart.isBefore(LocalDateTime.now());
        boolean isFinished = match.getMatchIsFinished();
        spiel.setStarted(hasStarted && matchResult.getPointsTeam1() != null);
        spiel.setFinished(hasStarted && isFinished);

        Spieler heim = new Spieler();
        heim.setVereinId((long) match.getTeam1().getTeamId());
        heim.setTore(matchResult.getPointsTeam1());
        spiel.setHeim(heim);

        Spieler gast = new Spieler();
        gast.setVereinId((long) match.getTeam2().getTeamId());
        gast.setTore(matchResult.getPointsTeam2());
        spiel.setGast(gast);

        return spiel;
    }

    private static MatchResult getEndMatchResult(List<MatchResult> matchResults) {
        return matchResults.stream()
                .filter(matchResult -> Objects.equals(matchResult.getResultTypeID(), 2)).findFirst()
                // if the result type id is not present, get match result with the highest goal count (fallback)
                .or(() -> matchResults.stream()
                        .max(Comparator.comparingInt(
                                matchResult -> matchResult.getPointsTeam1() + matchResult.getPointsTeam2())))
                .orElse(new MatchResult());
    }

    private EntityVoConverter() {
    }
}
