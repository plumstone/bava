package de.bredex.phil.bava.utils;

import de.bredex.phil.bava.entity.Verein;
import de.bredex.phil.bava.tabelle.VereinWrapper;

import java.util.Comparator;

/**
 * Comparator utility class.
 *
 * @author Philemon Hilscher
 */
public final class Comparators {

    /** Compare result value BELOW. */
    public static final int BELOW = -1;
    /** Compare result value EQUAL. */
    public static final int EQUAL = 0;
    /** Compare result value ABOVE. */
    public static final int ABOVE = 1;

    /** Comparator to sort the clubs in a ListView or ComboBox. */
    public static final Comparator<Verein> VEREIN = Comparators::compareVerein;

    /** Komparator für die Sortierung der Tabelle nach GESAMT. */
    public static final Comparator<VereinWrapper> GESAMT = Comparators::compareGesamt;

    /** Komparator für die Sortierung der Tabelle nach HEIM. */
    public static final Comparator<VereinWrapper> HEIM = Comparators::compareHeim;

    /** Komparator für die Sortierung der Tabelle nach AUSWÄRTS. */
    public static final Comparator<VereinWrapper> AUSWAERTS = Comparators::compareAuswaerts;

    private Comparators() {
    }

    /**
     * Comparator method for two clubs. Compares the city names.
     *
     * @param verein1
     *         club1
     * @param verein2
     *         club2
     * @return 0 if equal, -1 if club1 is aphabeticly below club2, 1 if club1 is alphabeticly above club2.
     */
    private static int compareVerein(Verein verein1, Verein verein2) {
        int result;
        result = Comparator.<String>naturalOrder().compare(verein1.getStadt(), verein2.getStadt());
        if (result != EQUAL) {
            return result;
        }
        return Comparator.<String>naturalOrder().compare(verein1.getName(), verein2.getName());
    }

    /**
     * Komparator für die Tabelle. Die Sortierkriterien sind: Punkte, Tordifferenz, Tore und Gegentore in dieser
     * Reihenfolge.
     *
     * @param v1
     *         the first Verein wrapped
     * @param v2
     *         the second Verein wrapped
     * @return {@link Comparators#BELOW}, {@link Comparators#EQUAL} or {@link Comparators#ABOVE}
     */
    private static int compareGesamt(VereinWrapper v2, VereinWrapper v1) {
        int result;
        result = Integer.compare(v1.getGesamtPunkte(), v2.getGesamtPunkte());
        if (result != EQUAL) {
            return result;
        }
        result = Integer.compare(v1.getGesamtTordifferenz(), v2.getGesamtTordifferenz());
        if (result != EQUAL) {
            return result;
        }
        result = Integer.compare(v1.getGesamtTore(), v2.getGesamtTore());
        if (result != EQUAL) {
            return result;
        }
        return Integer.compare(v1.getGesamtGegentore(), v2.getGesamtGegentore());
    }

    private static int compareHeim(VereinWrapper v2, VereinWrapper v1) {
        int result;
        result = Integer.compare(v1.getHeimPunkte(), v2.getHeimPunkte());
        if (result != EQUAL) {
            return result;
        }
        result = Integer.compare(v1.getHeimTordifferenz(), v2.getHeimTordifferenz());
        if (result != EQUAL) {
            return result;
        }
        result = Integer.compare(v1.getHeimTore(), v2.getHeimTore());
        if (result != EQUAL) {
            return result;
        }
        return Integer.compare(v1.getHeimGegentore(), v2.getHeimGegentore());
    }

    private static int compareAuswaerts(VereinWrapper v2, VereinWrapper v1) {
        int result;
        result = Integer.compare(v1.getAuswaertsPunkte(), v2.getAuswaertsPunkte());
        if (result != EQUAL) {
            return result;
        }
        result = Integer.compare(v1.getAuswaertsTordifferenz(), v2.getAuswaertsTordifferenz());
        if (result != EQUAL) {
            return result;
        }
        result = Integer.compare(v1.getAuswaertsTore(), v2.getAuswaertsTore());
        if (result != EQUAL) {
            return result;
        }
        return Integer.compare(v1.getAuswaertsGegentore(), v2.getAuswaertsGegentore());
    }

}
