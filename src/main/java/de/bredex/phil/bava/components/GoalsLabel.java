package de.bredex.phil.bava.components;

import java.util.Objects;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Label to display goals in the {@link MatchdayInput}.
 *
 * @author Philemon Hilscher
 */
@SuppressWarnings("java:S110")
public class GoalsLabel extends EntityLabel<Integer> {

    private static final Logger LOG = LoggerFactory.getLogger(GoalsLabel.class);

    private static final DataFormat DATA_FORMAT = new DataFormat(Integer.class.getName());

    /** Default styleclass for this class. */
    public static final String STYLECLASS = "goals-label";

    /**
     * Creates a new {@link GoalsLabel}.
     *
     * @param entity
     *         the entity
     */
    public GoalsLabel(Integer entity) {
        this(entity, String.valueOf(entity));
        setType(Type.SOURCE);
    }

    /**
     * Creates a new {@link GoalsLabel}.
     *
     * @param entity
     *         the entity
     * @param placeholder
     *         the placeholder
     */
    public GoalsLabel(Integer entity, String placeholder) {
        super(entity, placeholder);
        getStyleClass().add(STYLECLASS);
    }

    @Override
    protected void onDragDetected(MouseEvent event) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("drag detected");
        }
        if (Objects.nonNull(getEntity())) {
            initDragboard(startDragAndDrop(TransferMode.MOVE), event, DATA_FORMAT);
            if (Objects.equals(getType(), Type.TARGET)) {
                setTempEntity(getEntity());
                setEntity(null);
            }
        }
        event.consume();
    }

    @Override
    protected void onDragExited(DragEvent event) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("drag exited");
        }
        Object o = event.getDragboard().getContent(DATA_FORMAT);
        if (o instanceof Integer) {
            event.acceptTransferModes(TransferMode.MOVE);
            pseudoClassStateChanged(PSEUDO_CLASS_HOVERED, false);
            if (!event.isDropCompleted() && !Objects.equals(getType(), Type.SOURCE)) {
                setTempEntity(null);
            }
        }
        event.consume();
    }

    @Override
    protected void onDragEntered(DragEvent event) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("drag entered");
        }
        Object o = event.getDragboard().getContent(DATA_FORMAT);
        if (o instanceof Integer goals) {
            var goalsLabel = (GoalsLabel) event.getGestureSource();
            if (Objects.equals(getType(), Type.TARGET)
                    && (Objects.equals(goalsLabel.getType(), Type.SOURCE) || getEntity() == null)) {
                event.acceptTransferModes(TransferMode.MOVE);
                if (Objects.equals(getType(), Type.TARGET)) {
                    pseudoClassStateChanged(PSEUDO_CLASS_HOVERED, true);
                    setTempEntity(goals);
                }
            }
        }
        event.consume();
    }

    @Override
    protected void onDragOver(DragEvent event) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("drag over");
        }
        Object o = event.getDragboard().getContent(DATA_FORMAT);
        if (o instanceof Integer) {
            var goalsLabel = (GoalsLabel) event.getGestureSource();
            if (Objects.equals(getType(), Type.TARGET)
                    && (Objects.equals(goalsLabel.getType(), Type.SOURCE)) || getEntity() == null) {
                event.acceptTransferModes(TransferMode.MOVE);
            }
        }
        event.consume();
    }

    @Override
    protected void onDragDropped(DragEvent event) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("drag dropped");
        }
        Object o = event.getDragboard().getContent(DATA_FORMAT);
        var success = false;
        if (o instanceof Integer goals) {
            var goalsLabel = (GoalsLabel) event.getGestureSource();
            if (Objects.equals(getType(), Type.TARGET)
                    && (Objects.equals(goalsLabel.getType(), Type.SOURCE)) || getEntity() == null) {
                event.acceptTransferModes(TransferMode.MOVE);
                if (Objects.equals(goalsLabel.getType(), Type.TARGET)) {
                    goalsLabel.setEntity(getEntity());
                }
                setTempEntity(null);
                setEntity(goals);
                success = true;
            }
        }
        event.setDropCompleted(success);

        event.consume();
    }

    @Override
    protected void onDragDone(DragEvent event) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("drag done");
        }
    }
}
