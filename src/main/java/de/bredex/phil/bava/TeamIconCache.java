package de.bredex.phil.bava;

import de.bredex.phil.bava.icons.Icons;
import de.bredex.phil.bava.utils.SvgToPngLinkTranslator;
import de.bredex.phil.bava.utils.UrlEscaper;
import javafx.scene.image.Image;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static de.bredex.phil.bava.utils.Constants.TEAM_LOGO_SIZE_LARGE;
import static de.bredex.phil.bava.utils.Constants.TEAM_LOGO_SIZE_SMALL;

/**
 * Cache for all team icons used in the application.
 *
 * @author Philemon Hilscher
 */
@Component
public class TeamIconCache {

    private final Map<String, Image> internalCacheSmall = new HashMap<>();

    private final Map<String, Image> internalCacheLarge = new HashMap<>();

    /**
     * Maps a team icon to a team.
     *
     * @param url
     *         the team
     */
    public void invalidate(String url) {
        internalCacheSmall.remove(url);
        internalCacheLarge.remove(url);
    }

    /**
     * Gets the cached team icon to the given team.
     *
     * @param url
     *         the team to get the icon of
     * @param size
     *         the icon size to request
     * @return the team icon
     */
    public Image get(String url, int size) {
        Image result;
        if (size == TEAM_LOGO_SIZE_LARGE) {
            result = internalCacheLarge.computeIfAbsent(url, verein -> loadTeamLogo(verein, TEAM_LOGO_SIZE_LARGE));
            if (result.isError()) {
                internalCacheLarge.remove(url);
            }
        } else {
            result = internalCacheSmall.computeIfAbsent(url, verein -> loadTeamLogo(verein, TEAM_LOGO_SIZE_SMALL));
            if (result.isError()) {
                internalCacheSmall.remove(url);
            }
        }
        return result;
    }

    private Image loadTeamLogo(String iconUrl, int size) {
        if (iconUrl != null && !iconUrl.isBlank()) {
            if (iconUrl.endsWith(".svg")) {
                iconUrl = new SvgToPngLinkTranslator(size * 2).doWork(iconUrl);
            }
            return new Image(UrlEscaper.escape(iconUrl), size, size, true, true, true);
        }
        return Icons.NO_IMAGE;
    }
}
