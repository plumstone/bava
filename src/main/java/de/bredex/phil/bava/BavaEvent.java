package de.bredex.phil.bava;

import java.time.Year;
import java.util.List;
import javafx.concurrent.Worker;

import de.bredex.phil.bava.entity.Saison;

/**
 * Events to pass with the EventBus.
 *
 * @author Philemon Hilscher
 */
public final class BavaEvent {

    private BavaEvent() {
    }

    public record CheckBoxQuickResultChanged(boolean checked) {
    }

    public record SeasonUpdated(Saison saison) {
    }

    public record ToggleOnlineOfflineMode(boolean online) {
    }

    public record UpdateStateChanged(Worker.State state, Throwable cause) {
    }

    public record UpdateManually(Year year) {
    }

    public record SeasonListFetched(List<Saison> list) {
    }

    public record ApplicationUpdateCheckFinished(boolean updateAvailable) {
    }
}
