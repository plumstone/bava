package de.bredex.phil.bava.i18n;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Messages class for i18n.
 *
 * @author Philemon Hilscher
 */
public final class Messages {

    private static final String BUNDLE_NAME = "de.bredex.phil.bava.i18n.messages";

    /** The resource bundle. */
    public static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

    private Messages() {
    }

    /**
     * Returns the string fitting to the key from the properties file.
     *
     * @param key
     *         the message key
     * @return the string from the properties file
     */
    public static String getString(String key) {
        try {
            return RESOURCE_BUNDLE.getString(key);
        } catch (MissingResourceException e) {
            return '!' + key + '!';
        }
    }

    /**
     * Formats the resource bundle string with the given arguments.
     *
     * @param key
     *         the message key
     * @param args
     *         the arguments to format the string
     * @return the formatted string
     */
    public static String getString(String key, Object... args) {
        return MessageFormat.format(getString(key), args);
    }
}
