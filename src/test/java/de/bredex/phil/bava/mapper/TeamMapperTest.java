package de.bredex.phil.bava.mapper;

import de.bredex.phil.bava.entity.Verein;
import de.openligadb.api.teams.Team;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests the {@link TeamMapper}.
 *
 * @author Philemon Hilscher
 */
class TeamMapperTest {

    private TeamMapper teamMapper;

    @BeforeEach
    void setUp() {
        teamMapper = new TeamMapperImpl();
    }

    @Test
    void testMapNullToVerein() {
        Verein verein = teamMapper.map(null);
        assertNull(verein);
    }

    @Test
    void testMapEmptyTeamToVerein() {
        Team team = new Team();
        Verein verein = teamMapper.map(team);
        assertNotNull(verein);
    }

    @Test
    void testMapTeamToVerein() throws URISyntaxException {
        Team team = new Team();
        team.setTeamId(1000);
        team.setTeamName("Name");
        team.setTeamIconUrl(new URI("https://my-nice-icon-url.com"));
        team.setShortName("City");

        Verein verein = teamMapper.map(team);

        assertNotNull(verein);
        assertEquals(1000, verein.getId());
        assertEquals("Name", verein.getName());
        assertEquals("https://my-nice-icon-url.com", verein.getIconUrl());
        assertEquals("City", verein.getStadt());
    }

    @Test
    void testMapNullToVereinList() {
        List<Verein> vereinList = teamMapper.mapList(List.of());
        assertNotNull(vereinList);
        assertEquals(0, vereinList.size());
    }

    @Test
    void testMapEmptyTeamToVereinList() {
        Team team = new Team();
        List<Verein> vereinList = teamMapper.mapList(List.of(team));
        assertNotNull(vereinList);
        assertEquals(1, vereinList.size());
    }

    @Test
    void testMapTeamListToVereinList() throws URISyntaxException {
        Team team = new Team();
        team.setTeamId(1000);
        team.setTeamName("Name");
        team.setTeamIconUrl(new URI("https://my-nice-icon-url.com"));
        team.setShortName("City");

        List<Verein> vereinList = teamMapper.mapList(List.of(team));
        assertNotNull(vereinList);
        assertEquals(1, vereinList.size());

        Verein verein = vereinList.get(0);
        assertEquals(1000, verein.getId());
        assertEquals("Name", verein.getName());
        assertEquals("https://my-nice-icon-url.com", verein.getIconUrl());
        assertEquals("City", verein.getStadt());
    }
}
