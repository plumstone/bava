package de.bredex.phil.bava.components;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import de.bredex.phil.bava.ApplicationObjects;
import de.bredex.phil.bava.i18n.Messages;
import de.bredex.phil.bava.utils.Constants;

/**
 * The class AlertDialog. Able to show an alert whenever necessary.
 *
 * @author Philemon Hilscher
 */
public final class AlertDialog {

    private AlertDialog() {
    }

    /**
     * Shows an exception dialog, with the current thrown exception and the
     * stacktrace inside a text area.
     *
     * @param t
     *         the throwable
     */
    public static void showExceptionDialog(Throwable t) {
        if (!Platform.isFxApplicationThread()) {
            Platform.runLater(() -> {
                if (!Platform.isNestedLoopRunning()) {
                    implShowExceptionDialog(t);
                }
            });
        } else {
            if (!Platform.isNestedLoopRunning()) {
                implShowExceptionDialog(t);
            }
        }
    }

    private static void implShowExceptionDialog(Throwable t) {
        var alert = new Alert(AlertType.ERROR);
        alert.initOwner(ApplicationObjects.getPrimaryStage());
        alert.setTitle(Messages.getString("alert.exception"));
        alert.setHeaderText(t.getClass().getName());
        alert.setContentText(
                Objects.nonNull(t.getMessage()) ? t.getMessage() : Messages.getString("exception.nomessage"));

        var stringWriter = new StringWriter();
        t.printStackTrace(new PrintWriter(stringWriter));
        var exceptionText = stringWriter.toString();

        var label = new Label(Messages.getString("alert.stacktrace"));

        var textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(false);

        // important for resizing the TextArea vertically
        VBox.setVgrow(textArea, Priority.ALWAYS);

        var emailReport = new ButtonType(Messages.getString("alert.report-error"));

        var expContent = new VBox();
        expContent.getChildren().add(label);
        expContent.getChildren().add(textArea);

        alert.getButtonTypes().add(emailReport);

        alert.getDialogPane().setExpandableContent(expContent);

        Optional<ButtonType> result = alert.showAndWait();

        if (result.isPresent() && result.get().equals(emailReport)) {
            sendErrorReportMail(t);
        }
    }

    private static void sendErrorReportMail(Throwable t) {
        var stack = new StringWriter();
        t.printStackTrace(new PrintWriter(stack));

        var receiver = "incoming+plumstone-bava-4266296-issue-@incoming.gitlab.com";
        var subject = Messages.getString("alert.email.subject", "bava-4266296");
        String body = stack + "\n";

        String mailTo = MessageFormat.format("{0}?subject={1}&body={2}", receiver, subject, body);

        try {
            var uri = new URI("mailto", mailTo, null);
            ApplicationObjects.getApplicationHostServices().showDocument(uri.toString());
        } catch (URISyntaxException e) {
            showExceptionDialog(e);
        }
    }

    /**
     * Shows an information dialog with the given text as the content.
     *
     * @param text
     *         the text to be shown
     */
    public static void showInformationDialog(String text) {
        var alert = new Alert(AlertType.INFORMATION);
        alert.initOwner(ApplicationObjects.getPrimaryStage());
        alert.setTitle(Messages.getString("alert.information"));
        alert.setHeaderText(Constants.EMPTY_STRING);
        alert.setContentText(text);

        alert.showAndWait();
    }

    /**
     * Shows a confirmation dialog.
     *
     * @param headerText
     *         the header text
     * @param messageToConfirm
     *         the message to configrm
     * @param okConsumer
     *         the function to execute on okay
     * @param cancelConsumer
     *         the function to execute on cancel
     * @param payload
     *         the payload
     * @param <T>
     *         the type of the payload
     */
    public static <T> void showConfirmationDialog(String headerText, String messageToConfirm,
            Consumer<T> okConsumer, Consumer<T> cancelConsumer, T payload) {
        var alert = new Alert(AlertType.CONFIRMATION);
        alert.initOwner(ApplicationObjects.getPrimaryStage());
        alert.setTitle(Messages.getString("alert.confirmation"));
        alert.setHeaderText(headerText);
        alert.setContentText(messageToConfirm);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            okConsumer.accept(payload);
        } else {
            cancelConsumer.accept(payload);
        }

    }

}
