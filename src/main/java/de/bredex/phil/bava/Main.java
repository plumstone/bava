package de.bredex.phil.bava;

import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Dedicated main class for being able to launch this application from e.g. the command line.
 *
 * @author Philemon Hilscher
 */
@SpringBootApplication
public class Main {

    /**
     * The main method.
     *
     * @param args
     *         the program arguments
     */
    public static void main(String[] args) {
        Bava.runApplication(args);
    }
}
