package de.bredex.phil.bava;

import java.io.IOException;
import javafx.beans.binding.BooleanBinding;
import javafx.css.PseudoClass;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.util.Callback;

import de.bredex.phil.bava.components.AlertDialog;
import de.bredex.phil.bava.i18n.Messages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type jfx dialog controller.
 *
 * @param <R>
 *         the return type of the dialog
 * @param <P>
 *         the type of the pane
 * @author Philemon Hilscher
 */
public abstract class JFXDialogController<R, P extends Pane> extends Dialog<R> {

    private static final Logger LOG = LoggerFactory.getLogger(JFXDialogController.class);

    private P pane;

    private ButtonType saveButtonType;

    /**
     * Instantiates a new jfx dialog controller.
     *
     * @param fxmlPath
     *         the fxml path
     */
    protected JFXDialogController(String fxmlPath) {
        FXMLLoader fxmlloader = new FXMLLoader(getClass().getResource(fxmlPath), Messages.RESOURCE_BUNDLE, null,
                param -> this);
        try {
            pane = fxmlloader.load();
            getDialogPane().setContent(pane);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
            AlertDialog.showExceptionDialog(e);
        }
        initOwner(ApplicationObjects.getPrimaryStage());
        setResultConverter(resultConverter());
    }

    /**
     * Result converter callback. Each subclass has to provide the implementation to evaluate the result.
     *
     * @return the callback
     */
    protected abstract Callback<ButtonType, R> resultConverter();

    /**
     * Prepares a textfield for showing colored borders depending on the binding.
     *
     * @param textField
     *         the textfield
     * @param binding
     *         the binding
     */
    protected void validateInput(TextField textField, BooleanBinding binding) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            PseudoClass invalidClass = PseudoClass.getPseudoClass("invalid");
            PseudoClass validClass = PseudoClass.getPseudoClass("valid");
            textField.pseudoClassStateChanged(invalidClass, binding.get());
            textField.pseudoClassStateChanged(validClass, !binding.get());
        });
    }

    /**
     * Prepares two buttons (ok, cancel) on the dialog pane.
     *
     * @param binding
     *         the binding to disable the save button
     */
    protected void prepareButtons(BooleanBinding binding) {
        saveButtonType = new ButtonType(Messages.getString("controls.button.speichern"), ButtonBar.ButtonData.OK_DONE);
        getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, saveButtonType);
        getDialogPane().lookupButton(saveButtonType).disableProperty().bind(binding);
    }

    /**
     * Gets the pane.
     *
     * @return the pane
     */
    public P getPane() {
        return pane;
    }

    /**
     * Gets the save button type.
     *
     * @return the save button type
     */
    public ButtonType getSaveButtonType() {
        return saveButtonType;
    }
}
