package de.bredex.phil.bava.tabelle;

import de.bredex.phil.bava.entity.Verein;

import java.util.List;
import java.util.function.Supplier;

import static de.bredex.phil.bava.utils.Constants.POINTS_FOR_WIN;

/**
 * Wrapperklasse für {@link Verein} zur Darstellung in der TableView.
 *
 * @author Philemon Hilscher
 */
public class VereinWrapper {

    private final List<Supplier<Object>> gesamtProperties = List
            .of(this::getPosition, this::getName, this::getGesamtPunkte, this::getGesamtSpiele, this::getGesamtSiege,
                    this::getGesamtUnentschieden, this::getGesamtNiederlagen, this::getGesamtToreFormatted,
                    this::getGesamtTordifferenz);

    private final List<Supplier<Object>> homeProperties = List
            .of(this::getPosition, this::getName, this::getHeimPunkte, this::getHeimSpiele, this::getHeimSiege,
                    this::getHeimUnentschieden, this::getHeimNiederlagen, this::getHeimToreFormatted,
                    this::getHeimTordifferenz);

    private final List<Supplier<Object>> awayProperties = List
            .of(this::getPosition, this::getName, this::getAuswaertsPunkte, this::getAuswaertsSpiele,
                    this::getAuswaertsSiege, this::getAuswaertsUnentschieden, this::getAuswaertsNiederlagen,
                    this::getAuswaertsToreFormatted, this::getAuswaertsTordifferenz);

    private final Long id;
    private int position;
    private final String name;
    private int heimSiege;
    private int heimUnentschieden;
    private int heimNiederlagen;
    private int heimTore;
    private int heimGegentore;
    private int auswaertsSiege;
    private int auswaertsUnentschieden;
    private int auswaertsNiederlagen;
    private int auswaertsTore;
    private int auswaertsGegentore;

    /**
     * Creates a new {@link VereinWrapper}.
     *
     * @param adaptee
     *         the entity to wrap
     */
    public VereinWrapper(Verein adaptee) {
        this.id = adaptee.getId();
        this.name = adaptee.getName();
    }

    /**
     * Resets all values.
     */
    public void reset() {
        position = 0;
        heimSiege = 0;
        heimUnentschieden = 0;
        heimNiederlagen = 0;
        heimTore = 0;
        heimGegentore = 0;
        auswaertsSiege = 0;
        auswaertsUnentschieden = 0;
        auswaertsNiederlagen = 0;
        auswaertsTore = 0;
        auswaertsGegentore = 0;
    }

    /**
     * Gets gesamt punkte.
     *
     * @return the total points fo this team
     */
    public int getGesamtPunkte() {
        return getHeimPunkte() + getAuswaertsPunkte();
    }

    /**
     * Gets gesamt spiele.
     *
     * @return the total played matches
     */
    public int getGesamtSpiele() {
        return getGesamtSiege() + getGesamtUnentschieden() + getGesamtNiederlagen();
    }

    /**
     * Gets gesamt siege.
     *
     * @return the total wins
     */
    public int getGesamtSiege() {
        return getHeimSiege() + getAuswaertsSiege();
    }

    /**
     * Gets gesamt unentschieden.
     *
     * @return the total draws
     */
    public int getGesamtUnentschieden() {
        return getHeimUnentschieden() + getAuswaertsUnentschieden();
    }

    /**
     * Gets gesamt niederlagen.
     *
     * @return the total defeats
     */
    public int getGesamtNiederlagen() {
        return getHeimNiederlagen() + getAuswaertsNiederlagen();
    }

    /**
     * Gets gesamt tore formatted.
     *
     * @return the total goals formatted
     */
    public String getGesamtToreFormatted() {
        return formatGoals(getGesamtTore(), getGesamtGegentore());
    }

    /**
     * Gets gesamt tore.
     *
     * @return the total goals
     */
    public int getGesamtTore() {
        return getHeimTore() + getAuswaertsTore();
    }

    /**
     * Gets gesamt gegentore.
     *
     * @return the total countered goals
     */
    public int getGesamtGegentore() {
        return getHeimGegentore() + getAuswaertsGegentore();
    }

    /**
     * Gets gesamt tordifferenz.
     *
     * @return the total goal difference
     */
    public int getGesamtTordifferenz() {
        return getHeimTordifferenz() + getAuswaertsTordifferenz();
    }

    /**
     * Gets heim punkte.
     *
     * @return the home points
     */
    public int getHeimPunkte() {
        return (heimSiege * POINTS_FOR_WIN) + heimUnentschieden;
    }

    /**
     * Gets heim spiele.
     *
     * @return the home matches
     */
    public int getHeimSpiele() {
        return heimSiege + heimUnentschieden + heimNiederlagen;
    }

    /**
     * Adds home wins.
     *
     * @param heimSiege
     *         the home wins to add
     */
    public void addHeimSiege(int heimSiege) {
        this.heimSiege = getHeimSiege() + heimSiege;
    }

    /**
     * Adds home draws.
     *
     * @param heimUnentschieden
     *         the home draws to add
     */
    public void addHeimUnentschieden(int heimUnentschieden) {
        this.heimUnentschieden = getHeimUnentschieden() + heimUnentschieden;
    }

    /**
     * Adds home defeats.
     *
     * @param heimNiederlagen
     *         the home defeats to add
     */
    public void addHeimNiederlagen(int heimNiederlagen) {
        this.heimNiederlagen = getHeimNiederlagen() + heimNiederlagen;
    }

    /**
     * Gets heim tore formatted.
     *
     * @return the home goals formatted
     */
    public String getHeimToreFormatted() {
        return formatGoals(getHeimTore(), getHeimGegentore());
    }

    /**
     * Adds home goals.
     *
     * @param heimTore
     *         the home goals to add
     */
    public void addHeimTore(int heimTore) {
        this.heimTore = getHeimTore() + heimTore;
    }

    /**
     * Adds home countered goals.
     *
     * @param heimGegentore
     *         the home countergoals to add
     */
    public void addHeimGegentore(int heimGegentore) {
        this.heimGegentore = getHeimGegentore() + heimGegentore;
    }

    /**
     * Gets heim tordifferenz.
     *
     * @return the home goal difference
     */
    public int getHeimTordifferenz() {
        return getHeimTore() - getHeimGegentore();
    }

    /**
     * Gets auswaerts punkte.
     *
     * @return the outwards points
     */
    public int getAuswaertsPunkte() {
        return (getAuswaertsSiege() * POINTS_FOR_WIN) + getAuswaertsUnentschieden();
    }

    /**
     * Gets auswaerts spiele.
     *
     * @return the outwards matches
     */
    public int getAuswaertsSpiele() {
        return getAuswaertsSiege() + getAuswaertsUnentschieden() + getAuswaertsNiederlagen();
    }

    /**
     * Adds outwards wins.
     *
     * @param auswaertsSiege
     *         the outward wins to add
     */
    public void addAuswaertsSiege(int auswaertsSiege) {
        this.auswaertsSiege = getAuswaertsSiege() + auswaertsSiege;
    }

    /**
     * Adds outwards draws.
     *
     * @param auswaertsUnentschieden
     *         the outward draws to add
     */
    public void addAuswaertsUnentschieden(int auswaertsUnentschieden) {
        this.auswaertsUnentschieden = getAuswaertsUnentschieden() + auswaertsUnentschieden;
    }

    /**
     * Adds outwards defeats.
     *
     * @param auswaertsNiederlagen
     *         the outwards defeats to add
     */
    public void addAuswaertsNiederlagen(int auswaertsNiederlagen) {
        this.auswaertsNiederlagen = getAuswaertsNiederlagen() + auswaertsNiederlagen;
    }

    /**
     * Gets auswaerts tore formatted.
     *
     * @return the outwards goals formatted
     */
    public String getAuswaertsToreFormatted() {
        return formatGoals(getAuswaertsTore(), getAuswaertsGegentore());
    }

    /**
     * Adds outwards goals.
     *
     * @param auswaertstore
     *         thae outward goals to add
     */
    public void addAuswaertsTore(int auswaertstore) {
        this.auswaertsTore = getAuswaertsTore() + auswaertstore;
    }

    /**
     * Adds outwards countered goals.
     *
     * @param auswaertsGegentore
     *         the outwards countered goals to add
     */
    public void addAuswaertsGegentore(int auswaertsGegentore) {
        this.auswaertsGegentore = getAuswaertsGegentore() + auswaertsGegentore;
    }

    /**
     * Gets auswaerts tordifferenz.
     *
     * @return the outwards goal difference formatted
     */
    public int getAuswaertsTordifferenz() {
        return getAuswaertsTore() - getAuswaertsGegentore();
    }

    private String formatGoals(int arg1, int arg2) {
        return String.format("%02d:%02d", arg1, arg2);
    }

    /**
     * Retrieves the {@link Supplier}s to the needed properties on the given argument.
     *
     * @param tableSwitchView
     *         the view to show in the table
     * @return the {@link Supplier}s
     */
    public List<Supplier<Object>> getGetters(TableSwitchView tableSwitchView) {
        return switch (tableSwitchView) {
            case GESAMT:
                yield gesamtProperties;
            case HEIM:
                yield homeProperties;
            case AUSWAERTS:
                yield awayProperties;
        };
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets position.
     *
     * @return the position
     */
    public int getPosition() {
        return position;
    }

    /**
     * Sets position.
     *
     * @param position
     *         the position
     */
    public void setPosition(int position) {
        this.position = position;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets heim siege.
     *
     * @return the heim siege
     */
    public int getHeimSiege() {
        return heimSiege;
    }

    /**
     * Gets heim unentschieden.
     *
     * @return the heim unentschieden
     */
    public int getHeimUnentschieden() {
        return heimUnentschieden;
    }

    /**
     * Gets heim niederlagen.
     *
     * @return the heim niederlagen
     */
    public int getHeimNiederlagen() {
        return heimNiederlagen;
    }

    /**
     * Gets auswaerts siege.
     *
     * @return the auswaerts siege
     */
    public int getAuswaertsSiege() {
        return auswaertsSiege;
    }

    /**
     * Gets auswaerts unentschieden.
     *
     * @return the auswaerts unentschieden
     */
    public int getAuswaertsUnentschieden() {
        return auswaertsUnentschieden;
    }

    /**
     * Gets auswaerts niederlagen.
     *
     * @return the auswaerts niederlagen
     */
    public int getAuswaertsNiederlagen() {
        return auswaertsNiederlagen;
    }

    /**
     * Gets heim tore.
     *
     * @return the heim tore
     */
    public int getHeimTore() {
        return heimTore;
    }

    /**
     * Gets heim gegentore.
     *
     * @return the heim gegentore
     */
    public int getHeimGegentore() {
        return heimGegentore;
    }

    /**
     * Gets auswaerts tore.
     *
     * @return the auswaerts tore
     */
    public int getAuswaertsTore() {
        return auswaertsTore;
    }

    /**
     * Gets auswaerts gegentore.
     *
     * @return the auswaerts gegentore
     */
    public int getAuswaertsGegentore() {
        return auswaertsGegentore;
    }
}
