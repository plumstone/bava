package de.bredex.phil.bava.utils;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests for the {@link SvgToPngLinkTranslator}.
 *
 * @author Philemon Hilscher
 */
class SvgToPngLinkTranslatorTest {

    private static final int TEST_IMAGE_SIZE = 256;

    private static Stream<Arguments> provideStringsToTranslate() {
        return Stream.of(
                Arguments.of("https://upload.wikimedia.org/wikipedia/commons/e/e3/SC_Paderborn_07_Logo.svg",
                        "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/SC_Paderborn_07_Logo.svg/" + TEST_IMAGE_SIZE + "px-SC_Paderborn_07_Logo.svg.png"),
                Arguments.of("https://upload.wikimedia.org/wikipedia/de/f/f1/SC-Freiburg_Logo-neu.svg",
                        "https://upload.wikimedia.org/wikipedia/de/thumb/f/f1/SC-Freiburg_Logo-neu.svg/" + TEST_IMAGE_SIZE + "px-SC-Freiburg_Logo-neu.svg.png"),
                Arguments.of("https://upload.wikimedia.org/wikipedia/en/5/53/FC_Cologne_logo.svg",
                        "https://upload.wikimedia.org/wikipedia/en/thumb/5/53/FC_Cologne_logo.svg/" + TEST_IMAGE_SIZE + "px-FC_Cologne_logo.svg.png"),
                Arguments.of("https://upload.wikimedia.org/wikipedia/commons/9/9e/Logo_Mainz_05.svg",
                        "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Logo_Mainz_05.svg/" + TEST_IMAGE_SIZE + "px-Logo_Mainz_05.svg.png"),
                Arguments.of("https://upload.wikimedia.org/wikipedia/de/f/f7/Bayer_Leverkusen_Logo.svg",
                        "https://upload.wikimedia.org/wikipedia/de/thumb/f/f7/Bayer_Leverkusen_Logo.svg/" + TEST_IMAGE_SIZE + "px-Bayer_Leverkusen_Logo.svg.png"),
                Arguments.of("https://upload.wikimedia.org/wikipedia/commons/6/67/Borussia_Dortmund_logo.svg",
                        "https://upload.wikimedia.org/wikipedia/commons/thumb/6/67/Borussia_Dortmund_logo.svg/" + TEST_IMAGE_SIZE + "px-Borussia_Dortmund_logo.svg.png"),
                Arguments.of("https://upload.wikimedia.org/wikipedia/commons/6/64/TSG_Logo-Standard_4c.png",
                        "https://upload.wikimedia.org/wikipedia/commons/6/64/TSG_Logo-Standard_4c.png"),
                Arguments.of("https://www.openligadb.de/images/teamicons/Eintracht_Braunschweig.gif",
                        "https://www.openligadb.de/images/teamicons/Eintracht_Braunschweig.gif"),
                Arguments.of("https://turkgucu.de/i/logo-tm.svg",
                        "https://turkgucu.de/i/logo-tm.svg")
        );
    }

    @ParameterizedTest
    @MethodSource("provideStringsToTranslate")
    void testTranslation(String givenString, String expectedResult) {
        final String actualResult = new SvgToPngLinkTranslator(TEST_IMAGE_SIZE).doWork(givenString);

        assertEquals(expectedResult, actualResult);
    }
}
