package de.bredex.phil.bava.stammdaten.saison;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.fxml.FXML;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.util.Callback;

import de.bredex.phil.bava.JFXDialogController;
import de.bredex.phil.bava.entity.Saison;
import de.bredex.phil.bava.i18n.Messages;
import de.bredex.phil.bava.utils.Constants;

/**
 * The type season dialog controller.
 *
 * @author Philemon Hilscher
 */
public class SeasonDialogController extends JFXDialogController<Saison, GridPane> {

    @FXML
    private TextField tfName;
    @FXML
    private Label labelYear;

    /**
     * Instantiates a new season dialog controller.
     *
     * @param year
     *         the year
     */
    public SeasonDialogController(String year) {
        super("SeasonDialogPane.fxml");
        labelYear.setText(year);
    }

    @FXML
    private void initialize() {
        setTitle(Messages.getString("stammdaten.new"));
        setHeaderText(Messages.getString("stammdaten.season.new.header"));
        Label plus = new Label("+");
        plus.getStyleClass().add("plus-sign");
        setGraphic(plus);

        BooleanBinding tfNameInvalid = Bindings.createBooleanBinding(
                () -> !tfName.isDisabled() && !tfName.getText().matches("^\\w[\\wßÄäÖöÜü ./-]*$"),
                tfName.textProperty());

        validateInput(tfName, tfNameInvalid);
        prepareButtons(tfNameInvalid);
        setOnShown(event -> Platform.runLater(tfName::requestFocus));
    }

    @Override
    protected Callback<ButtonType, Saison> resultConverter() {
        return param -> {
            if (param.equals(getSaveButtonType())) {
                Saison season = new Saison();
                season.setName(tfName.getText().isBlank() ? Constants.EMPTY_STRING : tfName.getText());
                return season;
            }
            return null;

        };
    }
}
