package de.bredex.phil.bava;

/**
 * Interface for all controllers that support quick result handling.
 *
 * @author Philemon Hilscher
 */
public interface QuickResultHandler {

    /**
     * Shows the quick result in the view managed by the controller, depending on the given flag.
     *
     * @param event
     *         payload with information of the new state
     */
    void showQuickResults(BavaEvent.CheckBoxQuickResultChanged event);
}
