package de.bredex.phil.bava.repository;

import de.bredex.phil.bava.entity.Saison;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The season repository is responsible for managing all {@link Saison} objects in the application.
 *
 * @author Philemon Hilscher
 */
@Repository
public interface SeasonRepository extends JpaRepository<Saison, Long> {
}
