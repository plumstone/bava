create table SPIEL
(
    ID          BIGINT  not null primary key,
    NUMMER      INTEGER not null,
    GAST_ID     BIGINT,
    HEIM_ID     BIGINT,
    FINISHED    BOOLEAN not null default false,
    STARTED     BOOLEAN not null default false,
    constraint SPIEL_FK1 foreign key (HEIM_ID) references SPIELER (ID) on delete cascade,
    constraint SPIEL_FK2 foreign key (GAST_ID) references SPIELER (ID) on delete cascade
);
