package de.bredex.phil.bava.utils;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests for the {@link UrlEscaper}.
 *
 * @author Philemon Hilscher
 */
class UrlEscaperTest {

    @ParameterizedTest
    @MethodSource("provideTestData")
    void testEscape(String unescaped, String expected) {
        String escaped = UrlEscaper.escape(unescaped);
        assertEquals(expected, escaped);
    }

    @ParameterizedTest
    @MethodSource("provideTestData")
    void testUnescape(String expected, String escaped) {
        String unescaped = UrlEscaper.unescape(escaped);
        assertEquals(expected, unescaped);
    }

    private static Stream<Arguments> provideTestData() {
        return Stream.of(Arguments.of(
                        "https://upload.wikimedia.org/wikipedia/commons/1/1f/Logo_FC_Bayern_München_(2002–2017).svg",
                        "https://upload.wikimedia.org/wikipedia/commons/1/1f/Logo_FC_Bayern_M%C3%BCnchen_%282002%E2%80%932017%29.svg"),
                Arguments.of(
                        "https://upload.wikimedia.org/wikipedia/commons/f/fe/Türkgücü_München_Logo.svg",
                        "https://upload.wikimedia.org/wikipedia/commons/f/fe/T%C3%BCrkg%C3%BCc%C3%BC_M%C3%BCnchen_Logo.svg"),
                Arguments.of(
                        "https://i.imgur.com/apFwbYZ.png",
                        "https://i.imgur.com/apFwbYZ.png"),
                Arguments.of(
                        "https://upload.wikimedia.org/wikipedia/en/c/c8/Msv_duisburg(new).svg",
                        "https://upload.wikimedia.org/wikipedia/en/c/c8/Msv_duisburg%28new%29.svg"));
    }
}
