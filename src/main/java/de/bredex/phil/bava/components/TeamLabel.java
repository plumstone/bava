package de.bredex.phil.bava.components;

import java.util.Objects;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;

import de.bredex.phil.bava.entity.Verein;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Label for a team to use in the {@link MatchdayInput}.
 *
 * @author Philemon Hilscher
 */
@SuppressWarnings("java:S110")
public class TeamLabel extends EntityLabel<Verein> {

    private static final Logger LOG = LoggerFactory.getLogger(TeamLabel.class);

    private static final DataFormat DATA_FORMAT = new DataFormat(Verein.class.getName());

    private Long vereinId;

    /**
     * Creates a new {@link TeamLabel}.
     *
     * @param verein
     *         the team to bind
     */
    public TeamLabel(Verein verein) {
        this(verein, verein.getName());
        setType(Type.SOURCE);
        this.vereinId = verein.getId();
    }

    /**
     * Creates a new {@link TeamLabel}.
     *
     * @param entity
     *         the team to bind
     * @param placeholder
     *         the placeholder
     */
    public TeamLabel(Verein entity, String placeholder) {
        super(entity, placeholder);
        getStyleClass().add("team-label");
    }

    /**
     * Gets verein id.
     *
     * @return the verein id
     */
    public Long getVereinId() {
        return vereinId;
    }

    @Override
    protected void onDragDetected(MouseEvent event) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("drag detected");
        }
        if (Objects.nonNull(getEntity())) {
            initDragboard(startDragAndDrop(TransferMode.MOVE), event, DATA_FORMAT);
            setTempEntity(getEntity());
            setEntity(null);
        }
        event.consume();
    }

    @Override
    protected void onDragExited(DragEvent event) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("drag exited");
        }
        Object o = event.getDragboard().getContent(DATA_FORMAT);
        if (o instanceof Verein) {
            event.acceptTransferModes(TransferMode.MOVE);
            pseudoClassStateChanged(PSEUDO_CLASS_HOVERED, false);
            if (!event.isDropCompleted()) {
                setTempEntity(null);
            }
        }
        event.consume();
    }

    @Override
    protected void onDragEntered(DragEvent event) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("drag entered");
        }
        Object o = event.getDragboard().getContent(DATA_FORMAT);
        if (o instanceof Verein verein) {
            var teamLabel = (TeamLabel) event.getGestureSource();
            if (Objects.equals(getType(), Type.TARGET)
                    && (Objects.isNull(getEntity()) || !Objects.equals(teamLabel.getType(), Type.SOURCE))
                    || Objects.equals(vereinId, verein.getId())) {
                event.acceptTransferModes(TransferMode.MOVE);
                pseudoClassStateChanged(PSEUDO_CLASS_HOVERED, true);
                setTempEntity(verein);
            }
        }
        event.consume();
    }

    @Override
    protected void onDragOver(DragEvent event) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("drag over");
        }
        Object o = event.getDragboard().getContent(DATA_FORMAT);
        if (o instanceof Verein verein) {
            var teamLabel = (TeamLabel) event.getGestureSource();
            if (Objects.equals(getType(), Type.TARGET)
                    && (Objects.isNull(getEntity()) || !Objects.equals(teamLabel.getType(), Type.SOURCE))
                    || Objects.equals(vereinId, verein.getId())) {
                event.acceptTransferModes(TransferMode.MOVE);
            }
        }
        event.consume();
    }

    @Override
    protected void onDragDropped(DragEvent event) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("drag dropped");
        }
        Object o = event.getDragboard().getContent(DATA_FORMAT);
        var success = false;
        if (o instanceof Verein verein) {
            var teamLabel = (TeamLabel) event.getGestureSource();
            if (Objects.equals(getType(), Type.TARGET) || Objects.equals(this.vereinId, verein.getId())) {
                event.acceptTransferModes(TransferMode.MOVE);
                teamLabel.setEntity(getEntity());
                setTempEntity(null);
                setEntity(verein);
                success = true;
            }
        }
        event.setDropCompleted(success);

        event.consume();
    }

    @Override
    protected void onDragDone(DragEvent event) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("drag done");
        }
        if (!(event.getGestureTarget() instanceof TeamLabel)) {
            Object o = event.getDragboard().getContent(DATA_FORMAT);
            setEntity((Verein) o);
        }
    }

}
