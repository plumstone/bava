package de.bredex.phil.bava.stammdaten.verein;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.FXCollections;
import javafx.collections.transformation.FilteredList;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.util.Duration;

import de.bredex.phil.bava.BavaEvent;
import de.bredex.phil.bava.JFXController;
import de.bredex.phil.bava.SeasonManager;
import de.bredex.phil.bava.TeamIconCache;
import de.bredex.phil.bava.db.DatabaseExceptionHandler;
import de.bredex.phil.bava.entity.Saison;
import de.bredex.phil.bava.entity.Verein;
import de.bredex.phil.bava.i18n.Messages;
import de.bredex.phil.bava.repository.SeasonRepository;
import de.bredex.phil.bava.repository.TeamRepository;
import de.bredex.phil.bava.utils.Comparators;
import de.bredex.phil.bava.utils.Constants;
import de.bredex.phil.bava.utils.UrlEscaper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import static de.bredex.phil.bava.utils.Constants.EMPTY_STRING;
import static de.bredex.phil.bava.utils.Constants.TEAM_LOGO_SIZE_LARGE;

/**
 * Der ControllerStammdatenVerein. Er regelt das Hinzufügen und bearbeiten von Vereinen.
 *
 * @author Philemon Hilscher
 */
@Controller
public class StammdatenVereinController extends JFXController<BorderPane> implements SeasonManager {

    private static final Logger LOG = LoggerFactory.getLogger(StammdatenVereinController.class);

    @FXML
    private TextField search;
    @FXML
    private TextField tfName;
    @FXML
    private TextField tfStadt;
    @FXML
    private TextField tfIconUrl;
    @FXML
    private ImageView imgViewIconUrl;
    @FXML
    private CheckBox playsInSeasonCheckbox;
    @FXML
    private Button btnVereinSpeichern;
    @FXML
    private ListView<Verein> vereineListView;
    @FXML
    private AnchorPane inputPane;

    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private DatabaseExceptionHandler databaseExceptionHandler;
    @Autowired
    private SeasonRepository seasonRepository;
    @Autowired
    private TeamIconCache teamIconCache;

    private boolean online;
    private Optional<Saison> season;

    /**
     * Konstruktor des ControllerStammdatenSpieltag.
     */
    public StammdatenVereinController() {
        super("StammdatenVereinPane.fxml");
        season = Optional.empty();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void postConstruct() {
        setItems(teamRepository.findAll());
    }

    @FXML
    private void initialize() {
        initControls();
    }

    private void initControls() {
        BooleanBinding tfNameInvalid = Bindings.createBooleanBinding(
                () -> !tfName.isDisabled() && (tfName.getText().isEmpty() || !isUniqueName(tfName.getText())),
                tfName.textProperty());

        BooleanBinding tfIconUrlInvalid = Bindings.createBooleanBinding(() -> isInvalidUrl(tfIconUrl.getText()),
                tfIconUrl.textProperty());

        btnVereinSpeichern.disableProperty().bind(tfNameInvalid.or(tfIconUrlInvalid));

        PseudoClass invalidClass = PseudoClass.getPseudoClass("invalid");
        tfName.textProperty().addListener(
                (observable, oldValue, newValue) -> tfName.pseudoClassStateChanged(invalidClass, tfNameInvalid.get()));
        tfIconUrl.textProperty().addListener(
                (observable, oldValue, newValue) -> tfIconUrl.pseudoClassStateChanged(invalidClass,
                        tfIconUrlInvalid.get()));

        playsInSeasonCheckbox.setDisable(true);

        vereineListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
                vereineListViewChanged(newValue));
        vereineListView.setPlaceholder(new Label(Messages.getString("stammdaten.teams.placeholder.no-teams")));
    }

    private void setItems(List<Verein> teams) {
        vereineListView.setItems(FXCollections.observableArrayList());
        vereineListView.getItems().addAll(teams);
        vereineListView.getItems().sort(Comparators.VEREIN);
        initListViewFilter();
    }

    private void initListViewFilter() {
        FilteredList<Verein> filteredData = new FilteredList<>(vereineListView.getItems(), verein -> true);
        search.textProperty().addListener((observable, oldValue, newValue) -> filteredData.setPredicate(verein -> {
            if (ObjectUtils.isEmpty(newValue)) {
                return true;
            }

            String lowerCaseFilterInput = newValue.toLowerCase();
            return contains(verein.getName().toLowerCase(), lowerCaseFilterInput) || contains(
                    verein.getStadt().toLowerCase(), lowerCaseFilterInput);
        }));

        vereineListView.setItems(filteredData);
    }

    private boolean contains(String baseString, String stringToCheck) {
        if (baseString == null) {
            return false;
        }
        return baseString.contains(stringToCheck);
    }

    private boolean isUniqueName(String text) {
        return vereineListView.getItems().stream()
                .filter(verein -> !verein.equals(vereineListView.getSelectionModel().getSelectedItem()))
                .noneMatch(verein -> verein.getName().equals(text));
    }

    private boolean isInvalidUrl(String url) {
        boolean isInvalid = false;
        tfIconUrl.setTooltip(null);
        if (StringUtils.hasText(url)) {
            try {
                new URL(url);
            } catch (MalformedURLException e) {
                tfIconUrl.setTooltip(createTooltip(e));
                isInvalid = true;
            }
        }
        return isInvalid;
    }

    private final Tooltip tooltip = new Tooltip();

    private Tooltip createTooltip(Throwable t) {
        tooltip.setText(t.getMessage());
        tooltip.setShowDelay(Duration.ZERO);
        tooltip.setShowDuration(Duration.INDEFINITE);
        return tooltip;
    }

    private void vereineListViewChanged(Verein selectedItem) {
        inputPane.setDisable(selectedItem == null);
        Optional<Verein> team = Optional.ofNullable(selectedItem);

        tfName.setText(team.map(Verein::getName).orElse(Constants.EMPTY_STRING));
        tfStadt.setText(team.map(Verein::getStadt).orElse(EMPTY_STRING));
        tfIconUrl.setText(team.map(Verein::getIconUrl).map(UrlEscaper::unescape).orElse(EMPTY_STRING));
        imgViewIconUrl.setImage(
                team.map(Verein::getIconUrl).map(s -> teamIconCache.get(s, TEAM_LOGO_SIZE_LARGE)).orElse(null));
        playsInSeasonCheckbox.setSelected(
                season.isPresent() && team.isPresent() && season.get().getTeams().contains(team.get()));
    }

    @FXML
    private void vereinSpeichernClicked() {
        Verein vereinToSave = vereineListView.getSelectionModel().getSelectedItem();

        vereinToSave.setName(tfName.getText());
        vereinToSave.setStadt(tfStadt.getText());
        if (!Objects.equals(vereinToSave.getIconUrl(), UrlEscaper.escape(tfIconUrl.getText()))) {
            vereinToSave.setIconUrl(UrlEscaper.escape(tfIconUrl.getText()));
            // only reload the icon if it has changed
            teamIconCache.invalidate(vereinToSave.getIconUrl());
        }
        vereinToSave.setIconUrl(UrlEscaper.escape(tfIconUrl.getText()));

        if (!online && season.isPresent()) {
            Saison saison = season.get();
            if (playsInSeasonCheckbox.isSelected() && !saison.getTeams().contains(vereinToSave)) {
                saison.getTeams().add(vereinToSave);
                try {
                    LOG.info("Attempting to save season {} with new team {}", saison.getId(), vereinToSave.getId());
                    seasonRepository.save(saison);
                    EventBus.getDefault().post(new BavaEvent.SeasonUpdated(saison));

                } catch (DataIntegrityViolationException e) {
                    databaseExceptionHandler.handle(e);
                }
            } else if (!playsInSeasonCheckbox.isSelected() && saison.getTeams().contains(vereinToSave)) {
                saison.getTeams().remove(vereinToSave);
                try {
                    LOG.info("Attempting to save season {} with removed team {}", saison.getId(), vereinToSave.getId());
                    seasonRepository.save(saison);
                    EventBus.getDefault().post(new BavaEvent.SeasonUpdated(saison));

                } catch (DataIntegrityViolationException e) {
                    databaseExceptionHandler.handle(e);
                }
            }
        }

        try {
            vereinToSave = teamRepository.save(vereinToSave);

            tfIconUrl.setText(UrlEscaper.unescape(vereinToSave.getIconUrl()));
            vereineListView.getItems().sort(Comparators.VEREIN);
            vereineListView.requestFocus();
            vereineListView.scrollTo(vereinToSave);
        } catch (DataIntegrityViolationException e) {
            databaseExceptionHandler.handle(e);
        }
    }

    /**
     * Sets the online status.
     *
     * @param event
     *         the event to trigger this method
     */
    @Subscribe
    public void setOnline(BavaEvent.ToggleOnlineOfflineMode event) {
        this.online = event.online();
    }

    @Subscribe
    @Override
    public void updateView(BavaEvent.SeasonUpdated event) {
        season = Optional.ofNullable(event.saison());
        playsInSeasonCheckbox.setDisable(season.isEmpty() || online);
        if (season.isPresent()) {
            playsInSeasonCheckbox.setSelected(event.saison().getTeams()
                    .contains(vereineListView.getSelectionModel().getSelectedItem()));
        } else {
            playsInSeasonCheckbox.setSelected(false);
        }
    }

    @Override
    protected void refreshView() {
        int selectedIndex = vereineListView.getSelectionModel().getSelectedIndex();
        String searchStr = search.getText();
        search.clear();
        setItems(teamRepository.findAll());
        vereineListView.getSelectionModel().select(selectedIndex);
        search.setText(searchStr);
    }

    @FXML
    private void createTeam() {
        Dialog<Verein> newSeasonDialog = new TeamDialogController();
        boolean isIntegrityViolated;
        do {
            Optional<Verein> result = newSeasonDialog.showAndWait();
            try {
                result.ifPresent(team -> {
                    Verein newTeam = teamRepository.save(team);
                    setItems(teamRepository.findAll());
                    search.clear();
                    vereineListView.getSelectionModel().select(newTeam);
                    vereineListView.requestFocus();
                    vereineListView.scrollTo(newTeam);
                });
                isIntegrityViolated = false;
            } catch (DataIntegrityViolationException e) {
                databaseExceptionHandler.handle(e);
                isIntegrityViolated = true;
            }
        } while (isIntegrityViolated);
    }

}
