package de.bredex.phil.bava.utils;

import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

/**
 * Die Klasse die einige Konstanten enthält.
 *
 * @author Philemon Hilscher
 */
public final class Constants {

    /** The Constant SCREEN_SIZE. */
    private static final Rectangle2D SCREEN_SIZE = Screen.getPrimary().getVisualBounds();

    /** The Constant SCREEN_WIDTH. */
    public static final int SCREEN_WIDTH = (int) (SCREEN_SIZE.getWidth());

    /** The Constant SCREEN_HEIGHT. */
    public static final int SCREEN_HEIGHT = (int) (SCREEN_SIZE.getHeight());

    /** The factor for tables to avoid generating scroll bars. */
    public static final double COLUMN_WIDTH_FACTOR = 0.997;

    /** Placeholder to display, if no goal was scored. */
    public static final String NO_GOALS_PLACEHOLDER = "--";

    /** A constant to hold the empty string. */
    public static final String EMPTY_STRING = "";

    /** The small team logo size constant. */
    public static final int TEAM_LOGO_SIZE_SMALL = 40;

    /** The large team logo size constant. */
    public static final int TEAM_LOGO_SIZE_LARGE = 250;

    /** The progress indicator size constant. */
    public static final int PROGRESS_INDICATOR_SIZE = 25;

    /** The points to get for a won match. */
    public static final int POINTS_FOR_WIN = 3;

    private Constants() {
    }
}
