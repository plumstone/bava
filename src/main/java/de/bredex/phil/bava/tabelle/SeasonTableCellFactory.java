package de.bredex.phil.bava.tabelle;

import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

import java.util.List;
import java.util.function.Supplier;

/**
 * The cell factory for the season table to avoid using the {@link javafx.scene.control.cell.PropertyValueFactory}
 * which uses reflection and therefore slows down the application.
 *
 * @param <T>
 *         The type to display in the particular cell. Most likely of type {@link String} or {@link Integer}.
 * @author Philemon Hilscher
 */
public class SeasonTableCellFactory<T>
        implements Callback<TableColumn<VereinWrapper, T>, TableCell<VereinWrapper, T>> {

    private final TableSwitchView tableSwitchView;

    private final int columnIndex;

    /**
     * Creates a new {@link SeasonTableCellFactory}.
     *
     * @param tableSwitchView
     *         the {@link TableSwitchView}
     * @param columnIndex
     *         the column index
     */
    public SeasonTableCellFactory(TableSwitchView tableSwitchView, int columnIndex) {
        this.tableSwitchView = tableSwitchView;
        this.columnIndex = columnIndex;
    }

    @Override
    @SuppressWarnings("java:S110")
    public TableCell<VereinWrapper, T> call(TableColumn<VereinWrapper, T> param) {
        return new TableCell<>() {

            @Override
            protected void updateItem(T item, boolean empty) {
                super.updateItem(item, empty);
                VereinWrapper vereinWrapper = getTableRow().getItem();
                if (vereinWrapper != null) {
                    List<Supplier<Object>> getters = vereinWrapper.getGetters(tableSwitchView);
                    setText(getters.get(columnIndex).get().toString());
                }
            }
        };
    }
}
