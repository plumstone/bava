## 1.8-RELEASE

- Moved to Java 17.
- Improved the layout of the matchday base data view.
- Added a dark theme with automatic switch depending on the os theme.

#### 1.8.1-RELEASE
- Fixed coloring of update hyperlink for dark theme
- Fixed API calls to GitLab Releases and OpenLigaDB due to changed interfaces

## 1.7-RELEASE

- The application now checks on startup if a new version is available.
- Generate a maven site report in the build.
- Removed the league property of a season in the ui.
- Many internal improvements and optimizations.
- Moved to Java 16.

## 1.6-RELEASE

- The view switching is now animated.
- The application now saves to a database instead of XML files.
- Redesigned the team base data controller for better usability.
- Moved to Java 15.

#### 1.6.1-RELEASE
- Fixed ConstraintViolationException when team IDs in the online data have changed.

## 1.5-RELEASE

- Added a status bar at the bottom to display the web request status.
- Added an event bus for an event based communication between controllers.
- Added team icons in the matchday view.
- Added combobox to the season controller to change the league.
- Migrated project to Java 14.

#### 1.5.1-RELEASE
- Fixed errors which occur when the software is started the first time on any system.

## 1.4-RELEASE

- Added indication for running matches in the matchday view.
- Internal improvements.
- Migrated project to Java 13.

## 1.3-RELEASE

- The built .exe file is now executable.
- Improved the season controller.
	- Switch between online/offline possible.
	- Different seasons can be created and selected.
- Matchday input now rebuilds if a different season is loaded.

## 1.2-RELEASE

- Added a season controller to change the current season.
- Redesigned the matchday view. A matchday can now be edited by drag and drop.
- Application header now shows the current season name.
- Build generates a jar file, which can be run from the command line.
- Changed application icon to a better looking one.

## 1.1-RELEASE

- Migrated project to Java 12.
- Project now uses Spring Boot.
- Added application icon.
- Improved readability by increasing the font size.
- A service now updates the web data repetitively.
- Added quick result toggle.
- Added logging library (slf4j).
- Improved application performance.

## 1.0-SNAPSHOT

- Project is now built with Maven.
- Project is now based on Java 11.
- Added web request of season data.
- Added i18n (supported languages: english, german).
- Project is now version controlled by Git.
