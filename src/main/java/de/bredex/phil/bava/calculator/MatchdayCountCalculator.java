package de.bredex.phil.bava.calculator;

import de.bredex.phil.bava.entity.Saison;

/**
 * Calculator that calculates the matchday count.
 *
 * @author Philemon Hilscher
 */
public class MatchdayCountCalculator {

    /**
     * Calculates the matchday count to the given season.
     *
     * @param season
     *         the season
     * @return the machday count
     */
    public int calculateMatchdayCount(Saison season) {
        int teamCount = season != null ? season.getTeams().size() : 0;
        if (teamCount > 1) {
            return (teamCount - 1) * 2;
        }
        return 0;
    }
}
