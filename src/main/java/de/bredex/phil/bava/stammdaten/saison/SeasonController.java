package de.bredex.phil.bava.stammdaten.saison;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javafx.application.Platform;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.concurrent.Worker;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.util.StringConverter;

import de.bredex.phil.bava.BavaEvent;
import de.bredex.phil.bava.JFXController;
import de.bredex.phil.bava.SeasonManager;
import de.bredex.phil.bava.components.AlertDialog;
import de.bredex.phil.bava.db.DatabaseExceptionHandler;
import de.bredex.phil.bava.entity.Saison;
import de.bredex.phil.bava.i18n.Messages;
import de.bredex.phil.bava.icons.Icons;
import de.bredex.phil.bava.repository.SeasonRepository;
import de.bredex.phil.bava.utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;

/**
 * Controller for the stammdaten season view.
 *
 * @author Philemon Hilscher
 */
@Controller
public class SeasonController extends JFXController<BorderPane> implements SeasonManager {

    private static Year year = evaluateYear();

    @FXML
    private ToggleGroup toggleOnlineOffline;

    @FXML
    private ToggleButton toggleOnline;

    @FXML
    private ToggleButton toggleOffline;

    @FXML
    private Spinner<Year> spinner;

    @FXML
    private AnchorPane seasonEditPane;

    @FXML
    private ButtonBar editOptions;

    @FXML
    private ListView<Saison> seasonList;

    @FXML
    private TextField tfSeasonName;

    @FXML
    private Button btnSaveSaison;

    @FXML
    private Button btnDeleteSaison;

    private final SeasonRepository seasonRepository;

    private final DatabaseExceptionHandler databaseExceptionHandler;

    private BooleanProperty onlineProperty;

    private Saison currentSeason;

    /**
     * Creates a new {@link SeasonController}.
     *
     * @param seasonRepository
     *         the season repository
     * @param databaseExceptionHandler
     *         the database error handler
     */
    public SeasonController(SeasonRepository seasonRepository, DatabaseExceptionHandler databaseExceptionHandler) {
        super("SeasonPane.fxml");
        this.seasonRepository = seasonRepository;
        this.databaseExceptionHandler = databaseExceptionHandler;
        EventBus.getDefault().register(this);
    }

    @Override
    protected void postConstruct() {
        toggleOnline.setSelected(true);
    }

    @FXML
    private void initialize() {
        onlineProperty = new SimpleBooleanProperty();
        onlineProperty.bind(toggleOnline.selectedProperty());
        initSeasonYearSpinner();
        initToggleButtons();
        initListView();

        btnSaveSaison.disableProperty().bind(tfSeasonName.textProperty().isEmpty());
    }

    private void initSeasonYearSpinner() {
        spinner.getStyleClass().add(Spinner.STYLE_CLASS_SPLIT_ARROWS_HORIZONTAL);
        spinner.setValueFactory(new SpinnerValueFactory<>() {
            @Override
            public void decrement(int steps) {
                this.setValue(spinner.getValue().minusYears(steps));
            }

            @Override
            public void increment(int steps) {
                this.setValue(spinner.getValue().plusYears(steps));
            }
        });
        spinner.getValueFactory().setConverter(new StringConverter<>() {
            @Override
            public String toString(Year year) {
                return MessageFormat.format("{0} {1}/{2}", Messages.getString("stammdaten.season"), year,
                        year.plusYears(1));
            }

            @Override
            public Year fromString(String string) {
                return Year.parse(string.replace("[A-Za-z]* ", "").replace("/*", ""));
            }
        });
        spinner.getValueFactory().setValue(evaluateYear());
        spinner.getValueFactory().valueProperty().addListener((observable, oldYear, newYear) -> {
            setYear(newYear);
            if (onlineProperty.get()) {
                EventBus.getDefault().post(new BavaEvent.UpdateManually(newYear));
            } else {
                loadOfflineSeasons(newYear);
            }
        });
    }

    /**
     * Evaluates the year from the current Date. A new Season always begins after the 1st of July, otherwise this
     * needs to be adjusted. The Year returned should always be the year the last season was started in. <p/> e.g. 15th
     * of October -> return this year, 3rd of April -> return last year
     *
     * @return the year according to the rule above
     */
    private static Year evaluateYear() {
        if (Month.from(LocalDate.now()).getValue() < Month.JULY.getValue()) {
            return Year.now().minusYears(1);
        }
        return Year.now();
    }

    /**
     * Gets year.
     *
     * @return the year
     */
    public static Year getYear() {
        return year;
    }

    /**
     * Sets year.
     *
     * @param year
     *         the year
     */
    public static void setYear(Year year) {
        SeasonController.year = year;
    }

    private void initListView() {
        seasonList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null) {
                tfSeasonName.clear();
            } else {
                tfSeasonName.setText(newValue.getName());
            }
            currentSeason = newValue;
        });
        BooleanBinding disableProperty =
                seasonList.getSelectionModel().selectedItemProperty().isNull().or(onlineProperty);
        seasonEditPane.disableProperty().bind(disableProperty);
        btnDeleteSaison.disableProperty().bind(disableProperty);
        editOptions.disableProperty().bind(onlineProperty);
    }

    private void initToggleButtons() {
        EventHandler<Event> toggleButtonDeselectEventHandler = event -> {
            if (event.getSource() instanceof ToggleButton toggleButton && toggleButton.isSelected()) {
                event.consume();
            }
        };
        toggleOnline.addEventFilter(Event.ANY, toggleButtonDeselectEventHandler);
        toggleOffline.addEventFilter(Event.ANY, toggleButtonDeselectEventHandler);
        toggleOnlineOffline.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (Objects.equals(toggleOnline, newValue)) {
                setOnline(true);
                seasonList.getSelectionModel().clearSelection();
            } else if (Objects.equals(toggleOffline, newValue)) {
                setOnline(false);
            } else {
                throw new IllegalStateException("this toggle is not handled");
            }
        });
    }

    private void setOnline(boolean isOnline) {
        if (isOnline) {
            EventBus.getDefault().post(new BavaEvent.UpdateManually(year));
        } else {
            loadOfflineSeasons(year);
            seasonList.setPlaceholder(new Label(Messages.getString("stammdaten.season.list.placeholder")));
        }
        EventBus.getDefault().post(new BavaEvent.ToggleOnlineOfflineMode(isOnline));
    }

    private void loadOfflineSeasons(Year newYear) {
        List<Saison> seasons = seasonRepository.findAll().stream()
                .filter(saison -> saison.getYear() == null || saison.getYear().equals(newYear))
                .toList();
        seasonList.setItems(FXCollections.observableArrayList(seasons));
    }

    @Override
    @Subscribe
    public void updateView(BavaEvent.SeasonUpdated event) {
        if (onlineProperty.get()) {
            currentSeason = event.saison();
            tfSeasonName.setText(
                    Optional.ofNullable(currentSeason).map(Saison::getName).orElse(Constants.EMPTY_STRING));
        }
    }

    /**
     * Triggered method when the season should be updated manually.
     *
     * @param event
     *         the event to pass
     */
    @Subscribe
    public void updateManually(BavaEvent.UpdateManually event) {
        seasonList.getItems().clear();
        if (onlineProperty.get()) {
            seasonList.setPlaceholder(new ProgressIndicator());
        }
    }

    /**
     * Triggered method when sesason update state changes.
     *
     * @param event
     *         the event to pass
     */
    @Subscribe
    public void updateStateChanged(BavaEvent.UpdateStateChanged event) {
        Node placeholder = seasonList.getPlaceholder();
        if (event.state() == Worker.State.FAILED) {
            placeholder = new ImageView(Icons.NO_NETWORK);
        } else if (event.state() == Worker.State.SUCCEEDED) {
            placeholder = new Label(Messages.getString("stammdaten.season.list.placeholder"));
        }
        Node finalPlaceholder = placeholder;
        Platform.runLater(() -> seasonList.setPlaceholder(finalPlaceholder));
    }

    /**
     * Triggered method when the season list was fetched.
     *
     * @param event
     *         the event to pass
     */
    @Subscribe
    public void seasonListFetched(BavaEvent.SeasonListFetched event) {
        if (onlineProperty.get()) {
            if (seasonList.getItems() == null) {
                seasonList.setItems(FXCollections.observableArrayList(event.list()));
            } else {
                seasonList.getItems().setAll(event.list());
            }
        }
    }

    @FXML
    private void reloadData() {
        currentSeason = seasonList.getSelectionModel().getSelectedItem();
        if (Objects.nonNull(currentSeason)) {
            setYear(currentSeason.getYear());
        }
        EventBus.getDefault().post(new BavaEvent.SeasonUpdated(currentSeason));
    }

    @FXML
    private void createSeason() {
        Dialog<Saison> newSeasonDialog = new SeasonDialogController(spinner.getEditor().getText());
        boolean isIntegrityViolated;
        do {
            Optional<Saison> result = newSeasonDialog.showAndWait();
            try {
                result.ifPresent(season -> {
                    season.setYear(year);
                    currentSeason = seasonRepository.save(season);
                    seasonList.getItems().add(currentSeason);
                    seasonList.getSelectionModel().select(currentSeason);
                    seasonList.requestFocus();
                });
                isIntegrityViolated = false;
            } catch (DataIntegrityViolationException e) {
                databaseExceptionHandler.handle(e);
                isIntegrityViolated = true;
            }
        } while (isIntegrityViolated);

    }

    @FXML
    private void saveSeason() {
        String oldName = currentSeason.getName();
        try {
            currentSeason.setName(tfSeasonName.getText());
            seasonRepository.save(currentSeason);
            seasonList.refresh();
            seasonList.requestFocus();
        } catch (DataIntegrityViolationException e) {
            databaseExceptionHandler.handle(e);
            currentSeason.setName(oldName);
            tfSeasonName.requestFocus();
            tfSeasonName.setText(oldName);
        }

    }

    @FXML
    private void deleteSeason() {
        Saison seasonToDelete = seasonList.getSelectionModel().getSelectedItem();
        AlertDialog.showConfirmationDialog(Messages.getString("stammdaten.season.delete.header"),
                Messages.getString("stammdaten.season.delete.confirmation", seasonToDelete.getName()), saison -> {
                    seasonRepository.delete(saison);
                    seasonList.getItems().remove(seasonToDelete);
                    EventBus.getDefault().post(new BavaEvent.SeasonUpdated(null));
                }, saison -> {
                }, seasonToDelete);
    }
}
