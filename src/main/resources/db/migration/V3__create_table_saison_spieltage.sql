create table SAISON_SPIELTAGE
(
    SAISON_ID    BIGINT not null,
    SPIELTAGE_ID BIGINT not null
        constraint SAISON_SPIELTAGE_UK1 unique,
    constraint SAISON_SPIELTAGE_FK1 foreign key (SAISON_ID) references SAISON (ID) on delete cascade,
    constraint SAISON_SPIELTAGE_FK2 foreign key (SPIELTAGE_ID) references SPIELTAG (ID) on delete cascade
);
