package de.bredex.phil.bava.utils;

import java.util.Locale;

import de.bredex.phil.bava.ApplicationObjects;
import de.bredex.phil.bava.entity.Saison;
import de.bredex.phil.bava.i18n.Messages;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests for the {@link StageTitleBuilder}.
 *
 * @author Philemon Hilscher
 */
class StageTitleBuilderTest {

    private static String windowHeader;

    @BeforeAll
    static void init() {
        Locale.setDefault(Locale.GERMANY);
        windowHeader = Messages.getString("primarystage.title") + " v" + ApplicationObjects.getBuildInfo()
                .getProperty("build.version");
    }

    @Test
    void testNoSeason() {
        final String buildtTitle = new StageTitleBuilder().build(null);
        assertEquals(windowHeader, buildtTitle);
    }

    @Test
    void testSeasonEmptyName() {
        Saison saison = new Saison();
        final String name = "";
        saison.setName(name);
        final String buildtTitle = new StageTitleBuilder().build(saison);
        assertEquals(windowHeader, buildtTitle);
    }

    @Test
    void testWithSeason() {
        Saison saison = new Saison();
        final String name = "Saison Name 1234";
        saison.setName(name);
        String buildtTitle = new StageTitleBuilder().build(saison);
        assertEquals(windowHeader + " - " + name, buildtTitle);
    }
}
