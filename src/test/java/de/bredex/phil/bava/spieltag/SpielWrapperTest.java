package de.bredex.phil.bava.spieltag;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.bredex.phil.bava.entity.Saison;
import de.bredex.phil.bava.entity.Spiel;
import de.bredex.phil.bava.entity.Spieler;
import de.bredex.phil.bava.entity.Spieltag;
import de.bredex.phil.bava.utils.Constants;
import de.openligadb.api.matchdata.Match;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static de.bredex.phil.bava.utils.EntityVoConverter.convert;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests for the {@link SpielWrapper}.
 *
 * @author Philemon Hilscher
 */
class SpielWrapperTest {

    private static final List<Spiel> MATCHES = new ArrayList<>();

    @BeforeAll
    static void init() throws URISyntaxException, IOException {
        final int teamCount = 18;
        final URL matches = SpielWrapperTest.class.getResource("matches_19_20.json");
        String json = Files.readString(Path.of(Objects.requireNonNull(matches).toURI()));
        ObjectMapper objectMapper = new ObjectMapper();

        List<Match> matchList = Arrays.asList(objectMapper.readValue(json, Match[].class));
        Saison saison = convert(matchList, teamCount);
        for (Spieltag spieltag : saison.getSpieltage()) {
            MATCHES.addAll(spieltag.getSpiele());
        }

        Spiel spiel = new Spiel();
        spiel.setHeim(new Spieler());

        MATCHES.add(spiel);
    }

    private static Stream<Spiel> provideMatches() {
        return MATCHES.stream();
    }

    @ParameterizedTest
    @MethodSource("provideMatches")
    void testWrapping(Spiel spiel) {
        SpielWrapper spielWrapper = new SpielWrapper(spiel);
        assertEquals(spielWrapper.getHeimSpieler(), spiel.getHeim());
        assertEquals(spielWrapper.getGastSpieler(), spiel.getGast());

        assertEquals(spielWrapper.isStarted(), spiel.isStarted());
        assertEquals(spielWrapper.isFinished(), spiel.isFinished());

        if (spiel.getHeim() != null) {
            if (spiel.getHeim().getTore() == null) {
                assertEquals(Constants.NO_GOALS_PLACEHOLDER, spielWrapper.getHeimTore());
            } else {
                assertEquals(spielWrapper.getHeimTore(), String.valueOf(spiel.getHeim().getTore()));
            }
        } else {
            assertEquals(Constants.EMPTY_STRING, spielWrapper.getHeimTore());
        }

        if (spiel.getGast() != null) {
            if (spiel.getGast().getTore() == null) {
                assertEquals(Constants.NO_GOALS_PLACEHOLDER, spielWrapper.getGastTore());
            } else {
                assertEquals(spielWrapper.getGastTore(), String.valueOf(spiel.getGast().getTore()));
            }
        } else {
            assertEquals(Constants.EMPTY_STRING, spielWrapper.getGastTore());
        }
    }
}
