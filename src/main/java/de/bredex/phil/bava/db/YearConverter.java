package de.bredex.phil.bava.db;

import java.time.Year;
import java.util.Optional;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Converter for a {@link Year} to save the object as an {@link Integer} in the database.
 *
 * @author Philemon Hilscher
 */
@Converter(autoApply = true)
public class YearConverter implements AttributeConverter<Year, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Year year) {
        return Optional.of(year).map(Year::getValue).orElse(null);
    }

    @Override
    public Year convertToEntityAttribute(Integer year) {
        return Optional.ofNullable(year).map(Year::of).orElse(null);
    }
}
