package de.bredex.phil.bava.stammdaten.spieltag;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import de.bredex.phil.bava.BavaEvent;
import de.bredex.phil.bava.JFXController;
import de.bredex.phil.bava.SeasonManager;
import de.bredex.phil.bava.calculator.MatchdayCountCalculator;
import de.bredex.phil.bava.components.EntityLabel;
import de.bredex.phil.bava.components.GoalsLabel;
import de.bredex.phil.bava.components.MatchdayInput;
import de.bredex.phil.bava.components.SpieltagSlider;
import de.bredex.phil.bava.components.TeamLabel;
import de.bredex.phil.bava.entity.Saison;
import de.bredex.phil.bava.entity.Spiel;
import de.bredex.phil.bava.entity.Spieltag;
import de.bredex.phil.bava.entity.Verein;
import de.bredex.phil.bava.repository.SeasonRepository;
import de.bredex.phil.bava.repository.TeamRepository;
import de.bredex.phil.bava.utils.Comparators;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * Der Controller StammdatenSpieltag. Er regelt das Hinzufügen und Bearbeiten von Spieltagen.
 *
 * @author Philemon Hilscher
 */
@Controller
public class StammdatenSpieltagController extends JFXController<BorderPane> implements SeasonManager {

    @FXML
    private SpieltagSlider spieltagSlider;

    @FXML
    private VBox vereineLeft;

    @FXML
    private VBox vereineRight;

    @FXML
    private HBox tore;

    @FXML
    private MatchdayInput matchdayInput;

    private int matchdayNumber;
    private boolean online;

    private Saison season;

    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private SeasonRepository seasonRepository;

    /**
     * Constructor of the controller for the StammdatenSpieltag view.
     */
    public StammdatenSpieltagController() {
        super("StammdatenSpieltagPane.fxml");
        EventBus.getDefault().register(this);
    }

    @FXML
    private void initialize() {
        final int initialGoalCount = 5;
        final int maxGoalCount = 20;

        spieltagSlider.setOnChangedConsumer(this::loadMatchday);

        for (int i = 0; i <= initialGoalCount; i++) {
            GoalsLabel label = new GoalsLabel(i);
            label.getStyleClass().add(GoalsLabel.STYLECLASS);
            tore.getChildren().add(label);
        }
        Label plusLabel = new Label("+|\u2013");
        plusLabel.getStyleClass().addAll(EntityLabel.STYLECLASS, "plus-label");
        plusLabel.pseudoClassStateChanged(EntityLabel.PSEUDO_CLASS_FILLED, true);
        plusLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (event.getButton() == MouseButton.PRIMARY) {
                int index = tore.getChildren().size() - 1;
                if (index <= maxGoalCount) {
                    GoalsLabel label = new GoalsLabel(index);
                    label.getStyleClass().add(GoalsLabel.STYLECLASS);
                    tore.getChildren().add(index, label);
                }
            } else if (event.getButton() == MouseButton.SECONDARY
                    && tore.getChildren().size() > 2) {
                tore.getChildren().remove(tore.getChildren().size() - 2);
            }
        });
        tore.getChildren().add(plusLabel);
        tore.setVisible(false);
    }

    private void initTeamLabels() {
        vereineLeft.getChildren().clear();
        vereineRight.getChildren().clear();
        if (season == null) {
            return;
        }
        List<Verein> teamList = season.getTeams();
        teamList.sort(Comparators.VEREIN);
        for (Verein verein : teamList) {
            TeamLabel teamLabel = new TeamLabel(verein);
            VBox.setVgrow(teamLabel, Priority.ALWAYS);
            teamLabel.setMaxWidth(Double.MAX_VALUE);
            teamLabel.setMinWidth(Double.MIN_VALUE);
            if (vereineLeft.getChildren().size() == vereineRight.getChildren().size()) {
                vereineLeft.getChildren().add(teamLabel);
            } else {
                vereineRight.getChildren().add(teamLabel);
            }
        }
        resetDragDropLabels();
    }

    private void loadMatchday(int number) {
        resetDragDropLabels();
        if (season != null) {
            matchdayNumber = number;
            Spieltag spieltag = season.getSpieltag(number);
            for (int i = 0; i < matchdayInput.getMatchCount(); i++) {
                Spiel spiel = null;
                Verein home = null;
                Verein guest = null;
                if (spieltag != null) {
                    spiel = spieltag.getSpiel(i + 1);
                    if (spiel != null) {
                        home = teamRepository.findById(spiel.getHeim().getVereinId()).orElseThrow();
                        guest = teamRepository.findById(spiel.getGast().getVereinId()).orElseThrow();
                    }
                }

                matchdayInput.setMatch(i, spiel, home, guest);
            }
        }
        syncTeamLabelSource();
    }

    private void syncTeamLabelSource() {
        Set<Verein> enteredTeams = new HashSet<>();
        for (TeamLabel teamLabel : matchdayInput.getTeamLabels()) {
            if (teamLabel.getEntity() != null) {
                enteredTeams.add(teamLabel.getEntity());
            }
        }

        for (TeamLabel teamLabel : getTeamLabelSources()) {
            if (enteredTeams.stream().anyMatch(verein -> verein.equals(teamLabel.getEntity()))) {
                teamLabel.setEntity(null);
            }
        }

    }

    private void resetDragDropLabels() {
        matchdayInput.reset();

        getTeamLabelSources().forEach(teamLabel -> teamLabel.setEntity(season.getTeams().stream()
                .filter(verein -> Objects.equals(verein.getId(), teamLabel.getVereinId())).findFirst().orElseThrow()));
    }

    private List<TeamLabel> getTeamLabelSources() {
        List<TeamLabel> result = new ArrayList<>();
        List<Node> labelChildren = Stream.of(vereineLeft.getChildren(), vereineRight.getChildren())
                .flatMap(Collection::stream).toList();
        for (Node node : labelChildren) {
            result.add((TeamLabel) node);
        }
        return result;
    }

    @FXML
    private void spieltagSpeichernClicked() {
        if (!online) {
            Spieltag spieltag = matchdayInput.getMatchday();
            spieltag.setNummer(matchdayNumber);
            season.addSpieltag(spieltag);
            seasonRepository.save(season);
            EventBus.getDefault().post(new BavaEvent.SeasonUpdated(season));
        }
    }

    /**
     * Sets the online status.
     *
     * @param event
     *         the event to trigger this method
     */
    @Subscribe
    public void setOnline(BavaEvent.ToggleOnlineOfflineMode event) {
        this.online = event.online();
    }

    @Override
    @Subscribe
    public void updateView(BavaEvent.SeasonUpdated event) {
        season = event.saison();
        Platform.runLater(() -> {
            initTeamLabels();
            resetDragDropLabels();
            int matchdayCount = 0;
            int matchCount = 0;
            if (season != null) {
                matchdayCount = new MatchdayCountCalculator().calculateMatchdayCount(season);
                matchCount = season.getTeams().size() / 2;
            }
            matchdayInput.setMatchCount(matchCount);
            matchdayInput.rebuild();
            tore.setVisible(season != null);
            spieltagSlider.setMaximum(matchdayCount);
            if (matchdayCount > 0 && season.getSpieltage() != null) {
                loadMatchday(spieltagSlider.getIntValue());
            }
        });
    }

    @Override
    protected void refreshView() {
        if (spieltagSlider.getIntValue() == 1 && season != null) {
            long currentMatchday = season.getSpieltage().stream().filter(Spieltag::isStarted).count();
            spieltagSlider.setValue(currentMatchday);
        }
    }
}
