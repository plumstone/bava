package de.bredex.phil.bava.spieltag;

import java.util.List;
import java.util.Objects;
import javafx.application.Platform;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.image.ImageView;
import javafx.util.Callback;

import de.bredex.phil.bava.TeamIconCache;
import de.bredex.phil.bava.entity.Spieler;
import de.bredex.phil.bava.entity.Verein;

import static de.bredex.phil.bava.utils.Constants.TEAM_LOGO_SIZE_SMALL;

/**
 * Cell factory for the cells that show a team in the matchday overview. The team logo will be shown next to the team
 * name if there exists one (online data).
 *
 * @author Philemon Hilscher
 */
public class MatchTeamCellFactory
        implements Callback<TableColumn<SpielWrapper, Spieler>, TableCell<SpielWrapper, Spieler>> {

    private static final int GRAPHIC_TEXT_GAP = 10;

    private List<Verein> currentTeams;

    private TeamIconCache teamIconCache;

    @Override
    @SuppressWarnings("java:S110")
    public TableCell<SpielWrapper, Spieler> call(TableColumn<SpielWrapper, Spieler> param) {
        return new TableCell<>() {

            @Override
            protected void updateItem(Spieler item, boolean empty) {
                super.updateItem(item, empty);
                if (Objects.nonNull(getTableRow())) {
                    SpielWrapper spiel = getTableRow().getItem();
                    if (Objects.nonNull(item)) {
                        Verein verein = currentTeams.stream().filter(v -> Objects.equals(v.getId(), item.getVereinId()))
                                .findFirst().orElseThrow();
                        if (Objects.nonNull(spiel) && Objects.nonNull(verein.getIconUrl())) {
                            Platform.runLater(
                                    () -> setGraphic(new ImageView(
                                            teamIconCache.get(verein.getIconUrl(), TEAM_LOGO_SIZE_SMALL))));
                            setGraphicTextGap(GRAPHIC_TEXT_GAP);
                        } else {
                            setGraphicTextGap(0);
                            setGraphic(null);
                        }
                        setText(verein.getName());
                    } else {
                        setText(null);
                        setGraphic(null);
                    }
                }
            }
        };
    }

    /**
     * Sets the current teams to be used in the table.
     *
     * @param currentTeams
     *         the list of teams in the current season
     */
    public void setCurrentTeams(List<Verein> currentTeams) {
        this.currentTeams = currentTeams;
    }

    /**
     * Sets the team icon cache.
     *
     * @param teamIconCache
     *         the team icon cache
     */
    public void setTeamIconCache(TeamIconCache teamIconCache) {
        this.teamIconCache = teamIconCache;
    }
}
