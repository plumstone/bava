create table SAISON_TEAMS
(
    SAISON_ID BIGINT not null,
    TEAMS_ID  BIGINT not null,
    constraint SAISON_TEAMS_FK1 foreign key (TEAMS_ID) references VEREIN (ID) on delete cascade,
    constraint SAISON_TEAMS_FK2 foreign key (SAISON_ID) references SAISON (ID) on delete cascade
);
