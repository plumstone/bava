create table SAISON
(
    ID     BIGINT       not null primary key,
    NAME   VARCHAR(255) not null
        constraint SAISON_UK1 unique,
    YEAR   INTEGER,
    LEAGUE INTEGER,
    constraint SAISON_UK2 unique (NAME, YEAR)
);
