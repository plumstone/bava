package de.bredex.phil.bava.icons;

import javafx.scene.image.Image;

import static de.bredex.phil.bava.utils.Constants.TEAM_LOGO_SIZE_LARGE;

/**
 * Utility class which contains some icons for the application.
 *
 * @author Philemon Hilscher
 */
public final class Icons {

    /** The APPLICATION_ICON. */
    public static final Image APPLICATION_ICON = create("soccer_ball2.png");

    /** The ADD icon. */
    public static final Image ADD = create("plus_512x512.png", 32, 32);

    /** The NO_NETWORK icon. */
    public static final Image NO_NETWORK = create("no-network.png", 64, 64);

    /** The NO_IMAGE icon. */
    public static final Image NO_IMAGE = create("no-image-icon-6.png", TEAM_LOGO_SIZE_LARGE,
            TEAM_LOGO_SIZE_LARGE);

    private static Image create(String name) {
        return new Image(String.valueOf(Icons.class.getResource(name)));
    }

    private static Image create(String name, double requestedWidth, double requestedHeight) {
        return new Image(String.valueOf(Icons.class.getResource(name)), requestedWidth, requestedHeight, true,
                true);
    }

    private Icons() {
    }
}
