# Football Season Evaluation and Management Application

This software shows the seasons, matchdays and matches of the german Bundesliga. Furthermore, you can create your own seasons with custom teams.

## Status

[![Pipeline Status](https://gitlab.com/plumstone/bava/badges/master/pipeline.svg)](https://gitlab.com/plumstone/bava/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=de.bredex.phil%3Abava&metric=alert_status)](https://sonarcloud.io/dashboard?id=de.bredex.phil%3Abava)

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/dashboard?id=de.bredex.phil%3Abava)

## Build Project

### Create windows executable with launch4j
```
mvn package -P exe
```
### Create windows insaller (msi) - requires package
```
create-installer.cmd
```

## Built With

* [Java 17](https://adoptopenjdk.net/) - Programming Language
* [JavaFX 17](https://openjfx.io/) - UI-Toolkit and Framework
* [Spring](https://spring.io/) - Spring Framework with Spring Boot
* [Maven](https://maven.apache.org/) - Project Build and Dependency Management

## Authors

**Philemon Hilscher** - [plumstone](https://gitlab.com/plumstone)

## License

This project is licensed under the Apache License 2.0 - see the [LICENSE](LICENSE) file for details.
