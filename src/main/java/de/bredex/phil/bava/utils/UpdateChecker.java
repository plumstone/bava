package de.bredex.phil.bava.utils;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import de.bredex.phil.bava.ApplicationObjects;
import de.bredex.phil.bava.BavaEvent;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.api.releases.Release;
import org.greenrobot.eventbus.EventBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Version;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * The update checker checks if a new version of this program is available.
 *
 * @author Philemon Hilscher
 */
@Component
public class UpdateChecker {

    private static final Logger LOG = LoggerFactory.getLogger(UpdateChecker.class);

    @Value("${releases.api.url}")
    private String apiUrl;

    @Value("${releases.url}")
    private String downloadUrl;

    @Value("${web-request.timeout}")
    private int timeout;

    private Version latestRelease;

    @PostConstruct
    private void checkforUpdates() {
        Thread thread = new Thread(this::checkforUpdatesImpl);
        thread.start();
    }

    private void checkforUpdatesImpl() {
        try {
            HttpClient client = HttpClient.newHttpClient();

            HttpRequest request = HttpRequest.newBuilder(URI.create(apiUrl))
                    .header("Content-Type", "application/json")
                    .timeout(Duration.ofMillis(timeout))
                    .GET().build();

            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

            ObjectMapper mapper = new ObjectMapper();

            List<Release> releases = Arrays.asList(mapper.readValue(response.body(), Release[].class));
            releases.stream().findFirst().ifPresent(this::determineAvailableUpdate);
        } catch (IOException e) {
            LOG.warn("Could not check for available updates", e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private void determineAvailableUpdate(Release release) {
        String currentVersionStr = ApplicationObjects.getBuildInfo().getProperty("build.version");
        if (!StringUtils.hasText(currentVersionStr)) {
            return;
        }
        Version currentVersion = Version.parse(currentVersionStr);

        latestRelease = Version.parse(release.getName());
        LOG.debug("Latest release: {}", latestRelease);
        boolean isUpdateAvailable = latestRelease.isGreaterThan(currentVersion);
        if (isUpdateAvailable) {
            LOG.info("Update available: {} -> {}", currentVersion, latestRelease);
        } else {
            LOG.info("Application is up to date: {}", latestRelease);
        }
        EventBus.getDefault().post(new BavaEvent.ApplicationUpdateCheckFinished(isUpdateAvailable));
    }

    /**
     * Gets download url.
     *
     * @return the download url
     */
    public String getDownloadUrl() {
        return downloadUrl;
    }

    /**
     * Gets latest release.
     *
     * @return the latest release
     */
    public Version getLatestRelease() {
        return latestRelease;
    }
}
