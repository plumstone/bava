package de.bredex.phil.bava.utils;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.css.PseudoClass;
import javafx.scene.control.Button;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

/**
 * Utility class which contains Listeners used in multiple Controllers.
 *
 * @author Philemon Hilscher
 */
public final class Listeners {

    /** The logger. */
    private static final Logger LOG = LoggerFactory.getLogger(Listeners.class);

    public static class ViewSwitchButton implements ChangeListener<Button> {

        @Override
        public void changed(ObservableValue<? extends Button> observable, Button oldValue, Button newValue) {
            if (LOG.isTraceEnabled()) {
                LOG.trace("selected button {}", newValue);
            }
            if (Objects.nonNull(oldValue)) {
                oldValue.pseudoClassStateChanged(PseudoClass.getPseudoClass("selected"), false);
            }
            newValue.pseudoClassStateChanged(PseudoClass.getPseudoClass("selected"), true);
        }
    }

    private Listeners() {
    }

}
