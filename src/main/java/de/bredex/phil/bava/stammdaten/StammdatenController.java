package de.bredex.phil.bava.stammdaten;

import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import de.bredex.phil.bava.BavaEvent;
import de.bredex.phil.bava.JFXController;
import de.bredex.phil.bava.SeasonManager;
import de.bredex.phil.bava.stammdaten.saison.SeasonController;
import de.bredex.phil.bava.stammdaten.spieltag.StammdatenSpieltagController;
import de.bredex.phil.bava.stammdaten.verein.StammdatenVereinController;
import de.bredex.phil.bava.utils.Listeners;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * Die Klasse ControllerStammdaten. Hier werden alle Daten einer Saison festgelegt und ggf. geändert.
 *
 * @author Philemon Hilscher
 */
@Controller
public class StammdatenController extends JFXController<BorderPane> implements SeasonManager {

    @FXML
    private Button btnSeasonView;

    @FXML
    private Button btnTeamView;

    @FXML
    private Button btnMatchdayView;

    @FXML
    private VBox childContainer;

    @Autowired
    private SeasonController seasonController;

    @Autowired
    private StammdatenVereinController contrStammdatenVerein;

    @Autowired
    private StammdatenSpieltagController contrStammdatenSpieltag;

    private SimpleObjectProperty<Button> activeBtnProperty;

    /**
     * Der Konstruktor der Klasse ControllerStammdaten.
     */
    public StammdatenController() {
        super("StammdatenPane.fxml");
        EventBus.getDefault().register(this);
    }

    @Override
    protected void postConstruct() {
        showSeasonController();
    }

    @FXML
    private void initialize() {
        activeBtnProperty = new SimpleObjectProperty<>();
        activeBtnProperty.addListener(new Listeners.ViewSwitchButton());
    }

    @FXML
    private void showSeasonController() {
        childContainer.getChildren().clear();

        childContainer.getChildren().add(seasonController.getPane());
        bindControllerSize(seasonController);
        activeBtnProperty.set(btnSeasonView);
    }

    @FXML
    private void btnVereinAnlegenClicked() {
        childContainer.getChildren().clear();

        childContainer.getChildren().add(contrStammdatenVerein.getPane());
        bindControllerSize(contrStammdatenVerein);
        activeBtnProperty.set(btnTeamView);
    }

    @FXML
    private void btnSpieltagAnlegenClicked() {
        childContainer.getChildren().clear();

        childContainer.getChildren().add(contrStammdatenSpieltag.getPane());
        bindControllerSize(contrStammdatenSpieltag);
        activeBtnProperty.set(btnMatchdayView);
    }

    private void bindControllerSize(JFXController<? extends Pane> controller) {
        controller.getPane().prefWidthProperty().bind(getPane().widthProperty());
        controller.getPane().prefHeightProperty().bind(getPane().heightProperty());
    }

    @Subscribe
    @Override
    public void updateView(BavaEvent.SeasonUpdated event) {
        // noop
    }
}
