package de.bredex.phil.bava;

import java.io.IOException;
import java.util.Properties;
import javafx.application.HostServices;
import javafx.stage.Stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Central objects of the application, which should be accessible from anywhere.
 *
 * @author Philemon Hilscher
 */
public final class ApplicationObjects {

    private static final Logger LOG = LoggerFactory.getLogger(ApplicationObjects.class);

    private static HostServices hostServices;

    private static Stage primaryStage;

    /** The build-info.properties file loaded as a java object. */
    private static final Properties BUILD_INFO = new Properties();

    static {
        var inputStream = ApplicationObjects.class.getResourceAsStream("/META-INF/build-info.properties");
        if (inputStream != null) {
            try {
                BUILD_INFO.load(inputStream);
            } catch (IOException e) {
                LOG.warn("Could not load 'build-info.properties'", e);
            }
        }
    }

    private ApplicationObjects() {
    }

    /**
     * Returns the {@link HostServices}.
     *
     * @return the {@link HostServices}
     */
    public static HostServices getApplicationHostServices() {
        return hostServices;
    }

    /**
     * Sets application host services.
     *
     * @param hostServices
     *         the {@link HostServices}
     */
    public static void setApplicationHostServices(HostServices hostServices) {
        ApplicationObjects.hostServices = hostServices;
    }

    /**
     * Returns the primary {@link Stage}.
     *
     * @return the primary {@link Stage}
     */
    public static Stage getPrimaryStage() {
        return primaryStage;
    }

    /**
     * Sets primary stage.
     *
     * @param primaryStage
     *         the primary {@link Stage}
     */
    public static void setPrimaryStage(Stage primaryStage) {
        ApplicationObjects.primaryStage = primaryStage;
    }

    /**
     * Gets build info properties.
     *
     * @return the build info
     */
    public static Properties getBuildInfo() {
        return BUILD_INFO;
    }
}
