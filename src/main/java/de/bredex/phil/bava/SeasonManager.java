package de.bredex.phil.bava;

/**
 * Interface for all controllers that manage a season.
 *
 * @author Philemon Hilscher
 */
public interface SeasonManager {

    /**
     * Updates the view of the season manager.
     *
     * @param event
     *         the event to pass
     */
    void updateView(BavaEvent.SeasonUpdated event);

}
