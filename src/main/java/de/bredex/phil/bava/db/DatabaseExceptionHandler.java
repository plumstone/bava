package de.bredex.phil.bava.db;

import java.sql.SQLIntegrityConstraintViolationException;

import de.bredex.phil.bava.components.AlertDialog;
import de.bredex.phil.bava.i18n.Messages;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

/**
 * Error handler for errors occuring while operating with the database.
 *
 * @author Philemon Hilscher
 */
@Component
public class DatabaseExceptionHandler {

    /**
     * Handles a {@link DataIntegrityViolationException}.
     *
     * @param d
     *         the occured exception
     */
    public void handle(DataIntegrityViolationException d) {
        String message = d.getLocalizedMessage();
        if (d.getCause() instanceof ConstraintViolationException c) {
            message = c.getLocalizedMessage();
            if (c.getCause() instanceof SQLIntegrityConstraintViolationException s) {
                message = s.getLocalizedMessage();
                final int errorCodeDuplicate = 23505;
                if (s.getErrorCode() == errorCodeDuplicate) {
                    message = Messages.getString("alert.database.duplicate");
                }
            }
        }
        AlertDialog.showInformationDialog(message);
    }
}
