@echo off

mkdir target\jar\
copy target\bava-1.8.1.jar target\jar\

call jpackage --input target\jar\ --dest target\ --name "Bava" --main-jar bava-1.8.1.jar --app-version 1.8.1 --type msi --icon src\main\resources\soccer_ball2_u1M_icon.ico --win-dir-chooser --win-shortcut-prompt --win-per-user-install --verbose --copyright "Copyright (C) 2017-2022 Philemon Hilscher" --vendor "Philemon Hilscher"
