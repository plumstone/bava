package de.bredex.phil.bava.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

/**
 * Die KLasse Spiel. Sie repräsentiert ein Spiel an einem Spieltag.
 *
 * @author Philemon Hilscher
 */
@Entity
public class Spiel {

    @Id
    @GeneratedValue
    private Long id;

    /** Die Spiel-Nummer. */
    @Basic
    private int nummer;

    /** Der Heimspieler. */
    @OneToOne(cascade = CascadeType.ALL)
    private Spieler heim;

    /** Der Gastspieler. */
    @OneToOne(cascade = CascadeType.ALL)
    private Spieler gast;

    /** Benötigt für json Konvertierung. Definiert, zu welchem Spieltag dieses Spiel gehört. */
    @Transient
    private int spieltagnummer;

    @Basic
    private boolean started;

    @Basic
    private boolean finished;

    /**
     * Gets nummer.
     *
     * @return the nummer
     */
    public int getNummer() {
        return nummer;
    }

    /**
     * Sets nummer.
     *
     * @param nummer
     *         the nummer
     */
    public void setNummer(int nummer) {
        this.nummer = nummer;
    }

    /**
     * Gets heim.
     *
     * @return the heim
     */
    public Spieler getHeim() {
        return heim;
    }

    /**
     * Sets heim.
     *
     * @param heim
     *         the heim
     */
    public void setHeim(Spieler heim) {
        this.heim = heim;
    }

    /**
     * Gets gast.
     *
     * @return the gast
     */
    public Spieler getGast() {
        return gast;
    }

    /**
     * Sets gast.
     *
     * @param gast
     *         the gast
     */
    public void setGast(Spieler gast) {
        this.gast = gast;
    }

    /**
     * Gets spieltagnummer.
     *
     * @return the spieltagnummer
     */
    public int getSpieltagnummer() {
        return spieltagnummer;
    }

    /**
     * Sets spieltagnummer.
     *
     * @param spieltagnummer
     *         the spieltagnummer
     */
    public void setSpieltagnummer(int spieltagnummer) {
        this.spieltagnummer = spieltagnummer;
    }

    /**
     * Is started boolean.
     *
     * @return the boolean
     */
    public boolean isStarted() {
        return started;
    }

    /**
     * Sets started.
     *
     * @param started
     *         the started
     */
    public void setStarted(boolean started) {
        this.started = started;
    }

    /**
     * Is finished boolean.
     *
     * @return the boolean
     */
    public boolean isFinished() {
        return finished;
    }

    /**
     * Sets finished.
     *
     * @param finished
     *         the finished
     */
    public void setFinished(boolean finished) {
        this.finished = finished;
    }
}
