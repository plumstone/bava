package de.bredex.phil.bava.spieltag;

import java.util.Objects;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.util.Callback;

import static de.bredex.phil.bava.utils.Constants.PROGRESS_INDICATOR_SIZE;

/**
 * Cell factory for the cells that show the goal count in the matchday overview.
 * A progress indicator will be shown next to the goal count if the match is currently running (online data).
 *
 * @author Philemon Hilscher
 */
public class MatchGoalsCellFactory
        implements Callback<TableColumn<SpielWrapper, String>, TableCell<SpielWrapper, String>> {

    private static final int GRAPHIC_TEXT_GAP = 10;

    @Override
    @SuppressWarnings("java:S110")
    public TableCell<SpielWrapper, String> call(TableColumn<SpielWrapper, String> param) {
        return new TableCell<>() {

            private ProgressIndicator progressIndicator;

            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                TableRow<SpielWrapper> tableRow = getTableRow();
                if (tableRow != null) {
                    SpielWrapper spiel = tableRow.getItem();
                    if (spiel != null && spiel.isStarted() && !spiel.isFinished()) {
                        setGraphic(getProgressIndicator());
                        setGraphicTextGap(GRAPHIC_TEXT_GAP);
                    } else {
                        setGraphicTextGap(0);
                        setGraphic(null);
                    }
                    setText(item);
                }
            }

            private ProgressIndicator getProgressIndicator() {
                if (Objects.isNull(progressIndicator)) {
                    progressIndicator = new ProgressIndicator();
                    progressIndicator.setMaxSize(PROGRESS_INDICATOR_SIZE, PROGRESS_INDICATOR_SIZE);
                }
                return progressIndicator;
            }
        };

    }

}
