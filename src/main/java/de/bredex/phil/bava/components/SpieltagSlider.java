package de.bredex.phil.bava.components;

import java.util.function.IntConsumer;
import javafx.scene.control.Slider;

/**
 * Die Klasse SpieltagSlider. Ein SpieltagSlider ist ein erweitertes Objekt des Typs Slider, welcher genau für diesen
 * Zweck verwendet werden kann.
 *
 * @author Philemon Hilscher
 */
@SuppressWarnings("java:S110")
public class SpieltagSlider extends Slider {

    /**
     * Der Konstruktor der Klasse SpieltagSlider.
     */
    public SpieltagSlider() {
        setDefaultValues();
    }

    /**
     * Hier werden Standardwerte für den SpieltagSlider festgelegt.
     */
    private void setDefaultValues() {
        setMin(0);
        setMax(0);
        setValue(1);
        setShowTickLabels(true);
        setShowTickMarks(true);
        setSnapToTicks(true);
        setMajorTickUnit(1);
        setMinorTickCount(0);
        setBlockIncrement(1);
    }

    /**
     * Sets the maximum.
     *
     * @param max
     *         the maximum to set
     */
    public void setMaximum(int max) {
        if (max > 0) {
            setMin(1);
            setMax(max);
        } else {
            setMin(0);
            setMax(0);
        }
    }

    /**
     * @return den aktuellen Integer Wert
     */
    public int getIntValue() {
        return (int) Math.round(super.getValue());
    }

    /**
     * Sets the consumer that should handle the new int value of the slider when changed.
     *
     * @param consumer
     *         the consumer
     */
    public void setOnChangedConsumer(IntConsumer consumer) {
        valueProperty().addListener((obs, oldValue, newValue) -> {
            int oldIntValue = (int) Math.round(oldValue.doubleValue());
            int newIntValue = (int) Math.round(newValue.doubleValue());
            if (oldIntValue != newIntValue) {
                consumer.accept(newIntValue);
            }
        });
    }
}
