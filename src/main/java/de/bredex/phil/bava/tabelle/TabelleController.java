package de.bredex.phil.bava.tabelle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;

import de.bredex.phil.bava.BavaEvent;
import de.bredex.phil.bava.JFXController;
import de.bredex.phil.bava.QuickResultHandler;
import de.bredex.phil.bava.SeasonManager;
import de.bredex.phil.bava.entity.Saison;
import de.bredex.phil.bava.entity.Spieltag;
import de.bredex.phil.bava.entity.Verein;
import de.bredex.phil.bava.i18n.Messages;
import de.bredex.phil.bava.utils.Calculator;
import de.bredex.phil.bava.utils.Listeners;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.springframework.stereotype.Controller;

/**
 * The controller of the season table.
 *
 * @author Philemon Hilscher
 */
@Controller
public class TabelleController extends JFXController<BorderPane> implements SeasonManager, QuickResultHandler {

    @FXML
    private TableView<VereinWrapper> tableViewTabelle;

    @FXML
    private Button btnGesamt;

    @FXML
    private Button btnHeim;

    @FXML
    private Button btnAuswaerts;

    private SimpleObjectProperty<Button> activeBtnProperty;

    private boolean showQuickResults;

    private Saison season;

    /**
     * Der Konstruktor des Controllers, zuständig für die Tabelle.
     */
    public TabelleController() {
        super("TabellePane.fxml");
        EventBus.getDefault().register(this);
    }

    @FXML
    private void initialize() {
        initButtons();
        initTabelle();
    }

    /**
     * Initialisiert die Buttons der Tabelle um zwischen den Ansichten "Gesamt", "Heim" und "Auswärts" zu wechseln.
     */
    private void initButtons() {
        btnGesamt.setOnAction(event -> {
            activeBtnProperty.set(btnGesamt);
            showTabelle(TableSwitchView.GESAMT);
        });
        btnHeim.setOnAction(event -> {
            activeBtnProperty.set(btnHeim);
            showTabelle(TableSwitchView.HEIM);
        });
        btnAuswaerts.setOnAction(event -> {
            activeBtnProperty.set(btnAuswaerts);
            showTabelle(TableSwitchView.AUSWAERTS);
        });

        activeBtnProperty = new SimpleObjectProperty<>();
        activeBtnProperty.addListener(new Listeners.ViewSwitchButton());
        activeBtnProperty.setValue(btnGesamt);
    }

    private void prepareTabellendaten(List<Verein> vereinsListe) {
        List<VereinWrapper> tabellendaten = new ArrayList<>();
        for (Verein verein : vereinsListe) {
            tabellendaten.add(new VereinWrapper(verein));
        }
        tableViewTabelle.setItems(FXCollections.observableArrayList(tabellendaten));
    }

    /**
     * berechnet die Tabellendaten aus der gespeicherten Saison. Dabei wird jeder einzelne Spieltag und jedes enthaltene
     * Spiel gelesen und die Daten daraus in eine TabellenListe geschrieben.
     */
    private void berechneTabellendaten(List<Spieltag> matchdays) {
        List<VereinWrapper> tabellendaten = new ArrayList<>(tableViewTabelle.getItems());
        for (VereinWrapper vereinWrapper : tabellendaten) {
            vereinWrapper.reset();
        }

        Calculator calculator = new Calculator(showQuickResults);
        for (Spieltag spieltag : matchdays) {
            if (!spieltag.getSpiele().isEmpty() && spieltag.isStarted()) {
                calculator.calculateSpieltagResults(tabellendaten, spieltag);
            }
        }
    }

    private void refreshTable() {
        if (activeBtnProperty.get().equals(btnGesamt)) {
            showTabelle(TableSwitchView.GESAMT);
        } else if (activeBtnProperty.get().equals(btnHeim)) {
            showTabelle(TableSwitchView.HEIM);
        } else if (activeBtnProperty.get().equals(btnAuswaerts)) {
            showTabelle(TableSwitchView.AUSWAERTS);
        } else {
            throw new IllegalArgumentException("unsupported active btn property");
        }
    }

    /**
     * Führt den Wechsel zwischen Gesamt, Heim und Auswärtsdaten aus.
     *
     * @param tableSwitchView
     *         the indicator which view to show
     */
    private void showTabelle(TableSwitchView tableSwitchView) {
        ObservableList<TableColumn<VereinWrapper, ?>> columnlist = tableViewTabelle.getColumns();

        for (int i = 0; i < columnlist.size(); i++) {
            columnlist.get(i).setCellFactory(new SeasonTableCellFactory<>(tableSwitchView, i));
        }
        tableViewTabelle.getItems().sort(tableSwitchView.getComparator());
        Calculator calculator = new Calculator(showQuickResults);
        calculator.calculatePositions(tableSwitchView.getComparator(), tableViewTabelle.getItems());
        tableViewTabelle.refresh();
    }

    /**
     * Initialisiert die TableView.
     */
    private void initTabelle() {
        final double columnPositionFactor = 0.05;
        final double columnNameFactor = 0.3725;
        final double columnPointsFactor = 0.075;
        final double columnGoalsFactor = 0.1;
        tableViewTabelle.setFocusTraversable(false);
        tableViewTabelle.focusModelProperty().set(null);

        tableViewTabelle.getColumns().clear();
        TableColumn<VereinWrapper, Integer> columnPlatz = new TableColumn<>(
                Messages.getString("tabelle.columns.platz"));
        columnPlatz.prefWidthProperty().bind(tableViewTabelle.widthProperty().multiply(columnPositionFactor));

        TableColumn<VereinWrapper, String> columnName = new TableColumn<>(
                Messages.getString("tabelle.columns.verein"));
        columnName.prefWidthProperty().bind(tableViewTabelle.widthProperty().multiply(columnNameFactor));

        TableColumn<VereinWrapper, Integer> columnPunkte = new TableColumn<>(
                Messages.getString("tabelle.columns.punkte"));
        columnPunkte.prefWidthProperty().bind(tableViewTabelle.widthProperty().multiply(columnPointsFactor));

        TableColumn<VereinWrapper, Integer> columnSpiele = new TableColumn<>(
                Messages.getString("tabelle.columns.spiele"));
        columnSpiele.prefWidthProperty().bind(tableViewTabelle.widthProperty().multiply(columnPointsFactor));

        TableColumn<VereinWrapper, Integer> columnSiege = new TableColumn<>(
                Messages.getString("tabelle.columns.siege"));
        columnSiege.prefWidthProperty().bind(tableViewTabelle.widthProperty().multiply(columnPointsFactor));

        TableColumn<VereinWrapper, Integer> columnUnentschieden = new TableColumn<>(
                Messages.getString("tabelle.columns.unentschieden"));
        columnUnentschieden.prefWidthProperty().bind(tableViewTabelle.widthProperty().multiply(columnPointsFactor));

        TableColumn<VereinWrapper, Integer> columnNiederlagen = new TableColumn<>(
                Messages.getString("tabelle.columns.niederlagen"));
        columnNiederlagen.prefWidthProperty().bind(tableViewTabelle.widthProperty().multiply(columnPointsFactor));

        TableColumn<VereinWrapper, String> columnTore = new TableColumn<>(
                Messages.getString("tabelle.columns.tore"));
        columnTore.prefWidthProperty().bind(tableViewTabelle.widthProperty().multiply(columnGoalsFactor));

        TableColumn<VereinWrapper, Integer> columnTordifferenz = new TableColumn<>(
                Messages.getString("tabelle.columns.tordifferenz"));
        columnTordifferenz.prefWidthProperty().bind(tableViewTabelle.widthProperty().multiply(columnGoalsFactor));

        tableViewTabelle.getColumns()
                .addAll(Arrays.asList(columnPlatz, columnName, columnPunkte, columnSpiele, columnSiege,
                        columnUnentschieden, columnNiederlagen, columnTore, columnTordifferenz));
        tableViewTabelle.getColumns().forEach(column -> {
            column.setSortable(false);
            column.setResizable(false);
            column.setReorderable(false);
        });
    }

    @Override
    @Subscribe
    public void updateView(BavaEvent.SeasonUpdated event) {
        season = event.saison();
        Platform.runLater(() -> {
            List<Verein> teams = List.of();
            List<Spieltag> matches = List.of();
            if (season != null) {
                teams = season.getTeams();
                matches = season.getSpieltage();
            }
            prepareTabellendaten(teams);
            berechneTabellendaten(matches);
            refreshTable();
        });
    }

    @Override
    @Subscribe
    public void showQuickResults(BavaEvent.CheckBoxQuickResultChanged event) {
        this.showQuickResults = event.checked();
        if (season != null) {
            prepareTabellendaten(season.getTeams());
            berechneTabellendaten(season.getSpieltage());
        }
        refreshTable();
    }

}
