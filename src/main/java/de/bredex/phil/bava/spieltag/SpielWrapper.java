package de.bredex.phil.bava.spieltag;

import de.bredex.phil.bava.entity.Spiel;
import de.bredex.phil.bava.entity.Spieler;
import de.bredex.phil.bava.utils.Constants;

import java.util.Objects;

/**
 * The SpielWrapper wraps the entity {@link Spiel}, to provide logic methods.
 *
 * @author Philemon Hilscher
 */
public class SpielWrapper {

    private final Spieler heimSpieler;

    private final Spieler gastSpieler;

    private final boolean started;

    private final boolean finished;

    /**
     * Creates a new SpielWrapper.
     *
     * @param adaptee
     *         the entity to wrap
     */
    public SpielWrapper(Spiel adaptee) {
        heimSpieler = adaptee.getHeim();
        gastSpieler = adaptee.getGast();
        started = adaptee.isStarted();
        finished = adaptee.isFinished();
    }

    /**
     * Gets heim tore.
     *
     * @return the goals of the home team.
     */
    public String getHeimTore() {
        return getTore(heimSpieler);
    }

    /**
     * Gets gast tore.
     *
     * @return the goals of the guest team.
     */
    public String getGastTore() {
        return getTore(gastSpieler);
    }

    /**
     * Retrieves the goals of the given player.
     *
     * @param spieler
     *         the player
     * @return the goals or "--"
     */
    private String getTore(Spieler spieler) {
        if (Objects.nonNull(spieler)) {
            Integer tore = spieler.getTore();
            if (Objects.nonNull(tore)) {
                return String.valueOf(tore);
            } else {
                return Constants.NO_GOALS_PLACEHOLDER;
            }
        } else {
            return Constants.EMPTY_STRING;
        }
    }

    /**
     * Gets heim spieler.
     *
     * @return the heim spieler
     */
    public Spieler getHeimSpieler() {
        return heimSpieler;
    }

    /**
     * Gets gast spieler.
     *
     * @return the gast spieler
     */
    public Spieler getGastSpieler() {
        return gastSpieler;
    }

    /**
     * Is started boolean.
     *
     * @return the boolean
     */
    public boolean isStarted() {
        return started;
    }

    /**
     * Is finished boolean.
     *
     * @return the boolean
     */
    public boolean isFinished() {
        return finished;
    }
}
