package de.bredex.phil.bava;

import java.io.IOException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.http.HttpResponse;
import java.net.http.HttpTimeoutException;
import java.time.Year;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeoutException;

import de.bredex.phil.bava.entity.Saison;
import de.bredex.phil.bava.entity.Verein;
import de.bredex.phil.bava.mapper.TeamMapper;
import de.bredex.phil.bava.repository.SeasonRepository;
import de.bredex.phil.bava.repository.TeamRepository;
import de.bredex.phil.bava.web.SeasonUpdater;
import de.bredex.phil.bava.web.WebRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import static de.bredex.phil.bava.utils.EntityVoConverter.convert;
import de.openligadb.api.matchdata.Match;
import de.openligadb.api.teams.Team;
import org.greenrobot.eventbus.EventBus;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.testfx.api.FxToolkit;

/**
 * Integration tests for Bava.
 *
 * @author Philemon Hilscher
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ActiveProfiles("test")
@Rollback
class BavaIntegrationTest {

    private static final Logger LOG = LoggerFactory.getLogger(BavaIntegrationTest.class);

    private final int repetitions = 3;

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private SeasonRepository seasonRepository;

    @Autowired
    private SeasonUpdater seasonUpdater;

    @Autowired
    private TeamMapper teamMapper;

    @Autowired
    private WebRequest webRequest;

    @BeforeAll
    static void setup() throws TimeoutException {
        System.setProperty("testfx.robot", "glass");
        System.setProperty("testfx.headless", "true");
        System.setProperty("prism.order", "sw");
        System.setProperty("prism.text", "t2k");
        System.setProperty("java.awt.headless", "true");
        FxToolkit.registerPrimaryStage();
    }

    @Test
    void testSeasonUpdate() throws IOException {
        String jsonTeams = new String(
                Objects.requireNonNull(getClass().getResourceAsStream("tabelle/teams_19_20.json")).readAllBytes());
        String jsonMatches = new String(
                Objects.requireNonNull(getClass().getResourceAsStream("spieltag/matches_19_20.json")).readAllBytes());
        ObjectMapper objectMapper = new ObjectMapper();
        List<Team> teams = List.of(objectMapper.readValue(jsonTeams, Team[].class));
        assertFalse(teams.isEmpty());
        List<Match> matches = List.of(objectMapper.readValue(jsonMatches, Match[].class));
        assertFalse(matches.isEmpty());

        Saison saison = convert(matches, teams.size());
        assertNotNull(saison);
        saison.setTeams(teamMapper.mapList(teams));

        teamRepository.saveAll(saison.getTeams());

        EventBus.getDefault().post(new BavaEvent.SeasonUpdated(saison));
    }

    @RepeatedTest(repetitions)
    @Disabled("failing for yet unknown reason")
    void testWebRequestAvailableTeams(RepetitionInfo repetitionInfo) {
        final int year = 2020 - repetitionInfo.getCurrentRepetition();
        List<Verein> list = List.of();
        try {
            HttpResponse<String> response = webRequest.getAvailableTeams(repetitionInfo.getCurrentRepetition(),
                    Year.of(year));
            if (response.statusCode() == HttpURLConnection.HTTP_OK) {
                list = teamMapper.mapList(seasonUpdater.getListFromResponse(response, Team[].class));
            }
        } catch (ConnectException e) {
            LOG.info("Connection could not be established.", e);
        } catch (HttpTimeoutException e) {
            LOG.info("Request timed out.", e);
        } catch (IOException e) {
            LOG.warn("Requesting of data has failed.", e);
        }

        assertNotNull(list);

        teamRepository.saveAll(list);

        List<Verein> teams = teamRepository.findAll();
        assertNotEquals(0, teams.size());
    }

    @RepeatedTest(repetitions)
    @Disabled("failing for yet unknown reason")
    void testWebRequestMatchdays(RepetitionInfo repetitionInfo) {
        final int year = 2020;
        final int teamCount = 18;
        Saison season = null;
        try {
            HttpResponse<String> response = webRequest.getSpieltage(repetitionInfo.getCurrentRepetition(),
                    Year.of(year));
            season = convert(seasonUpdater.getListFromResponse(response, Match[].class), teamCount);
            if (response.statusCode() == HttpURLConnection.HTTP_OK) {
                assertNotNull(response.body());
            }
        } catch (ConnectException e) {
            LOG.info("Connection could not be established.", e);
        } catch (HttpTimeoutException e) {
            LOG.info("Request timed out.", e);
        } catch (IOException e) {
            LOG.warn("Requesting of data has failed.", e);
        }

        if (season != null) {
            seasonRepository.save(season);
        }

        List<Saison> list = seasonRepository.findAll();
        assertNotEquals(0, list.size());
    }

    @AfterEach
    void purgeRepositories() {
        teamRepository.deleteAll();
        seasonRepository.deleteAll();
    }
}
